<?php 

namespace App\Helpers;
use App\Quiz;
use App\Video;
use App\Book;
use App\ParticipantUser;
use App\MoneyRequest;


class Helper
{
    public static function addQuiz($request, $status)
    {
    	$quiz = new Quiz;
    	$quiz->title = $request->title;
    	$quiz->start_date = $request->start_date.' '.$request->start_time;
    	$quiz->start_time = $request->start_time;
    	$quiz->end_date = $request->end_date.' '.$request->end_time;
    	$quiz->end_time = $request->end_time;
    	$quiz->price = $request->price;
    	$quiz->status = $status;
        $quiz->grade = $request->grade;
        $quiz->person = $request->person;
        $quiz->mark = $request->mark;
        $quiz->description = $request->description;
        $quiz->total_quistion = $request->total_quistion;
        $quiz->distribution = $request->distribution;
    	$quiz->save();
    }
    public static function quizStatus( $url )
    {
    	$exp = explode('/', $url);
    	$url = end($exp);
    	$status = 1;

    	if($url == 'general-quiz'){
    		$status = 1;
    	}else if($url == 'spelling-quiz'){
    		$status = 2;
    	}else if($url == 'reading-quiz'){
    		$status = 3;
    	}else if($url == 'video-quiz'){
    		$status = 4;
    	}
    	return $status;
    }

    public static function updateQuiz($request)
    {
        $quiz = Quiz::find($request->id);
        $quiz->title = $request->title;
        $quiz->start_date = $request->start_date.' '.$request->start_time;
        $quiz->start_time = $request->start_time;
        $quiz->end_date = $request->end_date.' '.$request->end_time;
        $quiz->end_time = $request->end_time;
        $quiz->price = $request->price;
        $quiz->status = $request->status;
        $quiz->grade = $request->grade;
        $quiz->person = $request->person;
        $quiz->mark = $request->mark;
        $quiz->total_quistion = $request->total_quistion;
        $quiz->distribution = $request->distribution;
        $quiz->description = $request->description;
        $quiz->save();
    }
    public static function quizName( $id )
    {
        $quiz = Quiz::find($id);
        return $quiz->title;
    }
    public static function bookName( $id, $status )
    {
        if($status == 3){
            $bk = Book::find($id);
            return $bk->title;
        }
        if($status == 4){
            $bk = Video::find($id);
            return $bk->title;
        }
        
    }

    public static function sliceContent( $content)
    {
        $slContent = $content;
        if(strlen($content) > 300){
            $slContent = substr($content, 0, 300).'...';
        }
        return $slContent;
    }

    public static function currentAmount()
    {
        $userId = auth()->user()->id;
        $earn = ParticipantUser::where('user_id',$userId)->sum('aword');
        $paid = MoneyRequest::where('user_id',$userId)->where('paid_status','paid')->sum('paid');
        return number_format( $earn - $paid, 2 );

    }

    public static function pendingAmount()
    {
        $userId = auth()->user()->id;
        $pending = MoneyRequest::where('user_id',$userId)->where('paid_status','pending')->sum('requested_amount');
        return number_format( $pending, 2 );
    } 

    public static function paidAmount()
    {
        $userId = auth()->user()->id;
        $paid = MoneyRequest::where('user_id',$userId)->where('paid_status','paid')->sum('paid');
        return number_format( $paid, 2 );
    }

    public static function backFromAddQuestion( $status )
    {
        $url = '';
        if($status == 1){
            $url = route('admin.general.quiz');
        }else if($status == 2){
            $url = route('admin.spelling.quiz');
        }else if($status == 3){
            $url = route('admin.reading.quiz');
        }else{
            $url = route('admin.video.quiz');
        }
        return $url;
    } 

    public static function backFromAnswerdQuestion( $status, $quiz_id )
    {
        $url = '';
        if($status == 1 || $status == 2){
            $url = route('admin.general.participant',['quiz_id'=>$quiz_id,'status'=>$status]);
        }else if($status == 3){
            $url = route('admin.participant',['quiz_id'=>$quiz_id,'status'=>$status]);
        }else if($status == 4){
            $url = route('admin.video.participant',['quiz_id'=>$quiz_id,'status'=>$status]);
        }
        return $url;
    }

    public static function isAttendQuiz( $quiz_id )
    {
        $user = ParticipantUser::where('quiz_id',$quiz_id)->where('user_id',auth()->user()->id)->first();
        if($user){
            return true;
        }
        return false;

    }

    
}