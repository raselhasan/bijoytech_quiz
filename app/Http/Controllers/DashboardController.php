<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
    	return view('quiz.admin.dashboard.index');
    }

    /**
     * start function for user dashboard
     **/
    public function userDashboard()
    {
    	return view('quiz.frontend.pages.dashboard');
    }
}
