<div class="modal fade video-modal" id="pViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h5 class="modal-title" id="exampleModalLabel">Payment Details</h5>
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true"><i class="fal fa-times"></i></span>
	        	</button>
	      	</div>
		    <div class="modal-body">
				<div class="row pmntViewClas">
					<div class="col-md-6">
						<p><span>Name: </span>{{$payment->name}}</p>
						<p><span>Account Number: </span>{{$payment->account_number}}</p>
						<p><span>Account Name: </span>{{$payment->account_name}}</p>
						<p><span>Requested Amount: </span>${{number_format($payment->requested_amount,2)}}</p>
						<p><span>Payment Method: </span>{{$payment->payment_method}}</p>
						@if($payment->note)
							<p><span>Note: </span>{{$payment->note}}</p>
						@endif	
					</div>
					<div class="col-md-6">
						@if($payment->paid)
							<p><span>Paid Amount: </span>${{number_format($payment->paid,2)}}</p>
						@endif
						@if($payment->paid_status)
							<p><span>Paid Status: </span>{{$payment->paid_status}}</p>
						@endif
						@if($payment->payment_id)
							<p><span>Payment Id: </span>{{$payment->payment_id}}</p>
						@endif
						@if($payment->paid_date)
							<p><span>Paid Date: </span>{{date('m/d/Y',strtotime($payment->paid_date))}}</p>
						@endif
						@if($payment->message)
							<p><span>Admin Message: </span>{{$payment->message}}</p>
						@endif
					</div>
				</div>
		     </div>
    	</div>
  	</div>
</div>