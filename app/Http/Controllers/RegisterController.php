<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use Mail;
use Helper;
use App\User;
use Validator;
use App\Country;
use App\Mail\Welcome;
use App\Mail\SendCode;
use App\Verification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;


class RegisterController extends Controller
{
    public function userRegister()
    {
        $IPaddress = '103.136.96.6';
        //$IPaddress = \Request::ip();
        $json = file_get_contents("http://ipinfo.io/{$IPaddress}");
        $info = json_decode($json);
        $data['c_country'] = $info->country;
        $data['countries'] = Country::all();
        return view('quiz.frontend.auth.register',$data);
    }

    public function redirectRules( $status )
    {
        if(Auth::check()){
            return redirect()->route('user.quiz',['status'=>$status]);
        }
        Session::put('redirect_id', $status);
        return redirect()->route('user.login');
    }
    public function register(Request $request)
    {
        $rules = array(
            'email' => 'required|unique:users,email',
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return redirect()->back()->with('error','Email already been taken!')->withInput($request->all());
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->zip = $request->zip;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->grade = $request->grade;
        $user->guardian = $request->guardian;
        $user->relation_with_guardian = $request->relation_with_guardian;
        $user->role = 2;
        $user->date_of_birth = $request->date_of_birth;
        $user->guardian_phone = $request->guardian_phone;
        $user->guardian_email = $request->guardian_email;
        $user->save();

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        Mail::send( new Welcome($user));

        if (Auth::attempt($credentials)) {
            if(Session::has('redirect_id')){
                $red_id = Session::get('redirect_id');
                Session::forget('redirect_id');
                return redirect()->route('user.quiz',['status'=>$red_id]);
            }
            return redirect()->route('user.profile');
        }
        return redirect()->back()->with('error','Something went worng!')->withInput($request->all());
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function forgotPassword()
    {
        return view('quiz.frontend.auth.forgot-password');
    }
    public function sendCode(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if(!$user){
            return redirect()->back()->with('error','Email does not match our record!')->withInput($request->input());
        }
        Verification::where('email',$request->email)->delete();
        $veri = new Verification;
        $token = time();
        $veri->email = $request->email;
        $veri->token = $token;
        $veri->save();

        Mail::send( new SendCode($user, $token));
        return redirect()->route('user.verify')->with('msg','Verification code has been sent to your account');

    }

    public function vefiryCodePage()
    {
        return view('quiz.frontend.auth.verify');
    }

    public function vefiryCode(Request $request)
    {
        $veri = Verification::where('token',$request->code)->first();
        if(!$veri){
            return redirect()->back()->with('error','Enter valid code');
        }
        $url = route('user.resetPassword').'/'.$request->code;
        return redirect($url);

    }
    public function resetPassword($code = null)
    {
        return view('quiz.frontend.auth.reset-password',compact('code'));
    }

    public function confirmResetPassword(Request $request)
    {
        $veri = Verification::where('token',$request->code)->first();
        if(!$veri){
            return redirect()->back()->with('msg','Invalid Verification code');
        }
        if($request->password != $request->c_password){
            return redirect()->back()->with('msg','Password not match with confirm password');
        }

        $findUser = User::where('email',$veri->email)->first();
        $user = User::find($findUser->id);
        $user->password = Hash::make($request->password);
        $user->save();
        Verification::where('token',$request->code)->delete();

        Auth::login($user);
        return redirect()->route('user.profile');
    }
}
