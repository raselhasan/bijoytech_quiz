@if(count($questions) > 0)
@foreach($questions as $question)
<form method="post" class="edit-question-form" enctype="multipart/form-data" id="del-ques-{{$question->id}}">
     @csrf
    <div class="row mb-3 questions-row">
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-12">
                    <input type="hidden" name="question_id" value="{{$question->id}}">
                    <p class="d-flex align-items-center"><span class="indentity-txt">Q<span style="font-size: 12px;">{{ $loop->index + 1 }}</span></span>. 
                        <input class="form-control inactive" type="" readonly="" name="edit_question" value="{{$question->question}}">  
                        <button type="button" onclick="showInput(event)" class="btn waves-effect waves-dark btn-info btn-outline-info float-right edit-q" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-slice"></i></button> 
                        {{-- <button type="button" onclick="hideInput(event)" class="btn waves-effect waves-dark btn-danger btn-outline-danger cansel-q" style="font-size: 12px; color: #fff; width: 100px;">Cansel Edit</button> --}}
                        <button style="padding: 5px 11px;" type="button" class="btn ml-2 waves-effect waves-dark btn-danger btn-outline-danger questin-del-btn" quest-id="{{$question->id}}" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></button>
                    </p>
                </div>

                <div class="col-sm-12 ml-4">
                    <div class="row option-add-e">
                        @if(count($question->options) > 0)
                        @foreach($question->options as $option)
                        <div class="col-sm-6 option-col">
                            <p class="d-flex  align-items-center"><span class="indentity-txt mr-1">{{ $loop->index + 1 }}.</span> 
                                <input placeholder="Option name" class="form-control inactive" type="" readonly="" name="edit_option[]" value="{{$option->option}}">
                                <button type="button" onclick="optionR(event)" class="btn ml-2 waves-effect waves-dark btn-danger btn-outline-danger cansel-q"><i class="ti-trash"></i></button>
                            </p>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="mb-3 append-option"><button type="button" onclick="apeendOption(event)" class="btn waves-effect waves-dark btn-info btn-outline-info cansel-q"><i class="ti-slice"></i> Add Option</button></div>
                </div>
                <div class="col-sm-12 ml-4">
                    <p class="d-flex  align-items-center"><span class="indentity-txt mr-1">Answer:</span> <input class="form-control inactive" type="text" readonly="" name="edit_answer" value="{{$question->answer}}"></p>
                </div>
                <div class="col-sm-12 ml-4">
                    <button onclick="questionFormCall(event)" style="padding: 8px; width: 150px;" type="button" class="btn btn-primary waves-effect waves-light  my-3 cansel-q">Update</button> 
                </div>
            </div>

        </div>
    </div>
</form>
@endforeach
@else 
    <p class="noResult">No Result Found!</p>
@endif
