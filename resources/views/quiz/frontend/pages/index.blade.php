@extends('quiz.frontend.layout.app')
@section('title')
	Home
@endsection
@section('style')
	
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="row mr-0">
		@if(count($contents) > 0)
			@foreach($contents as $key=>$content)
				<div class="col-12 col-md-6 col-lg-3 pr-0">
					<div class="card mb-3">
						<div class="card-header text-center p-3">
							<div class="card-icon mb-3">
								@if($key == 0)
									<i class="fas fa-address-card"></i>
								@endif
								@if($key == 1)
									<i class="far fa-address-card"></i>
								@endif
								@if($key == 2)
									<i class="fab fa-accusoft"></i>
								@endif
								@if($key == 3)
									<i class="far fa-address-book"></i>
								@endif	
								@if($key > 3)
									<i class="fas fa-address-card"></i>
								@endif
							</div>
							<a href="{{route('singlecontent',['id'=>$content->id])}}"><h5 class="mb-0">{{$content->title}}</h5></a>
						</div>
						<div class="card-block pt-0">
							{!! Helper::sliceContent($content->content) !!}	
							<br/>
							<br/>
							@if($content->img)
								<img src="{{asset('homeImg/'.$content->img)}}" class="img-fluid" style="height:200px">
							@endif
							<div class="text-center">
								<a href="{{route('singlecontent',['id'=>$content->id])}}" class="btn btn-info">Read More</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		@endif
	</div>
</section>
@endsection

@section('script')

@endsection
