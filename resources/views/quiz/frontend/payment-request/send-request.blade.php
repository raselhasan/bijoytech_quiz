<form action="{{route('user.payment-request')}}" method="post">
	@csrf
	<div class="row mr-0">
		<div class="col-12 col-md-12 pr-0">
			@if(Session::has('success'))
				<div class="alert alert-primary" role="alert">
			  		{{Session::get('success')}}
				</div>
			@endif

			@if(Session::has('error'))	
				<div class="alert alert-danger" role="alert">
				  	{{Session::get('error')}}
				</div>
			@endif
		</div>	
		<div class="col-12 col-md-2 pr-0">
			<div class="form-group">
				<label class="mb-2">Payment Method</label>
				<select id="inputState" class="form-control" name="payment_method" required>
		        	<option value="">Choose...</option>
		        	<option value="paypal" @if(old('payment_method') == 'paypal') selected @endif>Paypal</option>
		        	<option value="bkash" @if(old('payment_method') == 'bkash') selected @endif>Bkash</option>
		        	<option value="zille" @if(old('payment_method') == 'zille') selected @endif>Zille</option>
		        	<option value="quick pay" @if(old('payment_method') == 'quick pay') selected @endif>Quick Pay</option>
		        	<option value="other" @if(old('payment_method') == 'other') selected @endif>Other</option>
		      	</select>
			</div>
		</div>
		<div class="col-12 col-md-3 pr-0">
			<div class="form-group">
				<label class="mb-2">Account Number</label>
				<input type="text" class="form-control" name="account_number" required value="{{old('account_number')}}">
			</div>
		</div>
		<div class="col-12 col-md-3 pr-0">
			<div class="form-group">
				<label class="mb-2">Account Name</label>
				<input type="text" class="form-control" name="account_name" required value="{{old('account_name')}}">
			</div>
		</div>
		<div class="col-12 col-md-4 pr-0">
			<div class="form-group">
				<label class="mb-2">Amount</label>
				<input type="number" class="form-control" name="amount" required value="{{old('amount')}}">
			</div>
		</div>
		
		<div class="col-12 col-md-12 pr-0">
			<div class="form-group">
				<label class="mb-2">Note</label>
				<textarea class="form-control" name="note" id="exampleFormControlTextarea1" rows="3">{{old('note')}}</textarea>
			</div>
		</div>
		<div class="col-12 col-md-6 pr-0">
			<div class="form-group">
				<button type="submit" class="btn btn-info" style="margin-top: 28px;">Send</button>
			</div>
		</div>
	</div>
</form>