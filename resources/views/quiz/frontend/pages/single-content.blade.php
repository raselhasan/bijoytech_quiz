@extends('quiz.frontend.layout.app')
@section('style')
	<style type="text/css">
		.cont-sec h4{
			color: yellowgreen;
			text-transform: uppercase;
			margin-top: 40px;
		}
		.cont-sec hr{
			background: #448aff;
    		width: 16%;
    		margin-bottom: 30px;
		}
	</style>
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="row mr-0">
		<div class="col-8 col-md-8 col-lg-8 pr-0">
			<h1 style="color: yellowgreen; margin-bottom: 40px;">{{$content->title}}</h1>
		</div>
		<div class="col-4 col-md-4 col-lg-4 pr-0 text-right">
			<a href="{{route('redirectRules',['status'=>$content->status])}}" class="btn btn-info">Get Started</a>
		</div>

		<div class="col-12 col-md-12 col-lg-12 pr-0">
			@if($content->img)
				<img src="{{asset('homeImg/'.$content->img)}}" class="img-fluid" style="margin-bottom: 20px; max-height: 200px;">
			@endif
			<div class="all-content">
				{!! $content->content !!}
			</div>
		</div>
		@include('quiz.frontend.pages.recent-quiz')
		@include('quiz.frontend.pages.up-coming')
	</div>
</section>
@endsection

@section('script')
	<script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>
   	<script type="text/javascript">
	    document.querySelectorAll( 'oembed[url]' ).forEach( element => {
	        const anchor = document.createElement( 'a' );
	        anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
	        anchor.className = 'embedly-card';

	        element.appendChild( anchor );
	    } );
   	</script>
@endsection
