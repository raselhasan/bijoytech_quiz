<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contact;
use App\Setting;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
 
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['quiz.frontend.include.header', 'quiz.frontend.include.footer','quiz.frontend.include.banner'], function ($view) {
            $view->with('hContact', Setting::first());
            $view->with('fContact', Contact::first());
        });
    }
}
