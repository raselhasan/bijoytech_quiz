<?php

namespace App\Http\Controllers;

use App\Question;
use App\QuestionOption;
use Illuminate\Http\Request;
use App\Quiz;
use Session;
use App\Answer;
use App\ParticipantUser;
use Helper;
class QuestionController extends Controller
{
    public function index( $quiz_id = null, $status = null )
    {   if($status == 3){
            $data['quiz'] = Quiz::where('id',$quiz_id)->with('books')->first();
        }else if($status == 4){
            $data['quiz'] = Quiz::where('id',$quiz_id)->with('videos')->first();
        }else if($status == 1 || $status == 2){
            $data['quiz'] = Quiz::where('id',$quiz_id)->first();
        }else{
            return redirect()->back();
        }
        $data['status'] = $status;
        return view('quiz.admin.question.index',$data);
    }
    public function searchQuestion(Request $request) {
        $quiz_id = $request->quiz_id;
        $variation_id = $request->variation_id;
        $status = $request->status;
        $search = $request->search;
        if($status == 3 || $status == 4){
            $data['questions'] = Question::where('variation_id',$variation_id)
            ->where('status',$status)
            ->where(function($query) use ($search){
                    $query->where('question','LIKE',"%{$search}%")
                        ->orWhere('question', 'LIKE',"%{$search}%")
                        ->orWhere('answer', 'LIKE',"%{$search}%");
                })
            ->get();
        }

        if($status == 1 || $status == 2){
            $data['questions'] = Question::where('variation_id',$quiz_id)
            ->where('status',$status)
            ->where('language',$variation_id)
            ->where(function($query) use ($search){
                    $query->where('question','LIKE',"%{$search}%")
                        ->orWhere('question', 'LIKE',"%{$search}%")
                        ->orWhere('answer', 'LIKE',"%{$search}%");
                })
            ->get();
        }
        $all_question = view('quiz.admin.question.question.all-question',$data)->render();
        return response()->json(['all_question'=>$all_question]);

    }
    public function editQuestion(Request $request){
        $ques = Question::find($request->question_id);
        $ques->question = $request->edit_question;
        $ques->answer = $request->edit_answer;
        $ques->save();
        $ques_option = 'deno-res';
        $ques_options = QuestionOption::where('question_id',$request->question_id)->get();

        foreach ($ques_options as $ques_option) {
             $ques_option->delete();
        }
        
        if($request->edit_option){
            if(count($request->edit_option) > 0){
                $options = array_filter($request->edit_option);
                for ($i = 0; $i < count($options); $i++) {
                    
                    $option = new QuestionOption();
                    $option->question_id = $ques->id;
                    $option->option = $options[$i];
                    $option->save();   
                    
                }
            }
        }    
    
        return $ques_option; 
    }

    public function addQuestion(Request $request)
    {
        $ques = new Question();
        $ques->variation_id = $request->variation_id;
        $ques->question = $request->question;
        $ques->answer = $request->answer;
        $ques->status = $request->status;
        $ques->language = $request->language;
        $ques->save();

        if($request->option){
            if(count($request->option) > 0){
                $options = array_filter($request->option);
                for ($i = 0; $i < count($options); $i++) {
                    $option = new QuestionOption();
                    $option->question_id = $ques->id;
                    $option->option = $options[$i];
                    $option->save();
                }
            }
        }
        
    }
    public function allQuestion(Request $request)
    {
        if($request->status == 3 || $request->status == 4){
            $data['questions'] = Question::where('variation_id',$request->variation_id)->where('status',$request->status)->with('options')->orderBy('id','desc')->get();
        }else if($request->status == 1 || $request->status == 2){
            $data['questions'] = Question::where('variation_id',$request->variation_id)->where('status',$request->status)->where('language',$request->language)->with('options')->orderBy('id','desc')->get();
        }
        $all_question = view('quiz.admin.question.question.all-question',$data)->render();
        return response()->json(['all_question'=>$all_question]);

    }

    /**
     * start function for user part
    **/
    public function showQuestion(Request $request, $quiz_id = null, $status = null, $book_id = null)
    {
        if(Helper::isAttendQuiz( $quiz_id ) == true){
            return redirect()->route('user.profile');
        }
        
        $data['status'] = $status;
        $data['book_id'] = $book_id;
        $data['quiz_id'] = $quiz_id;
        $data['lang'] = $request->lang;
        $data['quiz'] = $this->getBookQuestion($data);

        $questions = Session::get('userQuestions');
        $data['question'] = $questions[0];
        $data['quesNumber'] = 0;
        //dd($data['question']);
        return view('quiz.frontend.pages.questions.question',$data);
    }

    public function getBookQuestion($data)
    {   
        $quiz = Quiz::find($data['quiz_id']);
        if($data['status'] == 3 || $data['status'] == 4){
            $questions = Question::where('variation_id',$data['book_id'])->where('status',$data['status'])->limit($quiz->total_quistion)->inRandomOrder()->with('options')->get()->toArray();
        }
        if($data['status'] ==1 || $data['status'] == 2){
            $questions = Question::where('status',$data['status'])->where('variation_id',$data['quiz_id'])->where('language',$data['lang'])->limit($quiz->total_quistion)->inRandomOrder()->with('options')->get()->toArray();
        }
        
        Session::put('userQuestions',$questions);
        
    }

    public function getSingleQuestion(Request $request)
    {
        $this->saveAnswer( $request );
        $questions = Session::get('userQuestions');
        if(array_key_exists($request->quesNumber,$questions)){
            $data['question'] = $questions[$request->quesNumber];
            $data['quesNumber'] = $request->quesNumber;
            return view('quiz.frontend.pages.questions.single-question',$data)->render();
        }else{
            $data['result'] = ParticipantUser::where('quiz_id',$request->quiz_id)->where('user_id',auth()->user()->id)->first();
            $data['quiz'] = Quiz::find($request->quiz_id);
            return view('quiz.frontend.pages.questions.success',$data)->render();
        }
        
    }

    public function saveAnswer( $request )
    {
        $ans = new Answer();
        $ans->quiz_id = $request->quiz_id;
        $ans->variation_id = $request->variation_id;
        $ans->status = $request->status;
        $ans->questionId = $request->questionId;
        $get_ques = Question::find($request->questionId);
        if($get_ques->answer == $request->answer){
            $ans->rite_answer = 1;
        }else{
            $ans->worng_answer = 1;
        }
        $ans->answer = $request->answer;
        $ans->userId = auth()->user()->id;
        $ans->save();
        $right = Answer::where('quiz_id',$request->quiz_id)->where('userId',auth()->user()->id)->where('rite_answer',1)->count();
        $worng = Answer::where('quiz_id',$request->quiz_id)->where('userId',auth()->user()->id)->where('worng_answer',1)->count();
        $pUser = ParticipantUser::where('quiz_id',$request->quiz_id)->where('user_id',auth()->user()->id)->first();
        $getQuiz = Quiz::find($request->quiz_id);
        if($pUser){
            $updateU = ParticipantUser::find($pUser->id);
            $updateU->total_right_ans = $right;
            $updateU->total_worng_ans = $worng;
            $updateU->total_mark = $right * $getQuiz->mark;
            $updateU->save();
        }else{
            $addP  = new ParticipantUser();
            $addP->user_id = auth()->user()->id;
            $addP->quiz_id = $request->quiz_id;
            $addP->book_id = $request->variation_id;
            $addP->status = $request->status;
            $addP->total_right_ans = $right;
            $addP->total_worng_ans = $worng;
            $addP->total_mark = $right * $getQuiz->mark;
            $addP->date = date('Y-m-d');
            $addP->save();
        }
            
    }

    public function deleteQuestion(Request $request)
    {
        Question::where('id',$request->id)->delete();
    }

    

}
