<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function addVideo(Request $request, $quiz_id = null)
    {
        $video = new Video;
        $video->quiz_id = $quiz_id;
        $video->title = $request->title;
        $video->language = $request->language;
        $video->video = $request->video;
        $video->description = $request->description;
        $video->save();

        $data['quiz'] = Quiz::where('id',$quiz_id)->with('videos')->first();
        $new_videos = view('quiz.admin.question.videos.video-list',$data)->render();
        return response()->json(['new_videos'=>$new_videos, 'success'=>'Video added successfully']);
    }

    public function editVideo(Request $request)
    {
        $video = Video::find($request->video_id);
        $video_modal = view('quiz.admin.question.videos.edit-video',compact('video'))->render();
        return response()->json(['video_modal'=>$video_modal]);
    }

    public function updateVideo(Request $request, $quiz_id, $video_id)
    {
        $video = Video::find($video_id);
        $video->quiz_id = $quiz_id;
        $video->title = $request->title;
        $video->language = $request->language;
        $video->video = $request->video;
        $video->description = $request->description;
        $video->save();
        $data['quiz'] = Quiz::where('id',$quiz_id)->with('videos')->first();
        $new_videos = view('quiz.admin.question.videos.video-list',$data)->render();
        return response()->json(['new_videos'=>$new_videos, 'success'=>'Video successfully updated']);
    }
    public function deleteVideo(Request $request)
    {
        $video = Video::find($request->video_id);
        $video->delete();

        $data['quiz'] = Quiz::where('id',$request->quiz_id)->with('videos')->first();
        $new_videos = view('quiz.admin.question.videos.video-list',$data)->render();
        return response()->json(['new_videos'=>$new_videos, 'success'=>'Video successfully deleted']);

    }

    public function activeVideo(Request $request)
    {
        $video = Video::find($request->video_id);
        $video->active = $request->status;
        $video->save();
        $data['quiz'] = Quiz::where('id',$request->quiz_id)->with('videos')->first();
        $new_videos = view('quiz.admin.question.videos.video-list',$data)->render();
        return response()->json(['new_videos'=>$new_videos, 'success'=>'Video successfully deleted']);

    }
}
