@extends('quiz.admin.layout.layout')

@section('title')
    Video Quiz Compitision
@endsection

@section('cssfile')
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('style')
    <style type="text/css">
        
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Video Quiz Compitision</h5>
                        <p class="m-b-0">Here all video quiz compitision perticipients lists</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <a href="{{Helper::backFromAddQuestion($status)}}" class="btn waves-effect waves-light btn-success btn-skew fl-right bck-btn">Back</a>
                    <a href="{{route('admin.evaluate',['quiz_id'=>$quiz_id])}}" class="btn waves-effect waves-light btn-primary btn-skew fl-right">Evaluate</a>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('quiz.admin.global.video.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('jsfile')
    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {
           table = $('#order-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                     "url": "{{ route('admin.video-participant.get',['quiz_id'=>$quiz_id,'status'=>$status]) }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "quiz_title" },
                    { "data": "video_title" },
                    { "data": "user_name" },
                    { "data": "total_right_ans" },
                    { "data": "total_worng_ans" },
                    { "data": "aword" },
                    { "data": "position" },
                    { "data": "created_at" },
                    { "data": "options" } 
                ]    

            });
        });

        
    </script>
@endsection                        