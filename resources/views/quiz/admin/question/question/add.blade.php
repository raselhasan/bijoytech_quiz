<form class="w-100" action="{{route('admin.add.question')}}" method="post" id="add-question-form">
    @csrf
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Add Question</h5>
                <span>Add Required Question</span>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row set-new-option">
                            <div class="col-sm-11">
                                <div class="form-group form-default d-flex align-items-center">
                                    <span class="indentity-txt mr-2">Q.</span><input type="text" name="question" class="form-control" placeholder="Question Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6 option-name">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group form-default d-flex align-items-center">
                                    <span class="indentity-txt mr-2">1.</span>
                                            <input type="text" name="option[]" class="form-control" placeholder="Option Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn remove-option"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 option-name">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group form-default d-flex align-items-center">
                                    <span class="indentity-txt mr-2">2.</span>
                                            <input type="text" name="option[]" class="form-control" placeholder="Option Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn remove-option"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 option-name">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group form-default d-flex align-items-center">
                                    <span class="indentity-txt mr-2">3.</span>
                                            <input type="text" name="option[]" class="form-control" placeholder="Option Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn remove-option"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 option-name ">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group form-default d-flex align-items-center">
                                    <span class="indentity-txt mr-2">4.</span>
                                            <input type="text" name="option[]" class="form-control" placeholder="Option Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn remove-option"><i class="ti-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group form-default d-flex align-items-center mb-3">
                                    <span style="visibility: hidden;" class="indentity-txt mr-2">4.</span>
                                <button class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn add-new-optiion"  type="button"><i class="ti-slice"></i>Add Option</button>
                            </div>
                            <div class="col-sm-11">
                                <div class="form-group form-default d-flex align-items-center">
                                    <span class="indentity-txt mr-2">Answer:</span>
                                    <input type="text" name="answer" class="form-control" placeholder="Answer" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary waves-effect waves-light fl-right" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
