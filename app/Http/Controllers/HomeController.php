<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomeContent;
use Validator;
use App\Quiz;
use Carbon\Carbon;

class HomeController extends Controller
{
    
    /**
     * user home page function
     */
    public function index()
    {
        $data['contents'] = HomeContent::all();
        return view('quiz.frontend.pages.index',$data);
    }
    public function singleContent( $id )
    {
        $data['content'] = HomeContent::findOrFail( $id );
        $data['quizes'] = $this->recentQuiz($data['content']->status);
        $data['upcomeing'] = $this->upcomeingQuiz($data['content']->status);
        return view('quiz.frontend.pages.single-content',$data);
    }

    public function recentQuiz($status)
    {
        return Quiz::whereDate('end_date','<',Carbon::now())
            ->where('status',$status)
            ->orderBy('end_date','desc')
            ->limit(4)
            ->get();
    }
    public function upcomeingQuiz($status)
    {
        return Quiz::whereDate('start_date','>',Carbon::now())
            ->whereDate('end_date','>',Carbon::now())
            ->where('status',$status)
            ->orderBy('end_date','asc')
            ->limit(4)
            ->get();
    }


    /**
     * Admin function for home page
     */
    public function adminHome()
    {
        return view('quiz.admin.home.index');
    }
    public function adminHomeSave(Request $request)
    {
        //dd($request->all());

        if(!$request->id){
            $find = HomeContent::where('status', $request->category)->first();
            if($find){
                return redirect()->back()->with('error','Content already Exist');
            }
        }else{
            $find = HomeContent::where('status', $request->category)->whereNotIn('id',[$request->id])->first();
            if($find){
                return redirect()->back()->with('error','Content already Exist');
            }
        }
        $flag = 0;
        $fileName = '';
        if($request->file){
            $validator = Validator::make($request->all(), [
                'file' => 'mimes:jpeg,jpg,png',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->with('error','The files must be a file of type: jpeg, jpg, png');
            }

            $file = $request->file('file');
            $extention = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extention;
            $path = public_path('homeImg');
            $file->move($path,$fileName);
            $flag  = 1;
        }

        if(!$request->id){
            $hsave = new HomeContent();
            $hsave->title = $request->title;
            $hsave->content = $request->content;
            $hsave->status = $request->category;
            if($flag == 1){
                $hsave->img = $fileName;
            }
            $hsave->save();
            return redirect()->back()->with('success','Content successfully saved');
        }

        if($request->id){
            $hsave = HomeContent::find($request->id);
            $hsave->title = $request->title;
            $hsave->content = $request->content;
            $hsave->status = $request->category;
            if($flag == 1){
                $hsave->img = $fileName;
            }
            $hsave->save();
            return redirect()->back()->with('success','Content successfully Updated');
        }

    }

    public function editContent(Request $request)
    {
        $data['content'] = HomeContent::find($request->id);
        $data['status'] = 2;
        return view('quiz.admin.home.add',$data)->render();
    }

    public function getHomeContent(Request $request)
    {
        $columns = array( 
            0 => 'img', 
            1 => 'title', 
            2 => 'content',
            3 => 'id',

        );
  
        $totalData = HomeContent::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = HomeContent::offset($start)
                 ->limit($limit)
                 ->orderBy('id','desc')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = HomeContent::where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('title', 'LIKE',"%{$search}%")
                        ->orWhere('content', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = HomeContent::where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('title', 'LIKE',"%{$search}%")
                        ->orWhere('content', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $nestedData['title'] = $post->title;
                $content = '';
                if(strlen($post->content) > 100){
                    $content = substr($post->content, 0, 100).'....';
                }else{
                    $content = $post->content;
                }

                $nestedData['img'] = '';
                if($post->img){
                    $imgUrl = url('homeImg/'.$post->img);
                    $nestedData['img'] = "<img src='".$imgUrl."' style='width:50px'>";
                }
                $nestedData['content'] = $content;
                $nestedData['options'] = '
                <button content-id="'.$post->id.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn edt-content-btn" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-slice"></i></button>
                <button content-id="'.$post->id.'" class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn content-dlt-btn" data-toggle="tooltip" data-placement="top" title="Delete"><i class="ti-trash"></i></button>
                ';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }

    public function getHomeContentDelete(Request $request)
    {
        HomeContent::where('id',$request->id)->delete();
        return 'success';
    }


    
    
}
