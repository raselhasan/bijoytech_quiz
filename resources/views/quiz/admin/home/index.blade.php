@extends('quiz.admin.layout.layout')

@section('title')
    Home Content
@endsection

@section('cssfile')
     <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('style')
    <style type="text/css">
        .ck-editor__main{
            color: black;
        }
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Home page content</h5>
                        <p class="m-b-0">All home page content. Modify your home page content</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="add-content">
                        @include('quiz.admin.home.add')
                    </div>
                    @include('quiz.admin.home.table')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsfile')
    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/ckeditor.js"></script>
@endsection

@section('script')
    @if(Session::has('success'))
        <script>
            toastr["success"]("Saved!");
        </script>
    @endif
    @if(Session::has('error'))
        <script>
            toastr["error"]('{{Session::get("error")}}');
        </script>
    @endif
    <script type="text/javascript">
        ClassicEditor.create( document.querySelector( '#home-content' ),{} );
        var table;
        $(document).ready(function () {
           table = $('#order-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                     "url": "{{ route('admin.home.content') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "img" },
                    { "data": "title" },
                    { "data": "content" },
                    { "data": "options" } 
                ]    

            });
        });
        $(document).on('click','.content-dlt-btn', function(){
            var id = $(this).attr('content-id');
            $.confirm({
                title: 'Alert!',
                content: 'Are you sure to delete this item?',
                buttons: {
                    confirm: function () {
                        var data = new FormData();
                        data.append('id',id);
                        data.append('_token',window.token);
                        $.ajax({
                            processData: false,
                            contentType: false,
                            data: data,
                            type: 'POST',
                            url: '{{route("admin.home.content.delete")}}',
                            success: function(response) {
                                console.log(response);
                                table.ajax.reload(null, false);
                                toastr["success"]("Deleted!");
                            }
                        });
                    },
                    cancel: function () {
                    
                    },
                }
            });
        })
        $(document).on('click','.edt-content-btn', function(){
            var id = $(this).attr('content-id');
            var data = new FormData();
            data.append('id',id);
            data.append('_token',window.token);
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: '{{route("admin.home.content.edit")}}',
                success: function(response) {
                    $('.add-content').html(response);
                    ClassicEditor.create( document.querySelector( '#home-content' ),{} );
                    $(window).scrollTop(200);
                    console.log(response);
                }
            });
        })

    </script>

@endsection                        