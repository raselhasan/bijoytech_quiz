<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->dateTime('start_date');
            $table->string('start_time');
            $table->dateTime('end_date');
            $table->string('end_time');
            $table->float('price')->nullable();
            $table->integer('status')->default(1);
            $table->string('grade');
            $table->integer('person')->nullable();
            $table->integer('distribution')->nullable();
            $table->integer('active_bangla')->nullable();
            $table->integer('active_english')->nullable();
            $table->float('mark');
            $table->integer('total_quistion')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
