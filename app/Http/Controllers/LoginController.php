<?php

namespace App\Http\Controllers;


use Hash;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    public function userLogin()
    {
        return view('quiz.frontend.auth.login');
    }
    public function login(Request $request)
    {
        $role = 1;

        if($request->role){
            $role = $request->role; //send role = 2 if you are a user
        }
    	$check = User::where('email',$request->email)->where('role',$role)->first();
    	if(!$check){
    		return redirect()->back()->with('error','Authentication Failed')->withInput($request->input());
    	}
    	$credentials = [
		    'email' => $request->email,
		    'password' => $request->password,
		];

	   	if (Auth::attempt($credentials)) {
            if($role == 1){
                return redirect()->route('admin.users');
            }else{
                if(Session::has('redirect_id')){
                    $red_id = Session::get('redirect_id');
                    Session::forget('redirect_id');
                    return redirect()->route('user.quiz',['status'=>$red_id]);
                }
                return redirect()->route('user.profile');
            }
	    }else{
	    	return redirect()->back()->with('error','Authentication Failed')->withInput($request->input());
	    }
    }
}
