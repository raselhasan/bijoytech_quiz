<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Languages</h5>
            <span>Please select language before adding question</span>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Select Language</th>
                            <th>Status</th>
                            <th>Language</th>
                        </tr>
                    </thead>
                    <tbody id="set-all-languages">
                        @include('quiz.admin.question.language.single-list')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

