<div class="card">
    <div class="card-header">
        <h5>Payment History</h5>
        <span>Sent payment histories</span>
    </div>
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="order-table" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Acc. Number</th>
                        <th>Payment Method</th>
                        <th>Payment Id</th>
                        <th>Sent Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Acc. Number</th>
                        <th>Payment Method</th>
                        <th>Payment Id</th>
                        <th>Sent Amount</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>