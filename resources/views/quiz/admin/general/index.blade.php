@extends('quiz.admin.layout.layout')

@section('title')
    General Quiz Compitision
@endsection

@section('cssfile')
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('style')
    <style type="text/css">
        
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">General Quiz Compitision</h5>
                        <p class="m-b-0">Here available general quiz compitision quiz list</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn waves-effect waves-light btn-primary btn-skew fl-right" data-toggle="modal" data-target="#add-quiz-modal">Add Quiz</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('quiz.admin.general.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('quiz.admin.global.add-quiz')
    <div class="set-edit-quiz"></div>
    
@endsection

@section('jsfile')
    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {
           table = $('#order-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                     "url": "{{ route('admin.general.quiz.getPost') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "title" },
                    { "data": "start_date" },
                    { "data": "start_time" },
                    { "data": "end_date" },
                    { "data": "end_time" },
                    { "data": "price" },
                    { "data": "options" } 
                ]    

            });
        });

        $(document).on('submit','#update-quiz',function(e){
            e.preventDefault();
            var link = $(this).attr('action');
            var data = new FormData( $( 'form#update-quiz' )[ 0 ] );
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: link,
                success: function(response) {
                   if(response == 'success'){
                        $('#edit-quiz-modal').modal('hide');
                        table.ajax.reload(null, false);
                        toastr["success"]("Quiz Updated");
                   }
                }
            });
        });

        $(document).on('click','.quiz-dlt-btn', function(){
            var id = $(this).attr('quiz-id');
            $.confirm({
                title: 'Alert!',
                content: 'Are you sure to delete this item?',
                buttons: {
                    confirm: function () {
                        var data = new FormData();
                        data.append('id',id);
                        data.append('_token',window.token);
                        $.ajax({
                            processData: false,
                            contentType: false,
                            data: data,
                            type: 'POST',
                            url: '{{route("admin.quiz.delete")}}',
                            success: function(response) {
                                console.log(response);
                                table.ajax.reload(null, false);
                                toastr["success"]("Deleted!");
                            }
                        });
                    },
                    cancel: function () {
                    
                    },
                }
            });
        }) 
    </script>
@endsection                        