<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function questions()
    {
        return $this->hasMany(Question::class,'variation_id','id')->orderBy('id','desc');
    }
}
