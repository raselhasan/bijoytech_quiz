<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Quiz;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function startQuiz($status = null)
    {
    	if($status == ''){
    		return redirect()->back();
    	}
    	$data['status'] = $status;
    	$data['quizes'] = $this->getQuiz($status);
    	//dd($data['quizes']->toArray());
    	return view('quiz.frontend.pages.quiz.quiz',$data);
    }
    public function getQuiz($status)
    {
    	return Quiz::whereDate('start_date','<',Carbon::now())
    		->whereDate('end_date','>',Carbon::now())
    		->where('status',$status)
    		->where('grade',auth()->user()->grade)
    		->get();
    }
    public function showBooks($quiz_id, $status)
    {
    	$data['quiz_id'] = $quiz_id;
    	$data['status'] = $status;
        if($status == 3){
            $data['quiz'] = Quiz::where('id',$quiz_id)->with('active_books')->first();
        }else if($status == 4){
            $data['quiz'] = Quiz::where('id',$quiz_id)->with('active_videos')->first();
        }else if($status == 1 || $status == 2){
            $data['quiz'] = Quiz::where('id',$quiz_id)->first();
        }
    	
    	//dd($data['quiz']->toArray());
    	return view('quiz.frontend.pages.books.index',$data);
    }
}
