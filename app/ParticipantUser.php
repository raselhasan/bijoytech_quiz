<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantUser extends Model
{
    public function book()
    {
    	return $this->belongsTo(Book::class, 'book_id','id');
    }
    public function video()
    {
        return $this->belongsTo(Video::class, 'book_id','id');
    }
    public function quiz()
    {
    	return $this->belongsTo(Quiz::class, 'quiz_id','id');
    }
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id','id');
    }
}
