<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">
			@if(@$status == 1)
				General Quiz Compitision
			@endif
			@if(@$status == 2)
				Spelling Quiz Compitision
			@endif
			@if(@$status == 3)
				Reading Quiz Compitision
			@endif
			@if(@$status == 4)
				Video Quiz Compitision
			@endif
			@if(\Request::route()->getName() == 'user.profile')
				Update Profile
			@endif
			@if(\Request::route()->getName() == 'user.score')
				My Quiz Scores
			@endif	
			@if(\Request::route()->getName() == 'user.payment-request')
				Payment Request
			@endif	
			@if(\Request::route()->getName() == 'user.change-password')
				Change Password
			@endif	

				
		</li>
	</ol>
</nav>