@extends('quiz.frontend.layout.app')
@section('style')

@endsection
@section('content')
<section class="main-section">
	<div class="log-cnt">
		<div class="row mx-0">
			<div class="col-6 px-0">
				<div class="form">
					<h4 class="text-center mb-4 text-uppercase">Forgot password</h5>
					@if(Session::has('error'))
						<p class="err-msg">{{Session::get('error')}}</p>
					@endif
					<form action="{{route('user.forgotPassword')}}" method="post">
						@csrf
						<div class="form-group">
							<label class="mb-2">Email</label>
							<input type="email" class="form-control" value="{{old('email')}}" name="email" required />
						</div>
						<div class="form-group mt-4">
							<button type="submit" class="btn btn-primary btn-block">Send</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-6 px-0">
				<div class="lr-cover">
					<img class="cover"  src="{{asset('files/assets/images/log.jpg')}}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')

@endsection
