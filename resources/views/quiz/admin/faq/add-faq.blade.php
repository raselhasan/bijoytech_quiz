<form action="{{route('admin.qna.add')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add QNA</h5>
                    <span>Add New QNA page content here</span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <input type="hidden" name="id" value="{{@$qna->id}}">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Question</label>
                                <input type="text" name="question" class="form-control" value="{{@$qna->question}}" required>
                            </div>
                        </div>
                              
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Content</label>
                                <textarea id="qna-content" name="answer">{{@$qna->answer}}</textarea>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>