<div class="modal fade book-modal" id="readBookModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h5 class="modal-title" id="exampleModalLabel">Read Book</h5>
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true"><i class="fal fa-times"></i></span>
	        	</button>
	      	</div>
		    <div class="modal-body">
				<iframe class="setBookUrl" src="http://www.adventistchurchconnect.com/site/1/docs/test.pdf" width="100%" height="500">
				</iframe>
		     </div>
    	</div>
  	</div>
</div> 