<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
class VideoQuizController extends Controller
{
    public function index()
    {
    	return view('quiz.admin.video.index');
    }

    public function getPost(Request $request)
    {
    	 $columns = array( 
            0 => 'title', 
            1 => 'start_date',
            2 => 'start_time',
            3 => 'end_date',
            4 => 'end_time',
            5 => 'price',
            6 => 'id',

        );
  
        $totalData = Quiz::where('status',4)->count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = Quiz::where('status',4)->offset($start)
                 ->limit($limit)
                 ->orderBy('id','desc')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = Quiz::where('status',4)
                ->where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('title', 'LIKE',"%{$search}%")
                        ->orWhere('start_date', 'LIKE',"%{$search}%")
                        ->orWhere('start_time', 'LIKE',"%{$search}%")
                        ->orWhere('end_date', 'LIKE',"%{$search}%")
                        ->orWhere('end_time', 'LIKE',"%{$search}%")
                        ->orWhere('price', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Quiz::where('status',4)
                ->where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('title', 'LIKE',"%{$search}%")
                        ->orWhere('start_date', 'LIKE',"%{$search}%")
                        ->orWhere('start_time', 'LIKE',"%{$search}%")
                        ->orWhere('end_date', 'LIKE',"%{$search}%")
                        ->orWhere('end_time', 'LIKE',"%{$search}%")
                        ->orWhere('price', 'LIKE',"%{$search}%");
                })
	            ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $nestedData['title'] = $post->title;
                $nestedData['start_date'] = date('m/d/Y', strtotime($post->start_date));
                $nestedData['start_time'] = date('h:i A',strtotime($post->start_time));
                $nestedData['end_date'] = date('m/d/Y', strtotime($post->end_date));
                $nestedData['end_time'] = date('h:i A',strtotime($post->end_time));
                $nestedData['price'] = '$ '.number_format($post->price, 2);
                $url = route('admin.question.add').'/'.$post->id.'/4';
                $participant_url = route('admin.video.participant',['quiz_id'=>$post->id,'status'=>$post->status]);
                $nestedData['options'] = '<a href="'.$url.'" class="btn waves-effect waves-dark btn-success btn-outline-success edit-del-btn" data-toggle="tooltip" data-placement="top" title="Add Questions"><i class="ti-plus"></i></a>
                <button quiz-id="'.$post->id.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn edt-quiz-btn" data-toggle="tooltip" data-placement="top" title="Edit Quiz"><i class="ti-slice"></i></button>
                <button quiz-id="'.$post->id.'" class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn quiz-dlt-btn" data-toggle="tooltip" data-placement="top" title="Delete Quiz"><i class="ti-trash"></i></button>
                <a href="'.$participant_url.'" class="btn waves-effect waves-dark btn-success btn-outline-success edit-del-btn" data-toggle="tooltip" data-placement="top" title="View Participants"><i class="ti-user"></i></a>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }
}
