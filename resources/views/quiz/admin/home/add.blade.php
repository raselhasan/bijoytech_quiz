<form action="{{route('admin.home')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add New Content</h5>
                    <span>Add New home page content here</span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <input type="hidden" name="id" value="{{@$content->id}}">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Title</label>
                                <input type="text" name="title" class="form-control" required="" value="{{@$content->title}}">
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Image</label>
                                <input type="file" name="file" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Category</label>
                                <select class="form-control" name="category" id="exampleFormControlSelect1" required>
                                  <option value="">--select--</option>
                                  <option value="1" @if(@$content->status == 1) selected @endif>General Quiz Competition</option>
                                  <option value="2" @if(@$content->status == 2) selected @endif>Spelling Quiz Competition</option>
                                  <option value="3" @if(@$content->status == 3) selected @endif>Reading Quiz Competition</option>
                                  <option value="4" @if(@$content->status == 4) selected @endif>Video Quiz Competition</option>
                                </select>
                            </div>
                       </div>    
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Content</label>
                                <textarea id="home-content" name="content">{{@$content->content}}</textarea>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>