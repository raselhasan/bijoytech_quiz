@extends('quiz.admin.layout.layout')

@section('title')
    Money Withdrawal Request
@endsection

@section('cssfile')
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('style')
    <style type="text/css">
        
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Money Withdrawal Request</h5>
                        <p class="m-b-0">Participant user are sent to money withdrawal request</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('quiz.admin.withdrawal-request.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="set-make-paid"></div>
    
@endsection

@section('jsfile')
    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {
           table = $('#order-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                     "url": "{{ route('admin.withdrawal.request') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "created_at" },
                    { "data": "name" },
                    { "data": "phone" },
                    { "data": "current_balance" },
                    { "data": "requested_amount" },
                    { "data": "paid" },
                    { "data": "paid_status" },
                    { "data": "options" } 
                ]    

            });
        });

        $(document).on('click','.edt-money-btn', function(){
            var id = $(this).attr('w-request-id');
            var data = new FormData();
            data.append('id',id);
            data.append('_token',window.token);
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: '{{route("admin.withdrawal.request.get")}}',
                success: function(response) {
                    $('.set-make-paid').html(response);
                    $('#make-paid-modal').modal('show');
                }
            });

        });    

        $(document).on('submit','#make-paid-form',function(e){
            e.preventDefault();
            var link = $(this).attr('action');
            var data = new FormData( $( 'form#make-paid-form' )[ 0 ] );
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: link,
                success: function(response) {
                   if(response == 'success'){
                        $('#make-paid-modal').modal('hide');
                        table.ajax.reload(null, false);
                        toastr["success"]("Saved!");
                   }
                }
            });
        });

        $(document).on('click','.money-dlt-btn', function(){
            var id = $(this).attr('w-request-id');
            $.confirm({
                title: 'Alert!',
                content: 'Are you sure to delete this item?',
                buttons: {
                    confirm: function () {
                        var data = new FormData();
                        data.append('id',id);
                        data.append('_token',window.token);
                        $.ajax({
                            processData: false,
                            contentType: false,
                            data: data,
                            type: 'POST',
                            url: '{{route("admin.withdrawal.request.delete")}}',
                            success: function(response) {
                                console.log(response);
                                table.ajax.reload(null, false);
                                toastr["success"]("Deleted!");
                            }
                        });
                    },
                    cancel: function () {
                    
                    },
                }
            });
        }) 
    </script>
@endsection                        