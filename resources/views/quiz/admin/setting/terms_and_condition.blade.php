<form action="{{route('admin.setting')}}" method="post" class="saveSetting">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Terms And Condition</h5>
                    <span></span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <div class="card-block">
                    <div class="form-group form-default">
                        <label class="float-label">Content</label>
                        <textarea id="terms_and_condition" name="terms_and_condition">{{@$setting->terms_and_condition}}</textarea>
                    </div>
                    <button class="btn waves-effect waves-light btn-primary btn-skew fl-right" type="submit">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>