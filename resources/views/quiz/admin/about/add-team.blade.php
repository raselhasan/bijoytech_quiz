<form action="{{route('admin.team.add')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Team</h5>
                    <span>Add New team page content here</span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <input type="hidden" name="id" value="{{@$team->id}}">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Name</label>
                                <input type="text" name="name" class="form-control" value="{{@$team->name}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Designation</label>
                                <input type="text" name="deg" class="form-control" value="{{@$team->deg}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Image</label>
                                <input type="file" name="file" class="form-control" @if(@!$team->id) required @endif>
                            </div>
                        </div>
                              
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Description</label>
                                <textarea id="team-content" name="desc">{{@$team->desc}}</textarea>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>