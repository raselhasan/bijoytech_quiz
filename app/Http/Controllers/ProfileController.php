<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Country;
use Hash;
class ProfileController extends Controller
{
    public function index()
    {
    	$data['profile'] = User::find(auth()->user()->id);
        $data['countries'] = Country::all();
    	return view('quiz.frontend.pages.profile',$data);
    }
    public function updateProfile( Request $request )
    {
    	$rules = array(
            'email' => 'required|unique:users,email,'.auth()->user()->id,
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return redirect()->back()->with('error','Email already been taken!')->withInput($request->all());
        }
        $user = User::find(auth()->user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->zip = $request->zip;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->grade = $request->grade;
        $user->guardian = $request->guardian;
        $user->relation_with_guardian = $request->relation_with_guardian;
        $user->date_of_birth = $request->date_of_birth;
        $user->guardian_phone = $request->guardian_phone;
        $user->guardian_email = $request->guardian_email;
        $user->save();
        return redirect()->back()->with('success','Profile successfully updated');
    }

    public function uploadProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_img' => 'mimes:jpeg,jpg,png',
        ]);
        if ($validator->fails()) {
            return redirect()->back();
        }
        $path = public_path('user_image');
        $file = $request->file('profile_img');
        $extention = $file->getClientOriginalExtension();
        $fileName = time().'.'.$extention;
        $file->move($path,$fileName);

        $user = User::find(auth()->user()->id);
        $user->image = $fileName;
        $user->save();
        return redirect()->back();
    }

    public function changePassword()
    {
        return view('quiz.frontend.pages.change-password.index');
    }
    public function savePassword(Request $request)
    {
        if($request->new_password != $request->r_new_password){
            return redirect()->back()->with('error','New passwod does not match with repeat new password');
        }
        $user = User::find(auth()->user()->id);
        $user->password = Hash::make($request->new_password);
        $user->save();
        return redirect()->back()->with('success','Password has been successfully changed');
    }
}
