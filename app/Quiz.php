<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function books()
    {
        return $this->hasMany(Book::class)->orderBy('id','desc');
    }
     public function videos()
    {
        return $this->hasMany(Video::class)->orderBy('id','desc');
    }
    public function active_books()
    {
        return $this->hasMany(Book::class)->where('active',1)->orderBy('id','desc');
    }
    public function active_videos()
    {
        return $this->hasMany(Video::class)->where('active',1)->orderBy('id','desc');
    }
}
