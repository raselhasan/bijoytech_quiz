<div class="left-sidebar">
	<div class="profile-cnt">
		<form action="{{route('user.profilePic')}}" method="post" enctype="multipart/form-data" id="piuform">
			@csrf
			<div class="profile-img">
				<div class="img-upload">
					<input type="file" name="profile_img" class="profile_img">
					<i class="fas fa-camera"></i>
				</div>
				@if(auth()->user()->image)
					<img class="cover"  src="{{asset('user_image/'.auth()->user()->image)}}">
				@else 
					<img class="cover"  src="{{asset('user_image/default.jpg')}}">
				@endif	
			</div>
		</form>
		<div class="profile-txt mb-2">{{Auth::user()->name}}</div>
		<div class="profile-txt bal">Balance: ${{Helper::currentAmount()}}</div>
	</div>
	<nav class="side_nav"> 
		<ul class="side_ul">
			<li class="side_nav__items side_nav__items_out @if(\Request::route()->getName() == 'user.profile') user-menu-active @endif">
				<a href="{{route('user.profile')}}" class="side_nav__links side_nav__links_out">My profile Information</a>
			</li>
			<li class="side_nav__items side_nav__items_out @if(@$status == 1) user-menu-active @endif">
				<a href="{{route('user.quiz', 1)}}" class="side_nav__links side_nav__links_out">General Quiz Competition</a>
			</li>
			<li class="side_nav__items side_nav__items_out @if(@$status == 2) user-menu-active @endif">
				<a href="{{route('user.quiz', 2)}}" class="side_nav__links side_nav__links_out">Spelling Quiz Competition</a>
			</li>
			<li class="side_nav__items side_nav__items_out @if(@$status == 3) user-menu-active @endif">
				<a href="{{route('user.quiz',3)}}" class="side_nav__links side_nav__links_out">Reading Quiz Competition</a>
			</li>
			<li class="side_nav__items side_nav__items_out @if(@$status == 4) user-menu-active @endif">
				<a href="{{route('user.quiz',4)}}" class="side_nav__links side_nav__links_out">Video Quiz Competition</a>
			</li>
			
			<li class="side_nav__items side_nav__items_out @if(\Request::route()->getName() == 'user.score') user-menu-active @endif">
				<a href="{{route('user.score')}}" class="side_nav__links side_nav__links_out">My Quize Scores</a>
			</li>
			<li class="side_nav__items side_nav__items_out @if(\Request::route()->getName() == 'user.payment-request') user-menu-active @endif">
				<a href="{{route('user.payment-request')}}" class="side_nav__links side_nav__links_out">Payment Request</a>
			</li>

			<li class="side_nav__items side_nav__items_out @if(\Request::route()->getName() == 'user.change-password') user-menu-active @endif">
				<a href="{{route('user.change-password')}}" class="side_nav__links side_nav__links_out">Change Password</a>
			</li>

		</ul>
	</nav>
</div>