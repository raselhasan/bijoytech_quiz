@extends('quiz.frontend.layout.app')

@section('title')
	@if(@$status == 1)
		General Quiz Compitision
	@endif
	@if(@$status == 2)
		Spelling Quiz Compitision
	@endif
	@if(@$status == 3)
		Reading Quiz Compitision
	@endif
	@if(@$status == 4)
		Video Quiz Compitision
	@endif
@endsection

@section('style')

@endsection
@section('content')
	<section class="main-section p-0"> 
		<div class="row m-0">
			<div class="col-12 col-lg-2 p-0">
				@include('quiz.frontend.include.left-bar')
			</div>
			<div class="col-12 col-lg-10 p-0">
				<div class="main-body">
					@include('quiz.frontend.pages.quiz.bradcam')
					<div class="row mr-0">
						@include('quiz.frontend.pages.quiz.list')
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')

@endsection
