@extends('quiz.admin.layout.layout')

@section('title')
    Content
@endsection

@section('cssfile')

@endsection

@section('style')
    <style type="text/css">
        .ck-editor__editable {
            min-height: 300px;
        }
        .ck-editor__main{
            color: black;
        }
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Contact page content</h5>
                        <p class="m-b-0">All Contact page content. Modify your contact page content</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="add-contact">
                        @include('quiz.admin.contact.add')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsfile')
    <script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/ckeditor.js"></script>
@endsection

@section('script')
    @if(Session::has('success'))
        <script>
            toastr["success"]("Saved!");
        </script>
    @endif
    <script>
        ClassicEditor.create( document.querySelector( '#contact-content' ),{} );
    </script>
@endsection                        