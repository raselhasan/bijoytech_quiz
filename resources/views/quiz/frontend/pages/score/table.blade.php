<div class="col-md-12">
	<table class="table table-dark">
	  	<thead>
	    	<tr>
	      		<td>Date</td>
	      		<td>Quiz Title</td>
	      		<td>Book/Video</td>
	      		<td>Language</td>
	      		<td>Right</td>
	      		<td>Worng</td>
	      		<td>Score Got</td>
	      		<td>Total Mark</td>
	      		<td>Position</td>
	      		<td>Prize Money</td>
	    	</tr>
	  </thead>
	  <tbody id="set-score-data">
	  		@include('quiz.frontend.pages.score.list')    	
	  	</tbody>
	</table>
	<div id="pagination-data">
		@include('quiz.frontend.pages.score.pagination')  
	</div>
	
</div>