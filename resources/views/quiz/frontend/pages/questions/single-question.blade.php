<div class="card mb-3" style="padding: 20px;">
    <div class="card-header p-3" style="color: #fff;">
        <div class="pre-q d-block mb-2">Question- {{$quesNumber + 1}} : <span class="float-right">
            <h1 class='timer' data-minutes-left=1></h1>
        </span></div>
        <h5 class="mb-0 qsTitle">{{$question['question']}}</h5>
    </div>
    <input type="hidden" name="cQuestionId" class="cQuestionId" value="{{$question['id']}}">
    <div class="card-block pt-0">
        <div class="row mr-0">
            @if(count($question['options']) > 0)
                @foreach($question['options']  as $key=>$option)
                    <div class="col-12 col-md-6 pr-0 mb-3 everyQuestionOption">
                        <span class="pre-q">{{$key + 1}}.</span>
                        <label class="ctm-container optionRadio">
                            {{$option['option']}}
                            <input type="radio" name="radio" value="{{$option['option']}}" />
                            <span class="checkmark"></span>
                        </label>
                    </div>
                @endforeach
            @else 
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="mb-2">Answer</label>
                        <input type="text" class="form-control givenAns" name="answer" value="">
                    </div>
                </div>    

            @endif        
            
        </div>
    </div>
</div>
<div class="text-right  mt-4 mb-5">
    <button onclick="gtQuestion({{$quesNumber + 1}})" class="btn btn-primary">Next  <i class="fal fa-arrow-right"></i>
    </button>
</div>