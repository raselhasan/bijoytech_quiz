<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ParticipantUser;
class ScoreController extends Controller
{
    public function index(Request $request)
    {
    	$search = $request->search;
    	if(!empty($search)){
	    	$data['scores'] = ParticipantUser::select(
	    			'participant_users.*',
	    			'users.name as user_name',
	    			'users.id as user_id',
	    			'books.title as book_title',
	    			'books.language as book_language',
	    			'videos.title as video_title',
	    			'videos.language as video_language',
	    			'quizzes.title as quiz_title',
                    'quizzes.mark as mark',
                    'quizzes.total_quistion as total_quistion'
	    		)
	            ->where('participant_users.user_id',auth()->user()->id)
	            ->join("users","users.id","=","participant_users.user_id")
	            ->leftJoin("quizzes","quizzes.id","=","participant_users.quiz_id")
	            ->leftJoin("books","books.id","=","participant_users.book_id")
	            ->leftJoin("videos","videos.id","=","participant_users.book_id")
	            ->where(function($query) use ($search){
	                $query->where('participant_users.id','LIKE',"%{$search}%")
	                    ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('quizzes.mark', 'LIKE',"%{$search}%")
                        ->orWhere('quizzes.total_quistion', 'LIKE',"%{$search}%")
	                    ->orWhere('books.title', 'LIKE',"%{$search}%")
	                    ->orWhere('videos.title', 'LIKE',"%{$search}%")
	                    ->orWhere('users.name', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.language', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.total_mark', 'LIKE',"%{$search}%")
	                    ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
	            })
	            ->paginate(10);
	            $params = array('search' => $search);
	            return view('quiz.frontend.pages.score.index',$data, compact('params','search'));
        }


    	$data['scores'] = ParticipantUser::select(
	    			'participant_users.*',
	    			'users.name as user_name',
	    			'users.id as user_id',
	    			'books.title as book_title',
	    			'books.language as book_language',
	    			'videos.title as video_title',
	    			'videos.language as video_language',
	    			'quizzes.title as quiz_title',
                    'quizzes.mark as mark',
                    'quizzes.total_quistion as total_quistion'
    			)
            ->where('participant_users.user_id',auth()->user()->id)
            ->join("users","users.id","=","participant_users.user_id")
            ->leftJoin("quizzes","quizzes.id","=","participant_users.quiz_id")
            ->leftJoin("books","books.id","=","participant_users.book_id")
            ->leftJoin("videos","videos.id","=","participant_users.book_id")
    		->paginate(10);
    	return view('quiz.frontend.pages.score.index',$data);
    }

    public function search( Request $request)
    {
    	$search = $request->search;
    	$data['scores'] = ParticipantUser::select(
    			'participant_users.*',
    			'users.name as user_name',
    			'users.id as user_id',
    			'books.title as book_title',
    			'books.language as book_language',
    			'videos.title as video_title',
    			'videos.language as video_language',
    			'quizzes.title as quiz_title',
                'quizzes.mark as mark',
                'quizzes.total_quistion as total_quistion'
    		)
            ->where('participant_users.user_id',auth()->user()->id)
            ->join("users","users.id","=","participant_users.user_id")
            ->leftJoin("quizzes","quizzes.id","=","participant_users.quiz_id")
            ->leftJoin("books","books.id","=","participant_users.book_id")
            ->leftJoin("videos","videos.id","=","participant_users.book_id")
            ->where(function($query) use ($search){
                $query->where('participant_users.id','LIKE',"%{$search}%")
                    ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                    ->orWhere('quizzes.mark', 'LIKE',"%{$search}%")
                    ->orWhere('quizzes.total_quistion', 'LIKE',"%{$search}%")
                    ->orWhere('books.title', 'LIKE',"%{$search}%")
                    ->orWhere('videos.title', 'LIKE',"%{$search}%")
                    ->orWhere('users.name', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.language', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.total_mark', 'LIKE',"%{$search}%")
                    ->orWhere('participant_users.date', 'LIKE',"%{$search}%");

            })
            ->paginate(10);

            $params = array('search' => $search);
            $list = view('quiz.frontend.pages.score.list',$data)->render();
            $pagi = view('quiz.frontend.pages.score.pagination',$data,compact('params'))->render();
            return response()->json(['list'=>$list,'pagi'=>$pagi]);
    }
}
