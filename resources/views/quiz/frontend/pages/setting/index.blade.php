@extends('quiz.frontend.layout.app')

@section('title')
	@if($name == 'aboutus')
		About Us
	@elseif( $name == 'contactus')
		Contact Us
	@elseif( $name == 'faq')
		Faq
	@elseif( $name == 'privacy-policy')
		Privacy Policy
	@elseif( $name == 'terms-and-conditions')
		Terms and Conditions
	@endif
@endsection

@section('style')
	
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="row mr-0">
		<div class="col-12 col-md-12 col-lg-12 pr-0">
			@if($name == 'aboutus')
				{!! $setting->about_us !!}
			@elseif( $name == 'contactus')
				{!! $setting->contact_us !!}
			@elseif( $name == 'faq')
				{!! $setting->faq !!}
			@elseif( $name == 'privacy-policy')
				{!! $setting->privacy_policy !!}
			@elseif( $name == 'terms-and-conditions')
				{!! $setting->terms_and_condition !!}
			@endif
		</div>
	</div>
</section>
@endsection

@section('script')
	<script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>
   	<script type="text/javascript">
	    document.querySelectorAll( 'oembed[url]' ).forEach( element => {
	        const anchor = document.createElement( 'a' );
	        anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
	        anchor.className = 'embedly-card';

	        element.appendChild( anchor );
	    } );
   	</script>
@endsection
