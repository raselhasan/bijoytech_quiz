@extends('quiz.admin.layout.layout')

@section('title')
    Users
@endsection

@section('cssfile')
     <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
@endsection

@section('style')
    <style type="text/css">
        
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Users</h5>
                        <p class="m-b-0">All Registered user showing here. Please have a look</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('quiz.admin.users.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="set-view-user-modal"></div>
    {{-- @include('quiz.admin.users.view-user') --}}
@endsection

@section('jsfile')
    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/assets/pages/data-table/js/data-table-custom.js')}}"></script>
@endsection

@section('script')



    <script>
    $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
    
        var table;
        $(document).ready(function () {
           table = $('#order-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                     "url": "{{ route('admin.users') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "image" },
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "phone" },
                    { "data": "address" },
                    { "data": "country" },
                    { "data": "options" } 
                ]    

            });
        });

        $(document).on('click','.user-vw-btn',function(e){
            e.preventDefault();

            var id = $(this).attr('user-id');
            var link = '{{route("admin.user.view")}}';
            var data = new FormData();
            data.append('_token','{{csrf_token()}}');
            data.append('id',id);
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: link,
                success: function(response) {

                    $('.set-view-user-modal').html(response);
                    $('#view-user-modal').modal('show');
                }
            });
        });
       
    </script>
@endsection                        