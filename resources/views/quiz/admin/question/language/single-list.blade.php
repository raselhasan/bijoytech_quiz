<tr>
    <td>
        <div class="form-check" onclick="getAllQuestion({{$quiz->id}},{{$status}},'English')">
            <input class="form-check-input" name="language" type="radio" id="exampleRadios1" value="English">
            <label class="form-check-label" for="exampleRadios1">
                Select Language
            </label>
        </div>
    </td>
    <td>
        @if($quiz->active_english == 1)
             <p class="bk-active" onclick="languageActive({{$quiz->id}},'active_english','0')">Active</p>
        @else 
            <p class="bk-dactive" onclick="languageActive({{$quiz->id}},'active_english','1')">Dactive</p>
        @endif     
    </td>
    <td>
        English
    </td>
</tr>
<tr>
    <td>
        <div class="form-check" onclick="getAllQuestion({{$quiz->id}},{{$status}},'Bangla')">
            <input class="form-check-input" name="language" type="radio" id="exampleRadios2" value="Bangla">
            <label class="form-check-label" for="exampleRadios2">
                Select Language
            </label>
        </div>
    </td>
    <td>
        @if($quiz->active_bangla == 1)
             <p class="bk-active" onclick="languageActive({{$quiz->id}},'active_bangla','0')">Active</p>
        @else 
            <p class="bk-dactive" onclick="languageActive({{$quiz->id}},'active_bangla','1')">Dactive</p>
        @endif     
    </td>

    <td>
        Bangla
    </td>
</tr>