<script>
    window.base_url = '<?= url("/") ?>';
    window.token = '<?= csrf_token(); ?>';
    window.edit_book = '<?= route("admin.book.edit"); ?>';
    window.delete_book = '<?= route("admin.book.delete"); ?>';
    window.all_questions = '<?= route("admin.all.question"); ?>';
    window.edit_quiz = '<?= route("admin.quiz.edit"); ?>';
    window.edit_video = '<?= route("admin.video.edit"); ?>';
    window.delete_video = '<?= route("admin.video.delete"); ?>';
    window.book_active = '<?= route("admin.book.active"); ?>';
    window.video_active = '<?= route("admin.video.active"); ?>';
    window.language_active = '<?= route("admin.language.active"); ?>';
    
    
    


</script>
