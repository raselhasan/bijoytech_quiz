@extends('quiz.frontend.layout.app')
@section('style')
    <style type="text/css">
        .hdn-hour{
            display: none;
        }
        .timer{
          display: flex;
        }
    </style>
@endsection
@section('content')
<section class="main-section p-0">
    <div class="row m-0">
        <div class="col-12 col-lg-2 p-0">
            @include('quiz.frontend.include.left-bar')
        </div>
        <div class="col-12 col-lg-10 p-0">
            <div class="main-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        @if($status == 3 || $status == 4)
                            <li class="breadcrumb-item active" aria-current="page">{{Helper::quizName($quiz_id)}}</li>
                            <li class="breadcrumb-item active" aria-current="page">{{Helper::bookName($book_id, $status)}}</li>
                        @endif
                        @if($status == 1)
                            <li class="breadcrumb-item active" aria-current="page">General Quiz Compitision</li>
                            <li class="breadcrumb-item active" aria-current="page">{{@$lang}}</li>
                        @endif
                        @if($status == 2)
                            <li class="breadcrumb-item active" aria-current="page">Spelling Quiz Compitision</li>
                            <li class="breadcrumb-item active" aria-current="page">{{@$lang}}</li>
                        @endif

                    </ol>
                </nav>
                <div class="question-cnt setQuestion">
                     @include('quiz.frontend.pages.questions.single-question')
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('script')
    <script type="text/javascript" src="{{asset('timer/jquery.simple.timer.js')}}"></script>
    <script type="text/javascript">

        var quesNumber = 0;
        setTimer();
        function gtQuestion( quesNumber )
        {
            quesNumber = quesNumber;
            var answer = '';
            var quiz_id = '{{$quiz_id}}';
            var variation_id = '{{$book_id}}';
            var status = '{{$status}}';
            var questionId = $('.cQuestionId').val();
            $('.everyQuestionOption').each(function(){
                if($(this).find('input[type="radio"]:checked').length > 0){
                  answer = $(this).find('input[type="radio"]:checked').val();
                }   
            });
            if(answer == ''){
                answer = $('.givenAns').val();
            }
            var data = new FormData();
            data.append('quesNumber', quesNumber);
            data.append('_token','{{csrf_token()}}');
            data.append('quiz_id', quiz_id);
            data.append('variation_id', variation_id);
            data.append('status', status);
            data.append('questionId', questionId);
            data.append('answer', answer);

            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: "{{route('user.single-question.get')}}",
                success: function(response) {
                    console.log(response);
                    $('.setQuestion').html(response);
                    setTimer();
                }
            });
        }
        function setTimer()
        {
            $('.timer').startTimer({
                classNames: {
                    hours: 'hdn-hour',
                    minutes: 'myClass-minutes',
                    seconds: 'myClass-seconds',
                },
                onComplete: function(element){
                    quesNumber = quesNumber + 1;
                    gtQuestion( quesNumber );
                }
            })
        }

    </script>
@endsection
