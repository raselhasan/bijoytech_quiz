@if(count($scores) > 0)
	@foreach($scores as $score)
		<tr>
	  		<td>{{date('m/d/Y',strtotime($score->date))}}</td>
	  		<td>{{$score->quiz_title}}</td>
	  		<td>
	  			@if($score->status == 3)
	  				@if($score->book_title)
	  					{{$score->book_title}}
	  				@endif
	  			@endif

	  			@if($score->status == 4)
	  				@if($score->video_title)
	  					{{$score->video_title}}
	  				@endif
	  			@endif		
	  		</td>
	  		<td>
	  			@if($score->status == 3)
	  				@if($score->book_language)
	  					{{$score->book_language}}
	  				@endif
	  			@endif

	  			@if($score->status == 4)
	  				@if($score->video_language)
	  					{{$score->video_language}}
	  				@endif
	  			@endif
	  			@if($score->status == 1 || $score->status == 2)
	  				{{$score->language}}
	  			@endif	
	  		</td>
	  		<th>{{$score->total_right_ans}}</th>
	  		<td>{{$score->total_worng_ans}}</td>
	  		<td>{{number_format($score->total_mark,2)}}</td>
	  		<td>{{number_format($score->mark * $score->total_quistion ,2)}}</td>
	  		<td>{{$score->position}}</td>
	  		<td>${{number_format($score->aword, 2)}}</td>
		</tr>
	@endforeach
@endif