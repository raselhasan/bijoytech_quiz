<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('quiz_id');
            $table->unsignedBigInteger('book_id');
            $table->unsignedBigInteger('status');
            $table->unsignedBigInteger('total_right_ans')->nullable();
            $table->unsignedBigInteger('total_worng_ans')->nullable();
            $table->unsignedBigInteger('position')->nullable();
            $table->float('aword')->nullable();
            $table->float('total_mark')->nullable();
            $table->date('date')->nullable();
            $table->string('language')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_users');
    }
}
