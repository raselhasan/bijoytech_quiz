@extends('quiz.admin.layout.layout')

@section('title')
    Setting
@endsection

@section('cssfile')
    
@endsection

@section('style')
    <style type="text/css">
        .ck-editor__main{
            color: black;
        }
        .ck-editor__editable {
            min-height: 300px;
        }
    </style>
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Setting</h5>
                        <p class="m-b-0">Home content setting pages. You can configure your home page setting content</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    @include('quiz.admin.setting.logo')
                    @include('quiz.admin.setting.banner')
                    @include('quiz.admin.setting.terms_and_condition')
                    @include('quiz.admin.setting.privacy-policy')                     
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsfile')
    <script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/classic/ckeditor.js"></script>
@endsection

@section('script')
    <script type="text/javascript">
        ClassicEditor.create( document.querySelector( '#about_us' ),{} );
        ClassicEditor.create( document.querySelector( '#terms_and_condition' ) );
        ClassicEditor.create( document.querySelector( '#privacy_policy' ) );
        ClassicEditor.create( document.querySelector( '#faq' ) );
        ClassicEditor.create( document.querySelector( '#contact_us' ) );

        $(document).on('submit','.saveSetting', function(event){
            event.preventDefault();
            var link = $(this).attr('action');
            var data = new FormData( $(this)[ 0 ] );
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: link,
                success: function(response) {
                    toastr["success"]("Saved!");
                }
            });
        })
        $(document).on('submit','.saveLogo', function(event){
            event.preventDefault();
            var link = $(this).attr('action');
            var data = new FormData( $(this)[ 0 ] );
            $.ajax({
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url: link,
                success: function(response) {
                    if(response.success === true){
                        $('.add-logo-'+response.status).html(response.img);
                        toastr["success"]("Saved!");
                    }
                    
                }
            });
        })

    </script>
@endsection                        