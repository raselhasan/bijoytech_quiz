<?php

namespace App\Http\Controllers;

use App\Book;
use App\Quiz;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function addBook(Request $request, $quiz_id = null)
    {
        $book = new Book;
        $book->quiz_id = $quiz_id;
        $book->title = $request->title;
        $book->language = $request->language;
        $file = $request->file('file');
        $book_name = time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path('books'),$book_name);
        $book->book = $book_name;
        $book->description = $request->description;
        $book->save();

        $data['quiz'] = Quiz::where('id',$quiz_id)->with('books')->first();
        $new_books = view('quiz.admin.question.books.book-list',$data)->render();
        return response()->json(['new_books'=>$new_books, 'success'=>'Book added successfully']);
    }

    public function editBook(Request $request)
    {
        $book = Book::find($request->book_id);
        $book_modal = view('quiz.admin.question.books.edit-book',compact('book'))->render();
        return response()->json(['book_modal'=>$book_modal]);
    }

    public function updateBook(Request $request, $quiz_id, $book_id)
    {
        $book = Book::find($book_id);
        $book->quiz_id = $quiz_id;
        $book->title = $request->title;
        $book->language = $request->language;
        $book->description = $request->description;
        if($request->hasFile('file')){
            $file = $request->file('file');
            $book_name = time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('books'),$book_name);
            unlink(public_path('books/'.$book->book));
            $book->book = $book_name;
        }
        $book->save();
        $data['quiz'] = Quiz::where('id',$quiz_id)->with('books')->first();
        $new_books = view('quiz.admin.question.books.book-list',$data)->render();
        return response()->json(['new_books'=>$new_books, 'success'=>'Book successfully updated']);
    }
    public function deleteBook(Request $request)
    {
        $book = Book::find($request->book_id);
        unlink(public_path('books/'.$book->book));
        $book->delete();

        $data['quiz'] = Quiz::where('id',$request->quiz_id)->with('books')->first();
        $new_books = view('quiz.admin.question.books.book-list',$data)->render();
        return response()->json(['new_books'=>$new_books, 'success'=>'Book successfully deleted']);

    }

    public function activeBook(Request $request)
    {
        $book = Book::find($request->book_id);
        $book->active = $request->status;
        $book->save();
        $data['quiz'] = Quiz::where('id',$request->quiz_id)->with('books')->first();
        $new_books = view('quiz.admin.question.books.book-list',$data)->render();
        return response()->json(['new_books'=>$new_books, 'success'=>'Book successfully deleted']);

    }
}
