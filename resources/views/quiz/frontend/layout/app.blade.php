<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,700,700i|Yellowtail" rel="stylesheet"> 
	<!-- Favicon icon -->
	<link rel="icon" href="http://html.phoenixcoded.net/mega-able/files/assets/images/favicon.ico" type="image/x-icon">
	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('files/fontawesome/css/all.css')}}">
	<link rel="stylesheet" href="{{asset('files/assets/css/custom.css')}}">
	<style type="text/css">
		.btn-info{
			background-color: #0056b3;
    		border-color: #0056b3;
		}
		.breadcrumb{
			background-color: #1a1c1d;
   			padding: 12px;
   		}
	</style>
	@yield('style')
	<title>@yield('title')</title>
</head>
<body>
	@include('quiz.frontend.include.header')
	@yield('content')
	@include('quiz.frontend.include.footer')
		
	<script type="text/javascript" src="{{asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('files/assets/js/custom.js')}}"></script>
	<script type="text/javascript">
	   	$(function(){
			var navbar = $('.navbar');
			if($(window).scrollTop() <= 80){
				navbar.removeClass('navbar-fixed');
			} else {
				navbar.addClass('navbar-fixed');
			}
			$(window).scroll(function(){
				if($(window).scrollTop() <= 80){
					navbar.removeClass('navbar-fixed');
				} else {
					navbar.addClass('navbar-fixed');
				}
			});
		});
	</script>
	<script>
		$(document).on('change','.profile_img',function(){
			$('#piuform').submit();
		})
	</script>
	@yield('script')
</body>
</html>