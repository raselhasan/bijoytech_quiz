
    <section class="header-section">
      <div class="main-menu">
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('logo/'.$hContact->logo)}}"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="{{route('user.home')}}">Home</a>
              </li>
              @if(Auth::user())
              <li class="nav-item">
                <a class="nav-link" href="{{route('user.profile')}}">My Profile</a>
              </li>
              @endif
              @if(!Auth::user())
              <li class="nav-item">
                <a class="nav-link" href="{{route('user.register')}}">Register</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('user.login')}}">Login</a>
              </li>
              @endif
              @if(Auth::user())
              <li class="nav-item">
                <a class="nav-link" href="{{route('user.logout')}}">Logout</a>
              </li>
              @endif
            </ul>
          </div>
        </nav>
      </div>
    </section>