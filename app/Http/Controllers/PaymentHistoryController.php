<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentHistory;
use Helper;
use App\MoneyRequest;

class PaymentHistoryController extends Controller
{
    public function index()
    {
    	return view('quiz.admin.payment-history.index');
    }

    public function getPost(Request $request)
    {
    	$columns = array( 
            0 => 'created_at',
            1 => 'name', 
            2 => 'phone',
            3 => 'account_number',
            4 => 'payment_method',   
            5 => 'payment_id',
            6 => 'amount',
            7 => 'id',
        );
  
        $totalData = PaymentHistory::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = PaymentHistory::offset($start)
                 ->limit($limit)
                 ->orderBy('id','desc')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts =  PaymentHistory::where('id','LIKE',"%{$search}%")
                ->orWhere('created_at', 'LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('payment_id', 'LIKE',"%{$search}%")
                ->orWhere('amount', 'LIKE',"%{$search}%")
                ->orWhere('payment_method', 'LIKE',"%{$search}%")
                ->orWhere('account_number', 'LIKE',"%{$search}%")
                ->orWhere('phone', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = PaymentHistory::where('id','LIKE',"%{$search}%")
	            ->orWhere('created_at', 'LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('payment_id', 'LIKE',"%{$search}%")
                ->orWhere('amount', 'LIKE',"%{$search}%")
                ->orWhere('payment_method', 'LIKE',"%{$search}%")
                ->orWhere('account_number', 'LIKE',"%{$search}%")
                ->orWhere('phone', 'LIKE',"%{$search}%")
	            ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                
                $nestedData['created_at'] = date('m/d/Y', strtotime($post->created_at));
                $nestedData['payment_id'] = $post->payment_id;
                $nestedData['name'] = $post->name;
                $nestedData['phone'] = $post->phone;
                $nestedData['account_number'] = $post->account_number;
                $nestedData['amount'] = '$ '.number_format($post->amount, 2);
                $nestedData['payment_method'] = $post->payment_method;
                $nestedData['options'] = '<button p-history-id="'.$post->id.'" class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn payment-dlt-btn" data-toggle="tooltip" data-placement="top" title="Delete"><i class="ti-trash"></i></button>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }

    public function delete(Request $request)
    {
    	PaymentHistory::where('id',$request->id)->delete();
    	return 'success';
    }

    public function paymentRequest()
    {
        $data['paymentRequest'] = MoneyRequest::where('user_id', auth()->user()->id)->paginate(10);
        return view('quiz.frontend.payment-request.index',$data);
    }

    public function SendPaymentRequest( Request $request )
    {
        $currentAmount = Helper::currentAmount();
        $pendingAmount = Helper::pendingAmount();
        $paidAmount = Helper::paidAmount();

        $requestAmount = $pendingAmount + $paidAmount + $request->amount;

        if($requestAmount > $currentAmount){
            return redirect()->back()->with('error','Insufficient balance')->withInput($request->all());
        }

        $mRequest = new MoneyRequest();
        $mRequest->requested_amount  = $request->amount;
        $mRequest->user_id   = auth()->user()->id;
        $mRequest->name  = auth()->user()->name;
        $mRequest->current_balance  = $currentAmount;
        $mRequest->payment_method  = $request->payment_method;
        $mRequest->note  = $request->note;
        $mRequest->account_number  = $request->account_number;
        $mRequest->account_name  = $request->account_name;
        $mRequest->save();
        return redirect()->back()->with('success','Payment Request Has been sent');
    }

    public function viewPayment(Request $request)
    {
        //return $request->id;
        $data['payment'] = MoneyRequest::find($request->id);
        return view('quiz.frontend.payment-request.view',$data)->render();
    }
}
