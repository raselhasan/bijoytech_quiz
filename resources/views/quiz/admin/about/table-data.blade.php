<div class="dt-responsive table-responsive">
    <table id="order-table" class="table table-striped table-bordered nowrap">
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
</div>