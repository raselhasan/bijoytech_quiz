@extends('quiz.frontend.layout.app')

@section('style')
@endsection

@section('content')
<section class="main-section">
	<div class="log-cnt">
		<div class="row mx-0">
			<div class="col-6 px-0">
				<div class="form">
					<h4 class="text-center mb-4 text-uppercase">Login</h5>
					@if(Session::has('error'))
						<p class="err-msg">{{Session::get('error')}}</p>
					@endif
					<form method="post" action="{{route('admin.user.login')}}">
						@csrf
						<div class="form-group">
							<label class="mb-2">Email</label>
							<input type="email" class="form-control" name="email" value="{{old('email')}}">
						</div>
						<div class="form-group">
							<label class="mb-2">Password</label>
							<input type="password" class="form-control" name="password" value="{{old('password')}}">
						</div>
						<input type="hidden" name="role" value="2">
						<div class="form-group text-right">
							<a  href="{{route('user.forgotPassword')}}" style=" font-size:12px;">Forgot password?</a>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Login</button>
						</div>
						<div class="form-group text-center" style="margin-top: 40px">
							Don't have an account? <a href="{{route('user.register')}}">Register</a>
						</div>
					</form>
				</div>
			</div>
			<div class="col-6 px-0">
				<div class="lr-cover">
					<img class="cover"  src="{{asset('files/assets/images/log.jpg')}}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')

@endsection
