@extends('quiz.frontend.layout.app')
@section('title')
	Payment Request
@endsection
@section('style')
	<style type="text/css">
		.pmntViewClas{
			font-size: 17px;
		}
		.pmntViewClas p{
			margin-bottom: 5px;
		}
		.pmntViewClas span{
			color: #34cc21;
		}
	</style>
@endsection
@section('content')
	<section class="main-section p-0"> 
		<div class="row m-0">
			<div class="col-12 col-lg-2 p-0">
				@include('quiz.frontend.include.left-bar')
			</div>
			<div class="col-12 col-lg-10 p-0">
				<div class="main-body">
					@include('quiz.frontend.pages.quiz.bradcam')
					@include('quiz.frontend.payment-request.send-request')
					@include('quiz.frontend.payment-request.table')
					<div class="row mr-0">
						
						{{-- @include('quiz.frontend.pages.score.table') --}}
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="view-payment-modal"></div>
@endsection

@section('script')
	<script type="text/javascript">
		
		$(document).on('click','.payment-vw-btn',function(){
			var data = new FormData();
		  	data.append('id',$(this).attr('pmnt-id'));
		  	data.append('_token','{{csrf_token()}}');
		  	$.ajax({
			    processData: false,
			    contentType: false,
			    data: data,
			    type: 'POST',
			    url: '{{route("user.view-payment")}}',
			    success: function(response) {
			    	$('.view-payment-modal').html(response);
			    	$('#pViewModal').modal('show');
		            console.log(response);
			    }
		    });
		})
	</script>
@endsection
