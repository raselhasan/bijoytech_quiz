@extends('quiz.frontend.layout.app')
@section('title')
	Change Password
@endsection
@section('style')

@endsection
@section('content')
	<section class="main-section p-0"> 
		<div class="row m-0">
			<div class="col-12 col-lg-2 p-0">
				@include('quiz.frontend.include.left-bar')
			</div>
			<div class="col-12 col-lg-10 p-0">
				<div class="main-body">
					@include('quiz.frontend.pages.quiz.bradcam')
					<form action="{{route('user.change-password')}}" method="post">
						@csrf
						<div class="row mr-0">
							@if(Session::has('success'))
								<div class="col-12 col-md-12 pr-0">
									<div class="alert alert-primary" role="alert">
									   {{Session::get('success')}}
									</div>
								</div>
							@endif
							@if(Session::has('error'))
								<div class="col-12 col-md-12 pr-0">
									<div class="alert alert-danger" role="alert">
									   {{Session::get('error')}}
									</div>
								</div>
							@endif

							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">New Password</label>
									<input type="password" class="form-control" name="new_password" required>
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Repeat New Password</label>
									<input type="password" class="form-control" name="r_new_password" required>
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<button type="submit" class="btn btn-info" style="margin-top: 28px;">Change Password</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')

@endsection
