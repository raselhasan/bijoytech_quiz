@extends('quiz.frontend.layout.app')

@section('title')
	Faq
@endsection

@section('style')
	<style type="text/css">
		h4{
			color: yellowgreen;
			text-transform: uppercase;
		}
		hr{
			background: #448aff;
    		width: 16%;
		}
		.card{
			margin-top: 20px;
		}
	</style>
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="container">
		<div class="row mr-0">
			<div class="col-12 col-md-12 col-lg-12 pr-0 cont-sec">
				<h4 class="text-center">@if($faq->title) {{$faq->title}} @else Frequently Asked Questions (FAQ) @endif</h4>
				<hr/>
				{!! $faq->desc !!}
				<br/>
				<br/>
			</div>
			<div class="col-12 col-md-12 col-lg-12 pr-0">
				<h4 class="text-center">Getting Started</h4>
				<hr/>
			</div>

			@if(count($qnas) > 0)
				@foreach ($qnas as $key=>$qna)
					<div class="col-md-6">
						<div id="accordion">
						  	<div class="card">
						    	<div class="card-header" id="heading{{$key}}">
						      		<h5 class="mb-0">
						        		<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="false" aria-controls="collapse{{$key}}">
						          			{{$qna->question}}
						        		</button>
						      		</h5>
						    	</div>
						    	<div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordion">
						      		<div class="card-body">
						        		{!! $qna->answer!!}
						      		</div>
						    	</div>
						  	</div>
							
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</div>
</section>
@endsection

@section('script')
	
@endsection
