<div class="modal fade" id="view-book-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">View Book</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                 <iframe class="setBookUrlAdmin" src="http://www.adventistchurchconnect.com/site/1/docs/test.pdf" width="100%" height="500">
                  </iframe>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                  
              </div>
          </div>
      </div>
</div>

