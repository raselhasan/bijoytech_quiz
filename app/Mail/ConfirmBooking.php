<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmBooking extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Appointment scheduled for '.$this->data['date_and_time'].' with '. $this->data['user_name'];

        $message  = $this->from('info@globaltaxnyc.com', 'True Medical Care')->view('crm.mail.confirm-booking',['data'=>$this->data])->subject($subject)->to(trim($this->data['email']));
        return $message;
    }
}
