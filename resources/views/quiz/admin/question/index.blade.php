@extends('quiz.admin.layout.layout')

@section('title')
    Add Questions
@endsection

@section('cssfile')

@endsection

@section('style')
    <style type="text/css">
        .quiz-dts p{
            font-size: 16px;
            margin-bottom: 5px;

        }
        .quiz-dts span{
            color:cyan;
            
        }
        .ss-inputt {
            width: 320px;
            color: #fff;
        }

        .noResult{
            background: #37474f;
            padding: 15px;
            font-size: 15px;
        }


    </style>
@endsection

@section('content')
<!-- Page-header start -->
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="page-header-title">
                    <h5 class="m-b-10">Add Questions</h5>
                    <p class="m-b-0">Add your required Question</p>
                    <input type="hidden" id="quiz-status" value="{{$status}}">
                </div>
            </div>
            <div class="col-md-4">
                <a href="{{Helper::backFromAddQuestion($status)}}" class="btn waves-effect waves-light btn-success btn-skew fl-right bck-btn">Back</a>

                @if($status == 3)
                    <button class="btn waves-effect waves-light btn-primary btn-skew fl-right" data-toggle="modal" data-target="#add-book-modal">Add Book</button>
                @endif
                @if($status == 4)
                    <button class="btn waves-effect waves-light btn-primary btn-skew fl-right" data-toggle="modal" data-target="#add-video-modal">Add Video</button>
                @endif



            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="row">
                    <input type="hidden" id="quizId" value="{{$quiz->id}}">
                    @include('quiz.admin.question.quiz-details')
                    @if($status == 3)
                        @include('quiz.admin.question.books.books')
                    @endif
                    @if($status == 4)
                        @include('quiz.admin.question.videos.videos')
                    @endif
                    @if($status == 1 || $status == 2)
                        @include('quiz.admin.question.language.list')
                    @endif
                    @include('quiz.admin.question.question.add')
                </div>
                <div class="row">
                    @include('quiz.admin.question.question.list')
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" class="variationId" value="">
@if($status == 3)
    @include('quiz.admin.question.books.view-book')
@endif
@if($status == 4)
    @include('quiz.admin.question.videos.view-video')
@endif
@endsection

@section('jsfile')

@endsection

@section('script')

@if(Session::has('success'))
    <script>
        toastr["success"]("{{Session::get('success')}}");
    </script>
@endif

<script type="text/javascript">
window.edit_question = "<?= url('admin/edit-question')?>";
window.search_question = "<?= url('admin/search-question')?>";

    $(document).on('keyup','#searchQuestn',function(){
        var search = $(this).val();
        var quiz_id = $('#quizId').val();
        var status = '{{$status}}';
        var variationId = $('.variationId').val();
        var data = new FormData();
        data.append('variation_id',variationId);
        data.append('status',status);
        data.append('quiz_id',quiz_id);
        data.append('search',search);
        data.append('_token',window.token);
        $.ajax({
            processData: false,
            contentType: false,
            data: data,
            type: 'POST',
            url: window.search_question,
            success: function(response) {
               $('.set-all-question').html(response.all_question);
                console.log(response);
            }
        });
    })


    function showInput(e) {
        $(e.target).parents(".questions-row").find(".edit-q").hide();
        $(e.target).parents(".questions-row").find(".new-d-btn").show();
        $(e.target).parents(".questions-row").find(".cansel-q").show();
        $(e.target).parents(".questions-row").find(".form-control").removeClass("inactive");
        $(e.target).parents(".questions-row").find(".form-control").addClass("active");
        $(e.target).parents(".questions-row").find(".form-control").removeAttr("readonly");;
    }
    function hideInput(e) {
        $(e.target).parents(".questions-row").find(".cansel-q").hide();
        $(e.target).parents(".questions-row").find(".new-d-btn").hide();
        $(e.target).parents(".questions-row").find(".edit-q").show();
        $(e.target).parents(".questions-row").find(".form-control").removeClass("active");
        $(e.target).parents(".questions-row").find(".form-control").addClass("inactive");
        $(e.target).parents(".questions-row").find(".form-control").prop('readonly', true);
        $(".new-edit-col").remove();
    }
    function apeendOption(e) {
       var n = $(e.target).parent('.append-option').siblings('.option-add-e').find( ".option-col" ).length;
       var html = '<div class="col-sm-6 option-col new-edit-col"><p class="d-flex  align-items-center"><span class="indentity-txt mr-1">'+(n+1)+'.</span><input class="form-control active" placeholder="Option name" type="text" name="edit_option[]" value=""><button onclick="optionR(event)" type="button" class="btn ml-2 py-1 px-2 waves-effect waves-dark btn-danger btn-outline-danger new-d-btn"><i class="ti-trash"></i></button></p></div>';
       $(e.target).parent('.append-option').siblings('.option-add-e').append(html);  
   }
   function optionR(e) {
    $(e.target).closest('.option-col').remove();
}

function questionFormCall(e) {
    var data = new FormData( $(e.target).parents('.edit-question-form' )[ 0 ] );
    $.ajax({
        processData: false,
        contentType: false,
        data: data,
        type: 'POST',
        url: window.edit_question,
        success: function(response) {
            if(response) {
                toastr["success"]("Saved!");
                $(e.target).parents(".questions-row").find(".cansel-q").hide();
                $(e.target).parents(".questions-row").find(".new-d-btn").hide();
                $(e.target).parents(".questions-row").find(".edit-q").show();
                $(e.target).parents(".questions-row").find(".form-control").removeClass("active");
                $(e.target).parents(".questions-row").find(".form-control").addClass("inactive");
                $(e.target).parents(".questions-row").find(".form-control").prop('readonly', true);
                $(e.target).parents(".questions-row").find(".option-col").removeClass('new-edit-col');
                // $(e.target).parents(".questions-row").remove();  
            }
        }
    });
}

function viewBook(url)
{
    $('.setBookUrlAdmin').attr('src',url);
    $('#view-book-modal').modal('show');
}

function viewVideo(url)
{
    $('.adminViewVideo').html(url);
    $('#view-video-modal').modal('show');
}

$(document).on('click','.questin-del-btn',function(){
    var id = $(this).attr('quest-id');
    $.confirm({
        title: 'Alert!',
        content: 'Are you sure to delete this item?',
        buttons: {
            confirm: function () {
                var data = new FormData();
                data.append('id',id);
                data.append('_token',window.token);
                $.ajax({
                    processData: false,
                    contentType: false,
                    data: data,
                    type: 'POST',
                    url: '{{route("admin.qestion.delete")}}',
                    success: function(response) {
                        $('#del-ques-'+id).remove();
                        toastr["success"]("Deleted!");
                    }
                });
            },
            cancel: function () {
            
            },
        }
    });
})
</script>
@endsection
