<form action="{{route('admin.withdrawal.request.paid')}}" method="post" id="make-paid-form">
    @csrf
    <div class="modal fade" id="make-paid-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Make Paid</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Payment Method</label>
                                <select class="form-control" name="payment_method" id="exampleFormControlSelect1" required>
                                  <option value="bkash" @if($moneyRequest->payment_method == 'bkash') selected @endif>Bkash</option>
                                  <option value="paypal" @if($moneyRequest->payment_method == 'paypal') selected @endif>Paypal</option>
                                  <option value="zille" @if($moneyRequest->payment_method == 'zille') selected @endif>Zille</option>
                                  <option value="quick pay" @if($moneyRequest->payment_method == 'quick pay') selected @endif>Quick Pay</option>
                                  <option value="other" @if($moneyRequest->payment_method == 'other') selected @endif>Other</option>
                                </select>
                            </div>
                       </div>
                        <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Account Number</label>
                                <input type="text" name="account_number" class="form-control" value="{{$moneyRequest->account_number}}" readonly>
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Account Name</label>
                                <input type="text" name="account_name" class="form-control" value="{{$moneyRequest->account_name}}" readonly>
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Requested Amount</label>
                                <input type="text" name="title" class="form-control" value="{{$moneyRequest->requested_amount}}" readonly>
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Paid Amount</label>
                                <input type="number" name="paid" class="form-control" step="0.01" required value="{{$moneyRequest->paid}}">
                            </div>
                       </div>
                       <input type="hidden" name="id" value="{{$moneyRequest->id}}">
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Paid Status</label>
                                <select class="form-control" name="paid_status" id="exampleFormControlSelect1" required>
                                  <option value="paid" @if($moneyRequest->paid_status == 'paid') selected @endif>Paid</option>
                                  <option value="pending" @if($moneyRequest->paid_status == 'pending') selected @endif>Pending</option>

                                </select>
                            </div>
                       </div>
                       
                       
                       <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Payment Id</label>
                                <input type="text" name="payment_id" value="{{$moneyRequest->payment_id}}" class="form-control" required>
                            </div>
                       </div>
                       <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Note</label>
                                <textarea class="form-control" name="note" id="exampleFormControlTextarea1" rows="3">{{$moneyRequest->note}}</textarea>
                            </div>
                       </div>
                       <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Message</label>
                                <textarea class="form-control" name="message" id="exampleFormControlTextarea2" rows="3">{{$moneyRequest->message}}</textarea>
                            </div>
                       </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
