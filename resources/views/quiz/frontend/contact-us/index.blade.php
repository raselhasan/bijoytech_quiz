@extends('quiz.frontend.layout.app')

@section('title')
	Contact Us
@endsection

@section('style')
	<style type="text/css">
		.cont-sec{
		 	background: #1a1c1d45;;
    		padding: 10px;
		}
		.cont-sec h4{
			color: yellowgreen;
		}
		.cont-sec p{
			margin-bottom: 5px;
    		font-size: 16px;
		}
		.cont-sec hr{
			background: black;
		}
		.pt-3{
			margin-top: 0px;
		}
		
		.ques-have{
			color: yellowgreen;
    		margin-bottom: 31px;
		}
	</style>
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="row mr-0">
		<div class="col-4 col-md-4 col-lg-4 pr-0 cont-sec">
			<h4>Quick Contact</h4>
			<hr>
			{!! $contact->contact !!}
			<br/>
			<h4>More Information</h4>
			<hr>
			<div class="social-menu">
	            <a class="social-l fac" target="_blank" href="{{$contact->fb}}"><i class="fab fa-facebook-f"></i></a>
	            <a class="social-l tw" target="_blank" href="{{$contact->tw}}"><i class="fab fa-twitter"></i></a>
	            <a class="social-l you" target="_blank" href="{{$contact->yt}}"><i class="fab fa-youtube"></i></a>
	            <a class="social-l pin" target="_blank" href="{{$contact->pnt}}"><i class="fab fa-pinterest"></i></a>
	            <a class="social-l ins" target="_blank" href="{{$contact->ins}}"><i class="fab fa-instagram"></i></a>
          	</div>
		</div>
		<div class="col-8 col-md-8 col-lg-8 pr-0">
			<form method="post" action="{{route('send-contact')}}">
				@csrf
				<div class="row">
					
					<div class="col-md-12">
						<h4 class="ques-have">HAVE ANY QUESTIONS? SEND MESSAGE US</h4>
					</div>
					@if(Session::has('success'))
						<div class="col-md-12">
							<div class="alert alert-primary" role="alert">
							  	{{Session::get('success')}}
							</div>
						</div>
					@endif
					<div class="col-md-6">
						<div class="form-group">
						    <label for="exampleInputEmail1">Full Name</label>
						    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name" required>
						 </div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="exampleInputEmail1">Phone</label>
						    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="phone" required>
						 </div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="exampleInputEmail1">Email</label>
						    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
						    <label for="exampleInputEmail1">Subject</label>
						    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="subject" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <label for="exampleFormControlTextarea1">Message</label>
						    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="message" required></textarea>
						  </div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
						    <button class="btn btn-primary pull-right" type="submit">Send</button>
						 </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection

@section('script')
	
@endsection
