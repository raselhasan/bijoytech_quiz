@if(count($upcomeing) > 0)
	<div class="col-12 col-md-12 col-lg-12 pr-0 cont-sec">
		<h4 class="text-center">Upcoming Quiz</h4>
		<hr/>
	</div>
	@foreach($upcomeing as $quiz)
		<div class="col-3 col-md-3 col-lg-3 pr-0">
			<div class ="card mb-3">
				<div class="card-image">
					<img class="cover"  src="{{asset('files/assets/images/cover1.jpg')}}">
				</div>
				<div class="card-header text-center p-3">
					<a href="{{url('/question')}}"><h5 class="mb-0">{{$quiz->title}}</h5></a>
				</div>
				<div class="card-block pt-0 quiz-card">
					<p>Total Question: <span>{{$quiz->total_quistion}}</span></p>
					<p>Every Question Mark: <span>{{$quiz->mark}}</span></p>
					@if($quiz->price)
						<p>Price Money: <span>${{ number_format($quiz->price, 2)}}</span></p>
						<p>Price Money Will Get: <span>{{$quiz->person}} Top Scorer</span></p>
						<p>Top Each Person Will Get: <span>${{number_format($quiz->price / $quiz->person, 2)}}</span></p>

					@endif

				</div>
			</div>
		</div>
	@endforeach
@endif