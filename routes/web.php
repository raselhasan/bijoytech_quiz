<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('user.home');
Route::get('/content/{id}', 'HomeController@singleContent')->name('singlecontent');



Route::get('privacy-policy', 'GlobalController@policy')->name('privacy-policy');
Route::get('terms-and-conditions', 'GlobalController@terms')->name('terms-and-conditions');

Route::get('contact-us', 'GlobalController@contactUs')->name('contactus');
Route::post('send-contact', 'ContactController@sendContact')->name('send-contact');
Route::get('about-us', 'GlobalController@aboutUs')->name('aboutus');

Route::get('team/{id}', 'GlobalController@team')->name('team');
Route::get('faq', 'GlobalController@faq')->name('faq');
Route::get('redirect-for-quiz/{status}', 'RegisterController@redirectRules')->name('redirectRules');


Route::group(['middleware' => ['UserNotLogedIn'], 'prefix' => 'user'], function () {
        Route::get('login', 'LoginController@userLogin')->name('user.login');
        Route::get('register', 'RegisterController@userRegister')->name('user.register');
        Route::post('register', 'RegisterController@register')->name('user.register');
        Route::get('forgot-password', 'RegisterController@forgotPassword')->name('user.forgotPassword');
        Route::post('forgot-password', 'RegisterController@sendCode')->name('user.forgotPassword');
        Route::get('verify', 'RegisterController@vefiryCodePage')->name('user.verify');
        Route::post('verify', 'RegisterController@vefiryCode')->name('user.verify');
        Route::get('reset-password/{code?}', 'RegisterController@resetPassword')->name('user.resetPassword');
        Route::post('confirm-password/', 'RegisterController@confirmResetPassword')->name('user.confirmPassword');

});

Route::group(['middleware' => ['auth'],'prefix' => 'user'], function () {
        Route::group(['middleware' => ['UserLogedIn']], function () {
                Route::get('logout', 'RegisterController@logout')->name('user.logout');
                Route::get('dashboard', 'DashboardController@userDashboard')->name('user.dashboard');
                Route::get('quiz/{status?}', 'QuizController@startQuiz')->name('user.quiz');
                Route::get('quiz/book/{quiz_id?}/{status?}', 'QuizController@showBooks')->name('user.show.book');
                Route::get('question/{quiz_id}/{status}/{book_id?}', 'QuestionController@showQuestion')->name('user.question');
                Route::post('question/single-question', 'QuestionController@getSingleQuestion')->name('user.single-question.get');
                Route::get('profile', 'ProfileController@index')->name('user.profile');
                Route::post('profile', 'ProfileController@updateProfile')->name('user.profile');
                Route::get('score', 'ScoreController@index')->name('user.score');
                Route::post('score', 'ScoreController@search')->name('user.score');
                Route::get('payment-request', 'PaymentHistoryController@paymentRequest')->name('user.payment-request');
                Route::post('payment-request', 'PaymentHistoryController@SendPaymentRequest')->name('user.payment-request');
                Route::post('view-payment', 'PaymentHistoryController@viewPayment')->name('user.view-payment');
                Route::post('upload-profile-image', 'ProfileController@uploadProfileImage')->name('user.profilePic');

                Route::get('change-password', 'ProfileController@changePassword')->name('user.change-password');
                Route::post('change-password', 'ProfileController@savePassword')->name('user.change-password');
                
        });
});

Route::group(['middleware' => ['auth'],'prefix' => 'admin'], function () {
	Route::group(['middleware' => ['Admin']], function () {
		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
                
		Route::get('reading-quiz', 'ReadingController@index')->name('admin.reading.quiz');
		Route::post('reading-quiz', 'ReadingController@getPost')->name('admin.reading.quiz.getPost');

                Route::get('add-question/{quiz_id?}/{status?}', 'QuestionController@index')->name('admin.question.add');

                Route::post('add-book/{quiz_id}', 'BookController@addBook')->name('admin.book.add');
                Route::post('edit-book', 'BookController@editBook')->name('admin.book.edit');
                Route::post('update-book/{quiz_id}/{book_id}', 'BookController@updateBook')->name('admin.book.update');
                Route::post('delete-book', 'BookController@deleteBook')->name('admin.book.delete');
                Route::post('book-active', 'BookController@activeBook')->name('admin.book.active');


                Route::post('add-video/{quiz_id}', 'VideoController@addVideo')->name('admin.video.add');
                Route::post('edit-video', 'VideoController@editVideo')->name('admin.video.edit');
                Route::post('update-video/{quiz_id}/{video_id}', 'VideoController@updateVideo')->name('admin.video.update');
                Route::post('delete-video', 'VideoController@deleteVideo')->name('admin.video.delete');
                Route::post('active-video', 'VideoController@activeVideo')->name('admin.video.active');
                Route::post('active-language', 'GlobalController@activeLanguage')->name('admin.language.active');



                Route::post('all-question', 'QuestionController@allQuestion')->name('admin.all.question');
                Route::post('question-delete', 'QuestionController@deleteQuestion')->name('admin.qestion.delete');
                Route::post('add-question', 'QuestionController@addQuestion')->name('admin.add.question');
                Route::post('search-question', 'QuestionController@searchQuestion')->name('admin.search.question');
                Route::post('update-question', 'QuestionController@updateQuestion')->name('admin.question.update');
                Route::post('edit-question', 'QuestionController@editQuestion')->name('admin.edit.question');


                Route::get('general-quiz', 'GeneralQuizController@index')->name('admin.general.quiz');
                Route::post('general-quiz', 'GeneralQuizController@getPost')->name('admin.general.quiz.getPost');

                Route::get('spelling-quiz', 'SpellingQuizController@index')->name('admin.spelling.quiz');
                Route::post('spelling-quiz', 'SpellingQuizController@getPost')->name('admin.spelling.quiz.getPost');

                Route::get('video-quiz', 'VideoQuizController@index')->name('admin.video.quiz');
                Route::post('video-quiz', 'VideoQuizController@getPost')->name('admin.video.quiz.getPost');


                Route::post('add-quiz', 'GlobalController@addQuiz')->name('admin.quiz.add');
                Route::post('edit-quiz', 'GlobalController@editQuiz')->name('admin.quiz.edit');
                Route::post('update-quiz', 'GlobalController@updateQuiz')->name('admin.quiz.update');
                Route::post('delete-quiz', 'GlobalController@deleteQuiz')->name('admin.quiz.delete');

                // money request
                Route::get('withdrawal-request', 'MoneryRequestController@index')->name('admin.withdrawal.request');
                Route::post('withdrawal-request', 'MoneryRequestController@getPost')->name('admin.withdrawal.request');
                Route::post('withdrawal-request/delete', 'MoneryRequestController@delete')->name('admin.withdrawal.request.delete');
                Route::post('withdrawal-request/get', 'MoneryRequestController@getSingleRequest')->name('admin.withdrawal.request.get');
                Route::post('withdrawal-request/paid', 'MoneryRequestController@makePaid')->name('admin.withdrawal.request.paid');

                //payment history

                Route::get('payment-history', 'PaymentHistoryController@index')->name('admin.payment.history');
                Route::post('payment-history', 'PaymentHistoryController@getPost')->name('admin.payment.history');
                Route::post('payment-history/delete', 'PaymentHistoryController@delete')->name('admin.payment.history.delete');

                Route::get('participant/{quiz_id?}/{status?}', 'GlobalController@participant')->name('admin.participant');
                Route::get('participant-get/{quiz_id?}/{status?}', 'GlobalController@getParticipant')->name('admin.participant.get');

                Route::get('video-participant/{quiz_id?}/{status?}', 'GlobalController@videoParticipant')->name('admin.video.participant');

                Route::get('video-participant-get/{quiz_id?}/{status?}', 'GlobalController@getVideoParticipant')->name('admin.video-participant.get');

                Route::get('view-answer/{quiz_id?}/{status?}/{user_id?}','GlobalController@viewAnser')->name('admin.view.answer');


                Route::get('general-participant/{quiz_id?}/{status?}', 'GlobalController@GeneralParticipant')->name('admin.general.participant');

                 Route::get('general-participant-get/{quiz_id?}/{status?}', 'GlobalController@getGeneralParticipant')->name('admin.general-participant.get');

                 Route::get('evaluate/{quiz_id?}', 'GlobalController@evaluate')->name('admin.evaluate');

                 // home
                 Route::get('home', 'HomeController@adminHome')->name('admin.home');
                 Route::post('home', 'HomeController@adminHomeSave')->name('admin.home');
                 Route::post('home-content', 'HomeController@getHomeContent')->name('admin.home.content');
                 Route::post('home-content-edit', 'HomeController@editContent')->name('admin.home.content.edit');
                 Route::post('home-content-delete', 'HomeController@getHomeContentDelete')->name('admin.home.content.delete');

                 Route::get('setting', 'SettingController@index')->name('admin.setting');
                 Route::post('setting', 'SettingController@saveSetting')->name('admin.setting');
                 Route::post('setting/logo', 'SettingController@logoUpload')->name('admin.logo.upload');

                 Route::get('user', 'GlobalController@users')->name('admin.users');   
                 Route::post('user', 'GlobalController@getUsers')->name('admin.users');
                 Route::post('user/view', 'GlobalController@viewUser')->name('admin.user.view'); 



                 Route::get('contact', 'ContactController@index')->name('admin.contact');
                 Route::post('contact', 'ContactController@save')->name('admin.contact.save');

                 Route::get('faq', 'FaqController@index')->name('admin.faq');
                 Route::post('faq', 'FaqController@save')->name('admin.faq.save');    




                 Route::post('get-question-answer', 'FaqController@getQuestionAnswer')->name('admin.qna.get');
                 Route::post('save-question-answer', 'FaqController@addQuestionAnswer')->name('admin.qna.add');
                 Route::post('edit-question-answer', 'FaqController@editQuestionAnswer')->name('admin.qna.edit');
                 Route::post('delete-question-answer', 'FaqController@deleteQuestionAnswer')->name('admin.qna.delete');


                 Route::get('about', 'AboutController@index')->name('admin.about');
                 Route::post('about', 'AboutController@save')->name('admin.about.save');    

                 Route::post('team', 'AboutController@getTeam')->name('admin.about.team');
                 Route::post('add-team', 'AboutController@addTeam')->name('admin.team.add');
                 Route::post('edit-team', 'AboutController@editTeam')->name('admin.team.edit');
                 Route::post('delete-team', 'AboutController@deleteTeam')->name('admin.team.delete');

                 
                 

	});

});

Route::post('admin-user-login', 'LoginController@login')->name('admin.user.login');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
