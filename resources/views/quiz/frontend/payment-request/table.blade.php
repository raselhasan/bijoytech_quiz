@if(count($paymentRequest) > 0)
  <table class="table table-dark">
      <thead>
        <tr>
          <td scope="col">Date</td>
          <td scope="col">Requested Amount</td>
          <td scope="col">Paid</td>
          <td scope="col">Paid Status</td>
          <td scope="col">Paid Date</td>
          <td scope="col">Action</td>
        </tr>
    </thead>
    <tbody>
    @foreach($paymentRequest as $pRequest)
      <tr>
        <td>{{date('m/d/Y',strtotime($pRequest->created_at))}}</td>
        <td>${{number_format($pRequest->requested_amount, 2)}}</td>
        <td>${{number_format($pRequest->paid, 2)}} $</td>
        <td>{{$pRequest->paid_status}}</td>
        <td>
            @if($pRequest->paid_date)
            {{date('m/d/Y',strtotime($pRequest->paid_date))}}
            @endif
        </td>
        <td>
          <button class="btn btn-info payment-vw-btn" pmnt-id="{{$pRequest->id}}">View</button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
    {{@$paymentRequest->links()}}
@endif  