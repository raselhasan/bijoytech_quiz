@if(count($quiz->books) > 0)
    @foreach($quiz->books as $book)
        <tr>
            <td>
                <div class="form-check" onclick="getAllQuestion({{$book->id}}, '3','demo')">
                    <input class="form-check-input" type="radio" name="selected_book" id="exampleRadios{{$book->id}}" value="{{$book->id}}">
                    <label class="form-check-label" for="exampleRadios{{$book->id}}">
                        Select Book
                    </label>
                </div>
            </td>
            <td><a href="{{asset('books/'.$book->book)}}" download>{{$book->title}}</a></td>
            <td>{{$book->language}}</td>
            <td>
                @if($book->active == 1)
                    <p class="bk-active" onclick="bookActive({{$book->id}},{{$book->quiz_id}},'0')">Active</p>
                @else 
                    <p class="bk-dactive" onclick="bookActive({{$book->id}},{{$book->quiz_id}},'1')">Dactive</p>
                @endif    
            </td>
            <td>
                <button class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn mr-2" onclick="editBook({{$book->id}})" data-toggle="tooltip" data-placement="top" title="Edit Book"><i class="ti-slice"></i></button>
                <button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn" onclick="deleteBook({{$book->quiz_id}},{{$book->id}})" data-toggle="tooltip" data-placement="top" title="Delete Book"><i class="ti-trash"></i></button>
                <button class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn" onclick="viewBook('{{asset("books/".$book->book)}}')" data-toggle="tooltip" data-placement="top" title="View Book"><i class="ti-eye"></i></button>
            </td>
        </tr>
    @endforeach
@endif
