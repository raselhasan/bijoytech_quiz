@extends('quiz.frontend.layout.app')

@section('title')
	Quiz Score
@endsection


@section('style')

@endsection
@section('content')
	<section class="main-section p-0"> 
		<div class="row m-0">
			<div class="col-12 col-lg-2 p-0">
				@include('quiz.frontend.include.left-bar')
			</div>
			<div class="col-12 col-lg-10 p-0">
				<div class="main-body">
					@include('quiz.frontend.pages.quiz.bradcam')
					<div class="row mr-0">
						@include('quiz.frontend.pages.score.search')
						@include('quiz.frontend.pages.score.table')
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).on('keyup','#score-search',function(){
		  	var data = new FormData();
		  	data.append('search',$(this).val());
		  	data.append('_token','{{csrf_token()}}');
		  	$.ajax({
			    processData: false,
			    contentType: false,
			    data: data,
			    type: 'POST',
			    url: '{{route("user.score")}}',
			    success: function(response) {
			    	$('#set-score-data').html(response.list);
			    	$('#pagination-data').html(response.pagi);
		            console.log(response);
			    }
		    });
		})
	</script>
@endsection
