<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Quiz Details</h5>
            <span>Lorem Ipsum is simply dummy text of the printing</span>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    @foreach($questionAndAnswer as $key=>$ques)
                        <div class="row answerdQuestion">
                            <div class="col-md-10"><span>Q{{$key+1}}.</span> {{$ques->question->question}}</div>
                            <div class="col-md-2">
                                @if($ques->question->answer == $ques->answer)
                                    <i class="ti-check"></i>
                                @else    
                                    <i class="ti-close"></i>
                                @endif    
                            </div>
                            @if(count($ques->question->options) > 0)
                                @foreach($ques->question->options as $opkey=>$option)
                                    <div class="col-md-6">{{$opkey+1}}. {{$option->option}}</div>
                                @endforeach
                            @endif
                            <div class="col-md-6">
                                <span>Answer: </span>{{$ques->question->answer}} 
                            </div>
                            <div class="col-md-6">
                                <span>Given Answer: </span>{{$ques->answer}} 
                            </div>    
                        </div>
                    @endforeach

                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>