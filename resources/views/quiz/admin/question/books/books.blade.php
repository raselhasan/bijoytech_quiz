<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Books</h5>
            <span>All Book list here</span>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Select Book</th>
                            <th>Book Title</th>
                            <th>Book Language</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="set-all-books">
                        @include('quiz.admin.question.books.book-list')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('quiz.admin.question.books.add-book')
<div class="set-edit-book-modal"></div>
