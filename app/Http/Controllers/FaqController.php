<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;
use App\Qna;

class FaqController extends Controller
{
    public function index()
    {
    	$data['faq'] = Faq::orderBy('id','desc')->first();
    	return view('quiz.admin.faq.index', $data);
    }

    public function save(Request $request)
    {	
    	if($request->id){
    		$faq  = Faq::findOrFail($request->id);
    		$faq->fill($request->all())->save();
    	}else{
    		Faq::create($request->all());
    	}
    	
    	return redirect()->back()->with('success','Saved');
    }

    public function addQuestionAnswer(Request $request)
    {
    	if($request->id){
    		$qna  = Qna::findOrFail($request->id);
    		$qna->fill($request->all())->save();
    	}else{
    		Qna::create($request->all());
    	}
    	return redirect()->back()->with('success','Saved');
    }
    public function getQuestionAnswer( Request $request)
    {
        $columns = array( 
            0 => 'question', 
            1 => 'answer',
            2 => 'id'
        );
  
        $totalData = Qna::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = Qna::offset($start)
                 ->limit($limit)
                 ->orderBy('id','desc')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = Qna::where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('question', 'LIKE',"%{$search}%")
                        ->orWhere('answer', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Qna::where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('question', 'LIKE',"%{$search}%")
                        ->orWhere('answer', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $nestedData['question'] = $post->question;
                $content = '';
                if(strlen($post->answer) > 100){
                    $content = substr($post->answer, 0, 100).'....';
                }else{
                    $content = $post->answer;
                }
                $nestedData['answer'] = $content;
                $nestedData['options'] = '
                <button qna-id="'.$post->id.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn edt-qna-btn" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-slice"></i></button>
                <button qna-id="'.$post->id.'" class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn qna-dlt-btn" data-toggle="tooltip" data-placement="top" title="Delete"><i class="ti-trash"></i></button>
                ';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);

    }

    public function deleteQuestionAnswer(Request $request)
    {
        Qna::where('id',$request->id)->delete();
        return 'success';
    }
    public function editQuestionAnswer(Request $request)
    {
        $data['qna'] = Qna::where('id',$request->id)->first();
        return view('quiz.admin.faq.add-faq',$data)->render();
    }
}
