 <nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <img class="img-80 img-radius" src="{{asset('files/assets/images/admin.png')}}" alt="User-Profile-Image">
                <div class="user-details">
                    <span id="more-details">{{auth()->user()->name}}</span>
                </div>
            </div>
        </div>
        
        <div class="pcoded-navigation-label">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="@if(\Request::route()->getName() == 'admin.users') active @endif">
                <a href="{{route('admin.users')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-user"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Registered Users</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.reading.quiz') active @endif @if(@$status == 3) active @endif">
                <a href="{{route('admin.reading.quiz')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-widgetized"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Reading Quiz Compitision</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            
            <li class="@if(\Request::route()->getName() == 'admin.general.quiz') active @endif @if(@$status == 1) active @endif">
                <a href="{{route('admin.general.quiz')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-view-list-alt"></i><b>FS</b></span>
                    <span class="pcoded-mtext">General Quiz Compitision</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.spelling.quiz') active @endif @if(@$status == 2) active @endif">
                <a href="{{route('admin.spelling.quiz')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-view-grid"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Spelling Quiz Compitision</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.video.quiz') active @endif @if(@$status == 4) active @endif">
                <a href="{{route('admin.video.quiz')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Video Quiz Compitision</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.withdrawal.request') active @endif">
                <a href="{{route('admin.withdrawal.request')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-reload rotate-refresh"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Monery Withdrawal Req.</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.payment.history') active @endif">
                <a href="{{route('admin.payment.history')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-money"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Monery Sent History</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.home') active @endif">
                <a href="{{route('admin.home')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-layers-alt"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Home Page Content</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.setting') active @endif">
                <a href="{{route('admin.setting')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-settings"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Page Content Setting</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.contact') active @endif">
                <a href="{{route('admin.contact')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-email"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Contact Page Content</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.faq') active @endif">
                <a href="{{route('admin.faq')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-map-alt"></i><b>FS</b></span>
                    <span class="pcoded-mtext">Faq Page Content</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="@if(\Request::route()->getName() == 'admin.about') active @endif">
                <a href="{{route('admin.about')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-calendar"></i><b>FS</b></span>
                    <span class="pcoded-mtext">About Page Content</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </div>
</nav>