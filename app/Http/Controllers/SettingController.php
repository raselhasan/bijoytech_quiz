<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Validator;

class SettingController extends Controller
{
    public function index()
    {
    	$data['setting'] = Setting::first();
    	return view('quiz.admin.setting.index', $data);
    }

    public function saveSetting(Request $request)
    {
    	$setting = Setting::first();
    	if($setting){
    		foreach ($request->except('_token') as $key => $value) {
			  	$sv = Setting::find($setting->id);
			  	$sv->$key = $value;
			  	$sv->save();
			}
			return 'success';
    	}

    	$sv = new Setting();
    	foreach ($request->except('_token') as $key => $value) {
		  	$sv->$key = $value;
		  	$sv->save();
		}
		return 'success';
    }

    public function logoUpload( Request $request)
    {
        $whichImg = 'logo';
        $path = public_path('logo');
        if($request->status == 2){
            $whichImg = 'banner';
            $path = public_path('banner');
        }
         $validator = Validator::make($request->all(), [
            $whichImg => 'mimes:jpeg,jpg,png',
        ]);

        if ($validator->fails()) {
            return response()->json(['v_error'=>'The files must be a file of type: jpeg, jpg, png','status'=>$request->status]);
        }
        $file = $request->file($whichImg);
        $extention = $file->getClientOriginalExtension();
        $fileName = time().'.'.$extention;
        $file->move($path,$fileName);

        $setting = Setting::first();
        if($setting){
            $sv = Setting::find($setting->id);
            $sv->$whichImg = $fileName;
            $sv->save();
        }else{
            $sv = new Setting();
            $sv->$whichImg = $fileName;
            $sv->save();
        }
        
        $img = url($whichImg.'/'.$fileName);
        $img = view('quiz.admin.setting.img',compact('img'))->render();
        return response()->json(['success'=> true, 'img'=>$img,'status'=>$request->status]);
    }
}
