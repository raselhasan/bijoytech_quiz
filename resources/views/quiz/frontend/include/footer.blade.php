<section class="footer-section">
    <div class="footer-top">
      <div class="row m-0">
        <div class=" col-12 col-md-6 col-lg-4">
          <h5 class="mb-4 pb-4 footer-tlt">About Us</h5>
          <ul class="p-0">
            <li class="pb-2">
              <a class=" pb-2" href="{{route('aboutus')}}">About us</a>
            </li>
            <li class="pb-2">
              <a class="" href="{{route('terms-and-conditions')}}">Terms and conditions</a>
            </li>
            <li class="pb-2">
              <a class="" href="{{route('privacy-policy')}}">privacy policy</a>
            </li>
          </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4">
          <h5 class="mb-4 pb-4 footer-tlt">Help & support</h5>
          <ul class="p-0">
            <li class="pb-2">
              <a class="" href="{{route('faq')}}">Faq</a>
            </li>
            <li class="pb-2">
              <a class="" href="{{route('contactus')}}">Contact us</a>
            </li>
          </ul>
        </div>
        <div class="col-12 col-md-6 col-lg-4">
          <h5 class="mb-4 pb-4 footer-tlt">Follow us on</h5>
          
          <div class="social-menu pt-3">
            <a class="social-l fac" target="_blank" href="{{$fContact->fb}}"><i class="fab fa-facebook-f"></i></a>
            <a class="social-l tw" target="_blank" href="{{$fContact->tw}}"><i class="fab fa-twitter"></i></a>
            <a class="social-l you" target="_blank" href="{{$fContact->yt}}"><i class="fab fa-youtube"></i></a>
            <a class="social-l pin" target="_blank" href="{{$fContact->pnt}}"><i class="fab fa-pinterest"></i></a>
            <a class="social-l ins" target="_blank" href="{{$fContact->ins}}"><i class="fab fa-instagram"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>