@extends('quiz.admin.layout.layout')

@section('title')
    Answered Questions
@endsection

@section('cssfile')

@endsection

@section('style')
    <style type="text/css">
        .ti-check{
            color: #11deb8;
        }
        .ti-close{
            color: red;
        }
        .answerDetals p{
            font-size: 16px;
            margin-bottom: 4px;
        }
        .answerDetals span{
            color:#11deb8;
        }
        .qtitle{
            color:#11deb8;
        }
    </style>
@endsection

@section('content')
<!-- Page-header start -->
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="page-header-title">
                    <h5 class="m-b-10">Answered Questions</h5>
                    <p class="m-b-0">This particular participant amswer all this question</p>
                </div>
            </div>
            <div class="col-md-4">
               <a href="{{Helper::backFromAnswerdQuestion($status,$quiz_id)}}" class="btn waves-effect waves-light btn-success btn-skew fl-right bck-btn">Back</a>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <div class="row">
                    @include('quiz.admin.global.view-answer.info')
                    @include('quiz.admin.global.view-answer.list')
                </div>
                <div class="row">
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsfile')

@endsection

@section('script')

@endsection
