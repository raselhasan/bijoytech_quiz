<!DOCTYPE html>
<html>
<head>
	<title>Verification Code</title>
</head>
<body>
	<h1>Hi {{$user->name}}</h1>
	<p>Your reset password verification code is: {{$code}}</p>
</body>
</html>