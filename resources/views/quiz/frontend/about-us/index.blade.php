@extends('quiz.frontend.layout.app')

@section('title')
	About Us
@endsection

@section('style')
	<style type="text/css">
		h4{
			color: yellowgreen;
			text-transform: uppercase;
		}
		hr{
			background: #448aff;
    		width: 16%;
		}
		.card{
			margin-top: 20px;
		}
	</style>
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="container">
		<div class="row mr-0">
			<div class="col-12 col-md-12 col-lg-12 pr-0 cont-sec">
				<h4 class="text-center">@if($about->title){{$about->title}} @else About Us @endif</h4>
				<hr/>
				{!! $about->desc !!}
			</div>
			<div class="col-12 col-md-12 col-lg-12 pr-0">
				<h4 class="text-center">Our Team</h4>
				<hr/>
			</div>

			@if(count($teams) > 0)
				@foreach( $teams as $team)
					@php
						$content = '';
		                if(strlen($team->desc) > 100){
		                    $content = substr($team->desc, 0, 100).'....';
		                }else{
		                    $content = $team->desc;
		                }
					@endphp
					<div class="col-3 col-md-3 col-lg-3 pr-0">
						<div class="card">
						  	<img class="card-img-top img-fluid" src="{{asset('teams/'.$team->image)}}" alt="Card image cap" style="height: 300px">
						  	<div class="card-body">
							    <h5 class="card-title text-center">{{$team->name}}</h5>
							    <h6 class="card-title text-center">{{$team->deg}}</h6>
							    <p class="card-text">{!! $content !!}</p>
							    <div class="text-center">
							    <a href="{{route('team',['id'=>$team->id])}}" class="btn btn-primary" >Read More</a>
							    </div>
						  	</div>
						</div>
					</div>
				@endforeach
			@endif		
			


		</div>
	</div>
</section>
@endsection

@section('script')
	
@endsection
