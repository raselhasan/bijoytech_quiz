<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Quiz Details</h5>
            <span>About Quiz or Quiz Information</span>
        </div>
        <div class="card-block">
            <div class="quiz-dts">
                <p><span>{{$quiz->title}}</span></p>
                <p><span>From</span> {{date('m/d/Y h:i A',strtotime($quiz->start_date))}} - {{date('m/d/Y h:i A',strtotime($quiz->end_date))}}</p>
                @if($quiz->price)
                    <p>${{number_format($quiz->price, 2)}}</p>
                @endif    
            </div>
        </div>
    </div>
</div>