<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Home Content</h5>
                <span>List of home page contents</span>
            </div>
            
            <div class="card-block">
                @include('quiz.admin.home.table-data')
            </div>
        </div>
    </div>
</div>
