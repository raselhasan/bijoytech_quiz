<form action="{{route('admin.video.update',['quiz_id'=>$video->quiz_id,'video_id'=>$video->id])}}" method="post" id="edit-video-form">
    @csrf
    <div class="modal fade" id="edit-video-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Video</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="row">
                       <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Video Title</label>
                                <input type="text" name="title" class="form-control" required="" value="{{$video->title}}">
                            </div>
                       </div>
                       <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Language</label>
                                <select class="form-control" name="language" id="exampleFormControlSelect1" required>
                                  <option value="English" @if($video->language == 'English') selected @endif>English</option>
                                  <option value="Bangla" @if($video->language == 'Bangla') selected @endif>Bangla</option>
                                </select>
                            </div>
                       </div>
                       <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Video Embed Url</label>
                                <textarea class="form-control" name="video" id="exampleFormControlTextarea2" rows="3">{{$video->video}}</textarea>
                            </div>
                       </div>
                       <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Description</label>
                                <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3">{{$video->description}}</textarea>
                            </div>
                       </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
