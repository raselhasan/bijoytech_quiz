<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Mail;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    public function index()
    {
    	$data['contact'] = Contact::orderBy('id','desc')->first();
    	return view('quiz.admin.contact.index', $data);
    }

    public function save(Request $request)
    {	
    	if($request->id){
    		$contact  = Contact::findOrFail($request->id);
    		$contact->fill($request->all())->save();
    	}else{
    		Contact::create($request->all());
    	}
    	
    	return redirect()->back()->with('success','Saved');
    }
    public function sendContact( Request $request)
    {
        Mail::send( new ContactMail($request->all()));
        return redirect()->back()->with('success','Thank your for contact. We will response you soon.');
    }
}
