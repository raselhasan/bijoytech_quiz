<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $code;

    public function __construct( $user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message  = $this->from('info@globaltaxnyc.com', 'Global Multi Services Inc')->view('quiz.mail.verificatiion',['user'=>$this->user,'code'=>$this->code])->subject('Reset Password')->to(trim($this->user->email));
        return $message;
    }
}
