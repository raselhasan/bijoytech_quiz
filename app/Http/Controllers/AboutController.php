<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use Validator;
use App\Team;
class AboutController extends Controller
{
    public function index()
    {
    	$data['about'] = About::orderBy('id','desc')->first();
    	return view('quiz.admin.about.index', $data);
    }
    public function save(Request $request)
    {	
    	if($request->id){
    		$about  = About::findOrFail($request->id);
    		$about->fill($request->all())->save();
    	}else{
    		About::create($request->all());
    	}
    	
    	return redirect()->back()->with('success','Saved');
    }

    public function getTeam( Request $request )
    {
    	$columns = array( 
            0 => 'image', 
            1 => 'name',
            2 => 'deg',
            2 => 'desc',
            2 => 'id',
        );
  
        $totalData = Team::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = Team::offset($start)
                 ->limit($limit)
                 ->orderBy('id','desc')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = Team::where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('name', 'LIKE',"%{$search}%")
                        ->orWhere('deg', 'LIKE',"%{$search}%")
                        ->orWhere('desc', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = Team::where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('name', 'LIKE',"%{$search}%")
                        ->orWhere('deg', 'LIKE',"%{$search}%")
                        ->orWhere('desc', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
            	$img_url = url('teams/'.$post->image);
                $nestedData['image'] = '<img src="'.$img_url.'" style="width:50px; height:50px; border-radius:100%">';
                $content = '';
                if(strlen($post->desc) > 70){
                    $content = substr($post->desc, 0, 70).'....';
                }else{
                    $content = $post->desc;
                }
                $nestedData['desc'] = $content;
                $nestedData['name'] = $post->name;
                $nestedData['deg'] = $post->deg;
                $nestedData['options'] = '
                <button team-id="'.$post->id.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn edt-team-btn" data-toggle="tooltip" data-placement="top" title="Edit"><i class="ti-slice"></i></button>
                <button team-id="'.$post->id.'" class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn team-dlt-btn" data-toggle="tooltip" data-placement="top" title="Delete"><i class="ti-trash"></i></button>
                ';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }

    public function addTeam(Request $request)
    {
    	$data = $request->all();
    	if($request->file){
    		$validator = Validator::make($request->all(), [
            'file' => 'mimes:jpeg,jpg,png',
	        ]);

	        if ($validator->fails()) {
	            return redirect()->back()->with('error','The files must be a file of type: jpeg, jpg, png');
	        }

	        $file = $request->file('file');
	        $extention = $file->getClientOriginalExtension();
	        $fileName = time().'.'.$extention;
	        $path = public_path('teams');
	        $file->move($path,$fileName);
	        $data = array_merge($request->all(), ['image' => $fileName]);
    	}
    	
    	if($request->id){
    		$team  = Team::findOrFail($request->id);
    		$team->fill($data)->save();
    	}else{
    		Team::create($data);
    	}
    	
    	return redirect()->back()->with('success','Saved');
    }

    public function deleteTeam(Request $request)
    {
    	Team::where('id',$request->id)->delete();
    	return 'success';
    }

    public function editTeam(Request $request)
    {
    	$data['team'] = Team::where('id',$request->id)->first();
    	return view('quiz.admin.about.add-team',$data)->render();
    }
}
