<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MoneyRequest;
use App\PaymentHistory;

class MoneryRequestController extends Controller
{
    public function index()
    {
    	return view('quiz.admin.withdrawal-request.index');
    }

    public function getPost(Request $request)
    {
    	$columns = array( 
            0 => 'created_at', 
            1 => 'name',
            2 => 'phone',
            3 => 'current_balance',
            4 => 'requested_amount',
            5 => 'paid',
            6 => 'paid_status',
            7 => 'id',
        );
  
        $totalData = MoneyRequest::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = MoneyRequest::offset($start)
                 ->limit($limit)
                 ->orderBy('id','desc')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts =  MoneyRequest::where('id','LIKE',"%{$search}%")
                ->orWhere('created_at', 'LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('current_balance', 'LIKE',"%{$search}%")
                ->orWhere('requested_amount', 'LIKE',"%{$search}%")
                ->orWhere('paid', 'LIKE',"%{$search}%")
                ->orWhere('due', 'LIKE',"%{$search}%")
                ->orWhere('paid_status', 'LIKE',"%{$search}%")
                ->orWhere('phone', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = MoneyRequest::where('id','LIKE',"%{$search}%")
	            ->orWhere('created_at', 'LIKE',"%{$search}%")
                ->orWhere('name', 'LIKE',"%{$search}%")
                ->orWhere('current_balance', 'LIKE',"%{$search}%")
                ->orWhere('requested_amount', 'LIKE',"%{$search}%")
                ->orWhere('paid', 'LIKE',"%{$search}%")
                ->orWhere('due', 'LIKE',"%{$search}%")
                ->orWhere('paid_status', 'LIKE',"%{$search}%")
                ->orWhere('phone', 'LIKE',"%{$search}%")
	            ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                
                $nestedData['created_at'] = date('m/d/Y', strtotime($post->created_at));
                $nestedData['name'] = $post->name;
                $nestedData['phone'] = $post->phone;
                $nestedData['current_balance'] = '$ '.number_format($post->current_balance, 2);
                $nestedData['requested_amount'] = '$ '.number_format($post->requested_amount, 2);
                $nestedData['paid'] = '$ '.number_format($post->paid, 2);
                $nestedData['paid_status'] = $post->paid_status;
                $nestedData['options'] = '<button w-request-id="'.$post->id.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn edt-money-btn" data-toggle="tooltip" data-placement="top" title="Make Paid"><i class="ti-slice"></i></button>
				<button w-request-id="'.$post->id.'" class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn money-dlt-btn" data-toggle="tooltip" data-placement="top" title="Delete Request"><i class="ti-trash"></i></button>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 

    }

    public function getSingleRequest(Request $request)
    {
    	$data['moneyRequest'] = MoneyRequest::find($request->id)->first();
    	return view('quiz.admin.withdrawal-request.make-paid', $data)->render();
    }

    public function makePaid(Request $request)
    {
    	$payment = MoneyRequest::find($request->id);
    	$payment->paid = $request->paid;
    	$payment->paid_status = $request->paid_status;
    	$payment->payment_method = $request->payment_method;
    	$payment->payment_id = $request->payment_id;
        $payment->paid_date = date('Y-m-d');
        $payment->message = $request->message;
    	$payment->save();

    	$history = new PaymentHistory();
    	$history->name = $payment->name;
    	$history->user_id = $payment->user_id;
    	$history->withdrawal_id = $payment->id;
        $history->amount = $request->paid;
    	$history->payment_method = $request->payment_method;
    	$history->payment_id = $request->payment_id;
    	$history->save();

    	return 'success';
    }

    public function delete(Request $request)
    {
    	MoneyRequest::where('id',$request->id)->delete();
    	return 'success';
    }
}
