<form action="{{route('admin.logo.upload')}}" method="post" class="saveLogo" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Logo</h5>
                    <span></span>
                </div>
                <input type="hidden" name="status" value="1">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Select Logo</label>
                                <input type="file" name="logo">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button class="btn waves-effect waves-light btn-primary btn-skew fl-right" type="submit">Upload</button>
                        </div>
                    </div>
                    <div class="row add-logo-1">
                        <div class="col-md-12">
                            <img src="{{asset('logo/'.$setting->logo)}}" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>