@extends('quiz.frontend.layout.app')
@section('style')
	<style type="text/css">
		input[type="date"]::-webkit-calendar-picker-indicator {
		  cursor: pointer;
		  border-radius: 4px;
		  margin-right: 2px;
		  opacity: 0.6;
		  filter: invert(0.8);
		}
		input[type="date"]::-webkit-calendar-picker-indicator:hover {
		  opacity: 1
		}
	</style>
@endsection
@section('content')
<section class="main-section">
	<div class="log-cnt reg-cnt">
		<div class="row mx-0">
			<div class="col-7 px-0">
				<div class="form">
					<h4 class="text-center mb-4 text-uppercase">Register</h5>
					@if(Session::has('error'))
						<p class="err-msg">{{Session::get('error')}}</p>
					@endif	
					<form action="{{route('user.register')}}" method="post">
						@csrf
						<div class="row mr-0">
							<div class="col-12 col-md-12 pr-0">
								<div class="form-group">
									<label class="mb-2">Full Name</label>
									<input type="text" class="form-control" name="name" required value="{{old('name')}}">
								</div>
							</div>
							<div class="col-12 col-md-12 pr-0">
								<div class="form-group">
									<label class="mb-2">Email</label>
									<input type="email" class="form-control" name="email" required value="{{old('email')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Password</label>
									<input type="password" class="form-control" name="password" required>
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Date of Birth</label>
									<input type="date" class="form-control" name="date_of_birth" required value="{{old('date_of_birth')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Grade</label>
									<select id="inputState" class="form-control" name="grade" required>
							        	<option value="">Choose...</option>
							        	<option value="0-5 grade" @if(old('grade') == '0-5 grade') selected @endif >0-5 Grade</option>
							        	<option value="6-8 grade" @if(old('grade') == '6-8 grade') selected @endif >6-8 Grade</option>
							        	<option value="9-12 grade" @if(old('grade') == '9-12 grade') selected @endif >9-12 Grade</option>
							        	<option value="above/adult" @if(old('grade') == 'above/adult') selected @endif >Above/Adult</option>
							      	</select>
								</div>
							</div>

							
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Phone</label>
									<input type="text" class="form-control" name="phone" required value="{{old('phone')}}">
								</div>
							</div>
							<div class="col-12 col-md-12 pr-0">
								<div class="form-group">
									<label class="mb-2">Address</label>
									<input type="text" class="form-control" name="address" required value="{{old('phone')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">City</label>
									<input type="text" class="form-control" name="city" value="{{old('city')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">State</label>
									<input type="text" class="form-control" name="state" value="{{old('state')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Zip</label>
									<input type="text" class="form-control" name="zip" value="{{old('zip')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Country</label>
									<select id="inputState" class="form-control" name="country" required>
							        	<option value="">Choose...</option>
							        	@foreach($countries as $country)
							        		<option value="{{$country->name}}" @if(old('country') == $country->name) selected @endif @if(!old('country')) @if($country->iso == $c_country) selected @endif @endif >{{$country->name}}</option>
							        	@endforeach	
							      	</select>
								</div>
							</div>
							
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Guardian</label>
									<input type="text" class="form-control" name="guardian" value="{{old('guardian')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Relation with Guardian</label>
									<input type="text" class="form-control" name="relation_with_guardian" value="{{old('relation_with_guardian')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Guardian Phone</label>
									<input type="text" class="form-control" name="guardian_phone" value="{{old('guardian_phone')}}">
								</div>
							</div>
							<div class="col-12 col-md-6 pr-0">
								<div class="form-group">
									<label class="mb-2">Guardian Email</label>
									<input type="email" class="form-control" name="guardian_email" value="{{old('guardian_email')}}">
								</div>
							</div>
						</div>
						<div class="form-group pt-4">
							<button type="submit" class="btn btn-primary btn-block">Register</button>
						</div>
						<div class="form-group text-center" style="margin-top: 40px">
							Do you have an account? <a href="{{route('user.login')}}">Login</a>
						</div>
					</form>
				</div> 
			</div>
			<div class="col-5 px-0">
				<div class="lr-cover">
					<img class="cover"  src="{{asset('files/assets/images/log.jpg')}}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')

@endsection
