
<div class="modal fade" id="view-user-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$user->name}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="row">
                   <div class="col-md-12 text-center">
                      @if($user->image)
                        <img src="{{asset('user_image/'.$user->image)}}" class="img-fluid userImg">
                      @else 
                        <img src="{{asset('user_image/default.jpg')}}" class="img-fluid userImg" >
                      @endif  
                   </div>
                   <div class="col-md-6 userInfo">
                      @if($user->name)
                        <p><span>Name: </span>{{$user->name}}</p>
                      @endif
                      @if($user->email)  
                        <p><span>Email: </span>{{$user->email}}</p>
                      @endif 
                      @if($user->phone)   
                        <p><span>Phone: </span>{{$user->phone}}</p>
                      @endif
                      @if($user->date_of_birth)  
                        <p><span>Date Of Birth: </span>{{$user->date_of_birth}}</p>
                      @endif 
                      
                      @if($user->guardian)   
                        <p><span>Guardian: </span>{{$user->guardian}}</p>
                      @endif
                      @if($user->relation_with_guardian)     
                        <p><span>Relation with Guardian: </span>{{$user->relation_with_guardian}}</p>
                      @endif  
                   </div>
                   <div class="col-md-6 userInfo">
                      @if($user->country)
                        <p><span>Country: </span>{{$user->country}}</p>
                      @endif
                      @if($user->address)  
                        <p><span>Address: </span>{{$user->address}}</p>
                      @endif 
                      @if($user->city)   
                        <p><span>City: </span>{{$user->city}}</p>
                      @endif
                      @if($user->state)  
                        <p><span>State: </span>{{$user->state}}</p>
                      @endif 
                      
                      @if($user->zip)   
                        <p><span>Zip Code: </span>{{$user->zip}}</p>
                      @endif
                      @if($user->grade)     
                        <p><span>Grade: </span>{{$user->grade}}</p>
                      @endif  
                   </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
