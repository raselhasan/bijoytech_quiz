@extends('quiz.frontend.layout.app')
@section('style')

@endsection
@section('content')
<section class="main-section p-0"> 
	<div class="row m-0">
	<div class="col-12 col-lg-3 p-0">
		@include('quiz.frontend.include.left-bar')
	</div>
	<div class="col-12 col-lg-9 p-0">
		<div class="main-body">
			<div class="row mr-0">
				@for($i=0;$i<9;$i++)
				<div class="col-12 col-md-6 col-lg-4 pr-0">
					<div class="card mb-3">
					<div class="card-image">
					<img class="cover"  src="{{asset('files/assets/images/cover1.jpg')}}">
					</div>
						<div class="card-header text-center p-3">
							<a href="{{url('/question')}}"><h5 class="mb-0">Hello card</h5></a>
						</div>
						<div class="card-block pt-0">
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor

						</div>
					</div>
				</div>
				@endfor
			</div>
		</div>
	</div>
</div>
</section>
@endsection

@section('script')

@endsection
