<form action="{{route('admin.about.save')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add About</h5>
                    <span>Add New about page content here</span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <input type="hidden" name="id" value="{{@$about->id}}">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Title</label>
                                <input type="text" name="title" class="form-control" value="{{@$about->title}}" required>
                            </div>
                        </div>
                              
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Content</label>
                                <textarea id="about-content" name="desc">{{@$about->desc}}</textarea>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

