<form action="{{route('admin.faq.save')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Faq</h5>
                    <span>Add New faq page content here</span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <input type="hidden" name="id" value="{{@$faq->id}}">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Title</label>
                                <input type="text" name="title" class="form-control" value="{{@$faq->title}}" required>
                            </div>
                        </div>
                              
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Content</label>
                                <textarea id="faq-content" name="desc">{{@$faq->desc}}</textarea>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>