@extends('quiz.frontend.layout.app')

@section('title')
	Privacy and Policy
@endsection

@section('style')
	<style type="text/css">
		h4{
			color: yellowgreen;
			text-transform: uppercase;
		}
		hr{
			background: #448aff;
    		width: 16%;
		}
		
	</style>
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="container">
		<div class="row mr-0">
			<div class="col-12 col-md-12 col-lg-12 pr-0 cont-sec">
				<h4 class="text-center">privacy and policy</h4>
				<hr/>
				{!! $setting->privacy_policy!!}
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')
	
@endsection
