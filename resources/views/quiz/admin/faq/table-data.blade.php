<div class="dt-responsive table-responsive">
    <table id="order-table" class="table table-striped table-bordered nowrap">
        <thead>
            <tr>
                <th>Question</th>
                <th>Answer</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Question</th>
                <th>Answer</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
</div>