-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2021 at 03:40 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bijoytech_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `desc`, `created_at`, `updated_at`) VALUES
(2, 'About Us', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', '2021-04-30 18:56:39', '2021-04-30 20:15:36');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` bigint(20) UNSIGNED NOT NULL,
  `questionId` bigint(20) UNSIGNED NOT NULL,
  `userId` bigint(20) UNSIGNED NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rite_answer` int(11) DEFAULT NULL,
  `worng_answer` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `book` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `quiz_id`, `title`, `language`, `book`, `active`, `description`, `created_at`, `updated_at`) VALUES
(12, 17, 'Bangla Grammer', 'Bangla', '1617036972.pdf', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum', '2021-03-22 17:47:37', '2021-04-15 11:06:30'),
(13, 17, 'English Granmmer', 'English', '1617036946.pdf', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum', '2021-03-28 18:58:23', '2021-04-15 11:06:25'),
(15, 20, 'dfsdaf', 'English', '1618589601.pdf', 0, 'sadfdsaf', '2021-04-13 18:45:42', '2021-04-16 16:13:21');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pnt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ins` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `fb`, `tw`, `pnt`, `yt`, `ins`, `contact`, `created_at`, `updated_at`) VALUES
(4, 'Faebook Url', 'Twitter Url', 'Pinterest Url', 'Youtube Url', 'Instagram Url', '<p>GLOBAL MULTI SERVICES, INC.</p><p>Tel: (718) 205-2360</p><p>Cell: (917) 345-4850</p><p>Email: globaloffice2006@gmail.com</p><p>37-18 74th Street, Suite 202, Jackson Heights, NY 11372</p>', '2021-04-30 02:20:57', '2021-04-30 20:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'FREQUENTLY ASKED QUESTIONS (FAQ)', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', '2021-04-30 02:34:32', '2021-05-01 19:46:49');

-- --------------------------------------------------------

--
-- Table structure for table `home_contents`
--

CREATE TABLE `home_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `quiz_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_contents`
--

INSERT INTO `home_contents` (`id`, `title`, `content`, `img`, `status`, `quiz_name`, `created_at`, `updated_at`) VALUES
(1, 'General Quiz Competition', '<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', '1619895013.jpg', 1, NULL, '2021-04-07 14:58:46', '2021-05-03 00:57:22'),
(3, 'Reading Quiz Competition', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', '1619894983.jpg', 3, NULL, '2021-04-07 16:03:13', '2021-05-03 00:55:55'),
(4, 'Video Quiz Competition', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing</p>', '1619894933.jpg', 4, NULL, '2021-04-07 16:04:09', '2021-05-03 00:55:42'),
(6, 'Spelling Quiz Competition', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '1619894865.png', 2, NULL, '2021-04-07 16:04:56', '2021-05-03 00:57:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_11_194413_create_quizzes_table', 2),
(5, '2021_02_03_185550_create_books_table', 3),
(8, '2021_02_03_215242_create_questions_table', 4),
(9, '2021_02_03_215644_create_question_options_table', 4),
(11, '2021_03_20_205542_create_money_requests_table', 5),
(12, '2021_03_20_231036_create_payment_histories_table', 6),
(13, '2021_03_22_225724_create_videos_table', 7),
(14, '2021_03_28_235856_create_verifications_table', 8),
(15, '2021_04_02_193733_create_answers_table', 9),
(16, '2021_04_03_212955_create_participant_users_table', 10),
(17, '2021_04_07_204253_create_home_contents_table', 11),
(18, '2021_04_08_010131_create_settings_table', 12),
(19, '2021_04_30_080348_create_contacts_table', 13),
(20, '2021_04_30_082449_create_faqs_table', 14),
(21, '2021_04_30_084217_create_qnas_table', 15),
(22, '2021_05_01_004936_create_abouts_table', 16),
(23, '2021_05_01_005343_create_teams_table', 17);

-- --------------------------------------------------------

--
-- Table structure for table `money_requests`
--

CREATE TABLE `money_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_balance` double(8,2) NOT NULL,
  `requested_amount` double(8,2) NOT NULL,
  `paid` double(8,2) DEFAULT NULL,
  `due` double(8,2) DEFAULT NULL,
  `paid_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `payment_method` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_date` date DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `participant_users`
--

CREATE TABLE `participant_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `book_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` bigint(20) UNSIGNED NOT NULL,
  `total_right_ans` bigint(20) UNSIGNED DEFAULT NULL,
  `total_worng_ans` bigint(20) UNSIGNED DEFAULT NULL,
  `position` bigint(20) UNSIGNED DEFAULT NULL,
  `aword` double(8,2) DEFAULT NULL,
  `total_mark` float DEFAULT NULL,
  `date` date DEFAULT NULL,
  `language` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_histories`
--

CREATE TABLE `payment_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `withdrawal_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qnas`
--

CREATE TABLE `qnas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `qnas`
--

INSERT INTO `qnas` (`id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(7, 'What is the capital of Bangladesh?', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley</p>', '2021-04-30 20:23:52', '2021-04-30 20:23:52'),
(8, 'What is the capital of India.............?', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley</p>', '2021-04-30 20:24:28', '2021-04-30 20:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `language` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `variation_id`, `question`, `answer`, `status`, `language`, `created_at`, `updated_at`) VALUES
(16, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---2', 'tried', 3, NULL, '2021-03-29 17:00:12', '2021-03-29 17:00:12'),
(17, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---3', 'tried', 3, NULL, '2021-03-29 17:00:19', '2021-03-29 17:00:19'),
(18, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---4', 'tried', 3, NULL, '2021-03-29 17:00:27', '2021-03-29 17:00:27'),
(19, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---5', 'tried', 3, NULL, '2021-03-29 17:00:32', '2021-03-29 17:00:32'),
(20, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---6', 'tried', 3, NULL, '2021-03-29 17:00:37', '2021-03-29 17:00:37'),
(21, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---7', 'tried', 3, NULL, '2021-03-29 17:00:42', '2021-03-29 17:00:42'),
(22, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---8', 'tried', 3, NULL, '2021-03-29 17:00:48', '2021-03-29 17:00:48'),
(23, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---9', 'tried', 3, NULL, '2021-03-29 17:00:53', '2021-03-29 17:00:53'),
(24, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---10', 'tried', 3, NULL, '2021-03-29 17:00:59', '2021-03-29 17:00:59'),
(25, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---11', 'tried', 3, NULL, '2021-03-29 17:01:06', '2021-03-29 17:01:06'),
(26, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---12', 'tried', 3, NULL, '2021-03-29 17:01:11', '2021-03-29 17:01:11'),
(27, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---13', 'tried', 3, NULL, '2021-03-29 17:01:17', '2021-03-29 17:01:17'),
(28, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---14', 'tried', 3, NULL, '2021-03-29 17:01:22', '2021-03-29 17:01:22'),
(29, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---15', 'tried', 3, NULL, '2021-03-29 17:01:26', '2021-03-29 17:01:26'),
(30, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---16', 'tried', 3, NULL, '2021-03-29 17:01:31', '2021-03-29 17:01:31'),
(31, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---17', 'tried', 3, NULL, '2021-03-29 17:01:38', '2021-03-29 17:01:38'),
(32, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---18', 'tried', 3, NULL, '2021-03-29 17:01:43', '2021-03-29 17:01:43'),
(33, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---19', 'tried', 3, NULL, '2021-03-29 17:01:48', '2021-03-29 17:01:48'),
(34, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---20', 'tried', 3, NULL, '2021-03-29 17:01:53', '2021-03-29 17:01:53'),
(35, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---21', 'tried', 3, NULL, '2021-03-29 17:01:58', '2021-03-29 17:01:58'),
(36, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---22', 'tried', 3, NULL, '2021-03-29 17:02:03', '2021-03-29 17:02:03'),
(37, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---23', 'tried', 3, NULL, '2021-03-29 17:02:07', '2021-03-29 17:02:07'),
(38, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---24', 'tried', 3, NULL, '2021-03-29 17:02:12', '2021-03-29 17:02:12'),
(39, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---25', 'tried', 3, NULL, '2021-03-29 17:02:16', '2021-03-29 17:02:16'),
(40, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---26', 'tried', 3, NULL, '2021-03-29 17:02:21', '2021-03-29 17:02:21'),
(41, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---27', 'tried', 3, NULL, '2021-03-29 17:02:25', '2021-03-29 17:02:25'),
(42, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---28', 'tried', 3, NULL, '2021-03-29 17:02:31', '2021-03-29 17:02:31'),
(43, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---29', 'tried', 3, NULL, '2021-03-29 17:02:36', '2021-03-29 17:02:36'),
(44, 13, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---30', 'tried', 3, NULL, '2021-03-29 17:02:42', '2021-03-29 17:02:42'),
(45, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---31', 'tried', 3, NULL, '2021-03-29 17:10:40', '2021-03-29 17:10:40'),
(46, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---32', 'tried', 3, NULL, '2021-03-29 17:10:44', '2021-03-29 17:10:44'),
(47, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---33', 'tried', 3, NULL, '2021-03-29 17:10:49', '2021-03-29 17:10:49'),
(48, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---34', 'tried', 3, NULL, '2021-03-29 17:10:54', '2021-03-29 17:10:54'),
(49, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---35', 'tried', 3, NULL, '2021-03-29 17:10:58', '2021-03-29 17:10:58'),
(50, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---36', 'tried', 3, NULL, '2021-03-29 17:11:04', '2021-03-29 17:11:04'),
(51, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---37', 'tried', 3, NULL, '2021-03-29 17:11:08', '2021-03-29 17:11:08'),
(52, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---38', 'tried', 3, NULL, '2021-03-29 17:11:12', '2021-03-29 17:11:12'),
(53, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---39', 'tried', 3, NULL, '2021-03-29 17:11:16', '2021-03-29 17:11:16'),
(54, 12, 'Don\'t make so much noise. Noriko ..... to study for her ESL test ---40', 'tried', 3, NULL, '2021-03-29 17:11:25', '2021-03-29 17:11:25'),
(55, 3, 'What is Video ----1', 'Kamal', 4, NULL, '2021-04-02 16:18:01', '2021-04-02 16:18:01'),
(56, 3, 'What is Video ----2', 'Kamal', 4, NULL, '2021-04-02 16:18:06', '2021-04-02 16:18:06'),
(57, 3, 'What is Video ----3', 'Kamal', 4, NULL, '2021-04-02 16:18:11', '2021-04-02 16:18:11'),
(58, 3, 'What is Video ----4', 'Kamal', 4, NULL, '2021-04-02 16:18:16', '2021-04-02 16:18:16'),
(59, 3, 'What is Video ----5', 'Kamal', 4, NULL, '2021-04-02 16:18:21', '2021-04-02 16:18:21'),
(60, 3, 'What is Video ----6', 'Kamal', 4, NULL, '2021-04-02 16:18:26', '2021-04-02 16:18:26'),
(61, 3, 'What is Video ----7', 'Kamal', 4, NULL, '2021-04-02 16:18:31', '2021-04-02 16:18:31'),
(62, 3, 'What is Video ----8', 'Kamal', 4, NULL, '2021-04-02 16:18:36', '2021-04-02 16:18:36'),
(63, 3, 'What is Video ---- 9', 'Kamal', 4, NULL, '2021-04-02 16:18:42', '2021-04-02 16:18:42'),
(64, 3, 'What is Video ---- 10', 'Kamal', 4, NULL, '2021-04-02 16:18:46', '2021-04-02 16:18:46'),
(65, 21, 'General Quiz ---------------------- 1', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:16', '2021-04-02 17:07:16'),
(66, 21, 'General Quiz ---------------------- 1', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:19', '2021-04-02 17:07:19'),
(67, 21, 'General Quiz ---------------------- 3', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:25', '2021-04-02 17:07:25'),
(68, 21, 'General Quiz ---------------------- 4', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:29', '2021-04-02 17:07:29'),
(69, 21, 'General Quiz ---------------------- 5', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:34', '2021-04-02 17:07:34'),
(70, 21, 'General Quiz ---------------------- 6', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:38', '2021-04-02 17:07:38'),
(71, 21, 'General Quiz ---------------------- 6', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:42', '2021-04-02 17:07:42'),
(72, 21, 'General Quiz ---------------------- 8', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:47', '2021-04-02 17:07:47'),
(73, 21, 'General Quiz ---------------------- 9', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:51', '2021-04-02 17:07:51'),
(74, 21, 'General Quiz ---------------------- 10', 'avbbbbbb', 1, 'English', '2021-04-02 17:07:56', '2021-04-02 17:07:56'),
(75, 21, 'General Quiz ---------------------- 10', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:07', '2021-04-02 17:08:07'),
(76, 21, 'General Quiz ---------------------- 9', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:13', '2021-04-02 17:08:13'),
(77, 21, 'General Quiz ---------------------- 8', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:17', '2021-04-02 17:08:17'),
(78, 21, 'General Quiz ---------------------- 6', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:27', '2021-04-02 17:08:27'),
(79, 21, 'General Quiz ---------------------- 4', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:36', '2021-04-02 17:08:36'),
(80, 21, 'General Quiz ---------------------- 3', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:42', '2021-04-02 17:08:42'),
(81, 21, 'General Quiz ---------------------- 2', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:46', '2021-04-02 17:08:46'),
(82, 21, 'General Quiz ---------------------- 1', 'avbbbbbb', 1, 'Bangla', '2021-04-02 17:08:51', '2021-04-02 17:08:51'),
(83, 25, 'Answer first question ------------------------- 1', 'yyyy', 2, 'English', '2021-04-02 17:53:34', '2021-04-02 17:53:34'),
(84, 25, 'Answer first question ------------------------- 2', 'yyyy', 2, 'English', '2021-04-02 17:53:40', '2021-04-02 17:53:40'),
(85, 25, 'Answer first question ------------------------- 3', 'yyyy', 2, 'English', '2021-04-02 17:53:45', '2021-04-02 17:53:45'),
(86, 25, 'Answer first question ------------------------- 4', 'yyyy', 2, 'English', '2021-04-02 17:53:50', '2021-04-02 17:53:50'),
(87, 25, 'Answer first question ------------------------- 5', 'yyyy', 2, 'English', '2021-04-02 17:53:55', '2021-04-02 17:53:55'),
(88, 25, 'Answer first question ------------------------- 6', 'yyyy', 2, 'English', '2021-04-02 17:53:59', '2021-04-02 17:53:59'),
(89, 25, 'Answer first question ------------------------- 7', 'yyyy', 2, 'English', '2021-04-02 17:54:04', '2021-04-02 17:54:04'),
(90, 25, 'Answer first question ------------------------- 8', 'yyyy', 2, 'English', '2021-04-02 17:54:11', '2021-04-02 17:54:11'),
(91, 25, 'Answer first question ------------------------- 9', 'yyyy', 2, 'English', '2021-04-02 17:54:15', '2021-04-02 17:54:15'),
(92, 25, 'Answer first question ------------------------- 10', 'yyyy', 2, 'English', '2021-04-02 17:54:20', '2021-04-02 17:54:20'),
(93, 1, 'This is Video Question ----- 1', 'Dhaka', 4, NULL, '2021-04-04 15:40:18', '2021-04-04 15:40:18'),
(94, 1, 'This is Video Question ----- 2', 'Dhaka', 4, NULL, '2021-04-04 15:40:24', '2021-04-04 15:40:24'),
(95, 1, 'This is Video Question ----- 3', 'Dhaka', 4, NULL, '2021-04-04 15:40:29', '2021-04-04 15:40:29'),
(96, 1, 'This is Video Question ----- 4', 'Dhaka', 4, NULL, '2021-04-04 15:40:33', '2021-04-04 15:40:33'),
(97, 1, 'This is Video Question ----- 6', 'Dhaka', 4, NULL, '2021-04-04 15:40:43', '2021-04-04 15:40:43'),
(98, 1, 'This is Video Question ----- 7', 'Dhaka', 4, NULL, '2021-04-04 15:40:48', '2021-04-04 15:40:48'),
(99, 1, 'This is Video Question ----- 8', 'Dhaka', 4, NULL, '2021-04-04 15:40:53', '2021-04-04 15:40:53'),
(100, 1, 'This is Video Question ----- 9', 'Dhaka', 4, NULL, '2021-04-04 15:40:58', '2021-04-04 15:40:58'),
(101, 1, 'This is Video Question ----- 10', 'Dhaka', 4, NULL, '2021-04-04 15:41:02', '2021-04-04 15:41:02'),
(102, 25, 'What is the capital op bangadesh? -------1', 'Dhaka', 2, 'Bangla', '2021-04-05 16:53:52', '2021-04-05 16:53:52'),
(103, 25, 'What is the capital op bangadesh? -------2', 'Dhaka', 2, 'Bangla', '2021-04-05 16:53:58', '2021-04-05 16:53:58'),
(104, 25, 'What is the capital op bangadesh? -------3', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:04', '2021-04-05 16:54:04'),
(105, 25, 'What is the capital op bangadesh? -------4', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:08', '2021-04-05 16:54:08'),
(106, 25, 'What is the capital op bangadesh? -------5', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:13', '2021-04-05 16:54:13'),
(107, 25, 'What is the capital op bangadesh? -------6', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:18', '2021-04-05 16:54:18'),
(108, 25, 'What is the capital op bangadesh? -------7', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:22', '2021-04-05 16:54:22'),
(109, 25, 'What is the capital op bangadesh? -------8', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:27', '2021-04-05 16:54:27'),
(110, 25, 'What is the capital op bangadesh? -------9', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:32', '2021-04-05 16:54:32'),
(111, 25, 'What is the capital op bangadesh? -------10', 'Dhaka', 2, 'Bangla', '2021-04-05 16:54:37', '2021-04-05 16:54:37'),
(112, 6, 'Test Question ------------------ ?', 'rasel', 4, NULL, '2021-04-16 16:41:05', '2021-04-16 16:41:05'),
(113, 6, 'Test Question ------------------ ?', 'rasel', 4, NULL, '2021-04-16 16:41:19', '2021-04-16 16:41:19'),
(114, 6, 'Test QQQ ------ ??', 'hello', 4, NULL, '2021-04-16 16:44:34', '2021-04-16 16:44:34'),
(115, 6, 'Test QQQ ------ ??', 'hello', 4, NULL, '2021-04-16 16:45:02', '2021-04-16 16:45:02'),
(116, 6, 'Test QQQ ------ ??', 'hello', 4, NULL, '2021-04-16 16:45:03', '2021-04-16 16:45:03'),
(117, 6, 'Test QQQ ------ ??', 'hello', 4, NULL, '2021-04-16 16:45:05', '2021-04-16 16:45:05'),
(118, 6, 'Test QQQ ------ ??', 'hello', 4, NULL, '2021-04-16 16:45:06', '2021-04-16 16:45:06'),
(119, 6, 'Test QQQ ------ ??', 'hello', 4, NULL, '2021-04-16 16:45:07', '2021-04-16 16:45:07'),
(121, 13, 'What is the capital of Bangladesh?', 'Dhaka', 3, NULL, '2021-04-17 19:25:12', '2021-04-17 19:25:12'),
(122, 24, 'Test --- 1', 'Rasel', 1, 'English', '2021-04-17 20:41:05', '2021-04-17 20:41:05'),
(123, 24, 'Test --- 2', 'DDD', 1, 'English', '2021-04-17 20:42:22', '2021-04-17 20:42:22'),
(124, 24, 'Test ----5', 'ty', 1, 'English', '2021-04-17 20:49:09', '2021-04-17 20:49:09'),
(125, 28, 'hello', 'c', 2, 'English', '2021-04-17 20:50:06', '2021-04-17 20:50:06'),
(126, 28, 'Langua -----5', 'rrr', 2, 'English', '2021-04-17 20:50:22', '2021-04-17 20:50:41'),
(127, 15, 'Bookkkk', 'dfdf', 3, NULL, '2021-04-17 20:53:09', '2021-04-17 20:53:29'),
(128, 15, 'What is the capital of Bangladesh?', 'Dhaka', 3, NULL, '2021-04-17 20:53:51', '2021-04-17 20:53:51'),
(129, 5, 'What is the capital of Bangladesh?', 'Dhaka', 4, NULL, '2021-04-17 20:55:53', '2021-04-17 20:55:53'),
(130, 5, 'Test 000', 'rrr', 4, NULL, '2021-04-17 20:56:07', '2021-04-17 20:56:07');

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

CREATE TABLE `question_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question_options`
--

INSERT INTO `question_options` (`id`, `question_id`, `option`, `created_at`, `updated_at`) VALUES
(81, 17, 'try', '2021-03-29 17:00:19', '2021-03-29 17:00:19'),
(82, 17, 'tries', '2021-03-29 17:00:19', '2021-03-29 17:00:19'),
(83, 17, 'tried', '2021-03-29 17:00:19', '2021-03-29 17:00:19'),
(84, 17, 'is trying', '2021-03-29 17:00:19', '2021-03-29 17:00:19'),
(85, 18, 'try', '2021-03-29 17:00:27', '2021-03-29 17:00:27'),
(86, 18, 'tries', '2021-03-29 17:00:27', '2021-03-29 17:00:27'),
(87, 18, 'tried', '2021-03-29 17:00:27', '2021-03-29 17:00:27'),
(88, 18, 'is trying', '2021-03-29 17:00:27', '2021-03-29 17:00:27'),
(89, 19, 'try', '2021-03-29 17:00:32', '2021-03-29 17:00:32'),
(90, 19, 'tries', '2021-03-29 17:00:32', '2021-03-29 17:00:32'),
(91, 19, 'tried', '2021-03-29 17:00:32', '2021-03-29 17:00:32'),
(92, 19, 'is trying', '2021-03-29 17:00:32', '2021-03-29 17:00:32'),
(93, 20, 'try', '2021-03-29 17:00:37', '2021-03-29 17:00:37'),
(94, 20, 'tries', '2021-03-29 17:00:37', '2021-03-29 17:00:37'),
(95, 20, 'tried', '2021-03-29 17:00:37', '2021-03-29 17:00:37'),
(96, 20, 'is trying', '2021-03-29 17:00:37', '2021-03-29 17:00:37'),
(97, 21, 'try', '2021-03-29 17:00:42', '2021-03-29 17:00:42'),
(98, 21, 'tries', '2021-03-29 17:00:42', '2021-03-29 17:00:42'),
(99, 21, 'tried', '2021-03-29 17:00:42', '2021-03-29 17:00:42'),
(100, 21, 'is trying', '2021-03-29 17:00:42', '2021-03-29 17:00:42'),
(101, 22, 'try', '2021-03-29 17:00:48', '2021-03-29 17:00:48'),
(102, 22, 'tries', '2021-03-29 17:00:48', '2021-03-29 17:00:48'),
(103, 22, 'tried', '2021-03-29 17:00:48', '2021-03-29 17:00:48'),
(104, 22, 'is trying', '2021-03-29 17:00:48', '2021-03-29 17:00:48'),
(105, 23, 'try', '2021-03-29 17:00:53', '2021-03-29 17:00:53'),
(106, 23, 'tries', '2021-03-29 17:00:53', '2021-03-29 17:00:53'),
(107, 23, 'tried', '2021-03-29 17:00:53', '2021-03-29 17:00:53'),
(108, 23, 'is trying', '2021-03-29 17:00:53', '2021-03-29 17:00:53'),
(109, 24, 'try', '2021-03-29 17:00:59', '2021-03-29 17:00:59'),
(110, 24, 'tries', '2021-03-29 17:00:59', '2021-03-29 17:00:59'),
(111, 24, 'tried', '2021-03-29 17:00:59', '2021-03-29 17:00:59'),
(112, 24, 'is trying', '2021-03-29 17:01:00', '2021-03-29 17:01:00'),
(113, 25, 'try', '2021-03-29 17:01:06', '2021-03-29 17:01:06'),
(114, 25, 'tries', '2021-03-29 17:01:06', '2021-03-29 17:01:06'),
(115, 25, 'tried', '2021-03-29 17:01:06', '2021-03-29 17:01:06'),
(116, 25, 'is trying', '2021-03-29 17:01:06', '2021-03-29 17:01:06'),
(117, 26, 'try', '2021-03-29 17:01:11', '2021-03-29 17:01:11'),
(118, 26, 'tries', '2021-03-29 17:01:11', '2021-03-29 17:01:11'),
(119, 26, 'tried', '2021-03-29 17:01:11', '2021-03-29 17:01:11'),
(120, 26, 'is trying', '2021-03-29 17:01:11', '2021-03-29 17:01:11'),
(121, 27, 'try', '2021-03-29 17:01:17', '2021-03-29 17:01:17'),
(122, 27, 'tries', '2021-03-29 17:01:17', '2021-03-29 17:01:17'),
(123, 27, 'tried', '2021-03-29 17:01:17', '2021-03-29 17:01:17'),
(124, 27, 'is trying', '2021-03-29 17:01:17', '2021-03-29 17:01:17'),
(125, 28, 'try', '2021-03-29 17:01:22', '2021-03-29 17:01:22'),
(126, 28, 'tries', '2021-03-29 17:01:22', '2021-03-29 17:01:22'),
(127, 28, 'tried', '2021-03-29 17:01:22', '2021-03-29 17:01:22'),
(128, 28, 'is trying', '2021-03-29 17:01:22', '2021-03-29 17:01:22'),
(129, 29, 'try', '2021-03-29 17:01:26', '2021-03-29 17:01:26'),
(130, 29, 'tries', '2021-03-29 17:01:26', '2021-03-29 17:01:26'),
(131, 29, 'tried', '2021-03-29 17:01:26', '2021-03-29 17:01:26'),
(132, 29, 'is trying', '2021-03-29 17:01:26', '2021-03-29 17:01:26'),
(133, 30, 'try', '2021-03-29 17:01:31', '2021-03-29 17:01:31'),
(134, 30, 'tries', '2021-03-29 17:01:31', '2021-03-29 17:01:31'),
(135, 30, 'tried', '2021-03-29 17:01:31', '2021-03-29 17:01:31'),
(136, 30, 'is trying', '2021-03-29 17:01:31', '2021-03-29 17:01:31'),
(137, 31, 'try', '2021-03-29 17:01:38', '2021-03-29 17:01:38'),
(138, 31, 'tries', '2021-03-29 17:01:38', '2021-03-29 17:01:38'),
(139, 31, 'tried', '2021-03-29 17:01:38', '2021-03-29 17:01:38'),
(140, 31, 'is trying', '2021-03-29 17:01:38', '2021-03-29 17:01:38'),
(141, 32, 'try', '2021-03-29 17:01:43', '2021-03-29 17:01:43'),
(142, 32, 'tries', '2021-03-29 17:01:43', '2021-03-29 17:01:43'),
(143, 32, 'tried', '2021-03-29 17:01:43', '2021-03-29 17:01:43'),
(144, 32, 'is trying', '2021-03-29 17:01:43', '2021-03-29 17:01:43'),
(145, 33, 'try', '2021-03-29 17:01:48', '2021-03-29 17:01:48'),
(146, 33, 'tries', '2021-03-29 17:01:48', '2021-03-29 17:01:48'),
(147, 33, 'tried', '2021-03-29 17:01:48', '2021-03-29 17:01:48'),
(148, 33, 'is trying', '2021-03-29 17:01:48', '2021-03-29 17:01:48'),
(149, 34, 'try', '2021-03-29 17:01:53', '2021-03-29 17:01:53'),
(150, 34, 'tries', '2021-03-29 17:01:53', '2021-03-29 17:01:53'),
(151, 34, 'tried', '2021-03-29 17:01:53', '2021-03-29 17:01:53'),
(152, 34, 'is trying', '2021-03-29 17:01:53', '2021-03-29 17:01:53'),
(153, 35, 'try', '2021-03-29 17:01:58', '2021-03-29 17:01:58'),
(154, 35, 'tries', '2021-03-29 17:01:58', '2021-03-29 17:01:58'),
(155, 35, 'tried', '2021-03-29 17:01:58', '2021-03-29 17:01:58'),
(156, 35, 'is trying', '2021-03-29 17:01:58', '2021-03-29 17:01:58'),
(157, 36, 'try', '2021-03-29 17:02:03', '2021-03-29 17:02:03'),
(158, 36, 'tries', '2021-03-29 17:02:03', '2021-03-29 17:02:03'),
(159, 36, 'tried', '2021-03-29 17:02:03', '2021-03-29 17:02:03'),
(160, 36, 'is trying', '2021-03-29 17:02:03', '2021-03-29 17:02:03'),
(161, 37, 'try', '2021-03-29 17:02:07', '2021-03-29 17:02:07'),
(162, 37, 'tries', '2021-03-29 17:02:07', '2021-03-29 17:02:07'),
(163, 37, 'tried', '2021-03-29 17:02:07', '2021-03-29 17:02:07'),
(164, 37, 'is trying', '2021-03-29 17:02:07', '2021-03-29 17:02:07'),
(165, 38, 'try', '2021-03-29 17:02:12', '2021-03-29 17:02:12'),
(166, 38, 'tries', '2021-03-29 17:02:12', '2021-03-29 17:02:12'),
(167, 38, 'tried', '2021-03-29 17:02:12', '2021-03-29 17:02:12'),
(168, 38, 'is trying', '2021-03-29 17:02:12', '2021-03-29 17:02:12'),
(169, 39, 'try', '2021-03-29 17:02:16', '2021-03-29 17:02:16'),
(170, 39, 'tries', '2021-03-29 17:02:16', '2021-03-29 17:02:16'),
(171, 39, 'tried', '2021-03-29 17:02:16', '2021-03-29 17:02:16'),
(172, 39, 'is trying', '2021-03-29 17:02:16', '2021-03-29 17:02:16'),
(173, 40, 'try', '2021-03-29 17:02:21', '2021-03-29 17:02:21'),
(174, 40, 'tries', '2021-03-29 17:02:21', '2021-03-29 17:02:21'),
(175, 40, 'tried', '2021-03-29 17:02:21', '2021-03-29 17:02:21'),
(176, 40, 'is trying', '2021-03-29 17:02:21', '2021-03-29 17:02:21'),
(177, 41, 'try', '2021-03-29 17:02:25', '2021-03-29 17:02:25'),
(178, 41, 'tries', '2021-03-29 17:02:25', '2021-03-29 17:02:25'),
(179, 41, 'tried', '2021-03-29 17:02:25', '2021-03-29 17:02:25'),
(180, 41, 'is trying', '2021-03-29 17:02:25', '2021-03-29 17:02:25'),
(181, 42, 'try', '2021-03-29 17:02:31', '2021-03-29 17:02:31'),
(182, 42, 'tries', '2021-03-29 17:02:31', '2021-03-29 17:02:31'),
(183, 42, 'tried', '2021-03-29 17:02:31', '2021-03-29 17:02:31'),
(184, 42, 'is trying', '2021-03-29 17:02:31', '2021-03-29 17:02:31'),
(185, 43, 'try', '2021-03-29 17:02:36', '2021-03-29 17:02:36'),
(186, 43, 'tries', '2021-03-29 17:02:36', '2021-03-29 17:02:36'),
(187, 43, 'tried', '2021-03-29 17:02:36', '2021-03-29 17:02:36'),
(188, 43, 'is trying', '2021-03-29 17:02:36', '2021-03-29 17:02:36'),
(189, 44, 'try', '2021-03-29 17:02:42', '2021-03-29 17:02:42'),
(190, 44, 'tries', '2021-03-29 17:02:42', '2021-03-29 17:02:42'),
(191, 44, 'tried', '2021-03-29 17:02:42', '2021-03-29 17:02:42'),
(192, 44, 'is trying', '2021-03-29 17:02:42', '2021-03-29 17:02:42'),
(193, 45, 'try', '2021-03-29 17:10:40', '2021-03-29 17:10:40'),
(194, 45, 'tries', '2021-03-29 17:10:40', '2021-03-29 17:10:40'),
(195, 45, 'tried', '2021-03-29 17:10:40', '2021-03-29 17:10:40'),
(196, 45, 'is trying', '2021-03-29 17:10:40', '2021-03-29 17:10:40'),
(197, 46, 'try', '2021-03-29 17:10:44', '2021-03-29 17:10:44'),
(198, 46, 'tries', '2021-03-29 17:10:44', '2021-03-29 17:10:44'),
(199, 46, 'tried', '2021-03-29 17:10:44', '2021-03-29 17:10:44'),
(200, 46, 'is trying', '2021-03-29 17:10:44', '2021-03-29 17:10:44'),
(201, 47, 'try', '2021-03-29 17:10:49', '2021-03-29 17:10:49'),
(202, 47, 'tries', '2021-03-29 17:10:49', '2021-03-29 17:10:49'),
(203, 47, 'tried', '2021-03-29 17:10:49', '2021-03-29 17:10:49'),
(204, 47, 'is trying', '2021-03-29 17:10:49', '2021-03-29 17:10:49'),
(205, 48, 'try', '2021-03-29 17:10:54', '2021-03-29 17:10:54'),
(206, 48, 'tries', '2021-03-29 17:10:54', '2021-03-29 17:10:54'),
(207, 48, 'tried', '2021-03-29 17:10:54', '2021-03-29 17:10:54'),
(208, 48, 'is trying', '2021-03-29 17:10:54', '2021-03-29 17:10:54'),
(209, 49, 'try', '2021-03-29 17:10:58', '2021-03-29 17:10:58'),
(210, 49, 'tries', '2021-03-29 17:10:58', '2021-03-29 17:10:58'),
(211, 49, 'tried', '2021-03-29 17:10:58', '2021-03-29 17:10:58'),
(212, 49, 'is trying', '2021-03-29 17:10:58', '2021-03-29 17:10:58'),
(213, 50, 'try', '2021-03-29 17:11:04', '2021-03-29 17:11:04'),
(214, 50, 'tries', '2021-03-29 17:11:04', '2021-03-29 17:11:04'),
(215, 50, 'tried', '2021-03-29 17:11:04', '2021-03-29 17:11:04'),
(216, 50, 'is trying', '2021-03-29 17:11:04', '2021-03-29 17:11:04'),
(217, 51, 'try', '2021-03-29 17:11:08', '2021-03-29 17:11:08'),
(218, 51, 'tries', '2021-03-29 17:11:08', '2021-03-29 17:11:08'),
(219, 51, 'tried', '2021-03-29 17:11:08', '2021-03-29 17:11:08'),
(220, 51, 'is trying', '2021-03-29 17:11:08', '2021-03-29 17:11:08'),
(221, 52, 'try', '2021-03-29 17:11:12', '2021-03-29 17:11:12'),
(222, 52, 'tries', '2021-03-29 17:11:12', '2021-03-29 17:11:12'),
(223, 52, 'tried', '2021-03-29 17:11:12', '2021-03-29 17:11:12'),
(224, 52, 'is trying', '2021-03-29 17:11:12', '2021-03-29 17:11:12'),
(225, 53, 'try', '2021-03-29 17:11:17', '2021-03-29 17:11:17'),
(226, 53, 'tries', '2021-03-29 17:11:17', '2021-03-29 17:11:17'),
(227, 53, 'tried', '2021-03-29 17:11:17', '2021-03-29 17:11:17'),
(228, 53, 'is trying', '2021-03-29 17:11:17', '2021-03-29 17:11:17'),
(229, 54, 'try', '2021-03-29 17:11:25', '2021-03-29 17:11:25'),
(230, 54, 'tries', '2021-03-29 17:11:25', '2021-03-29 17:11:25'),
(231, 54, 'tried', '2021-03-29 17:11:25', '2021-03-29 17:11:25'),
(232, 54, 'is trying', '2021-03-29 17:11:25', '2021-03-29 17:11:25'),
(233, 55, 'Rasel', '2021-04-02 16:18:01', '2021-04-02 16:18:01'),
(234, 55, 'Hasan', '2021-04-02 16:18:01', '2021-04-02 16:18:01'),
(235, 55, 'Kamal', '2021-04-02 16:18:01', '2021-04-02 16:18:01'),
(236, 55, 'Jamal', '2021-04-02 16:18:01', '2021-04-02 16:18:01'),
(237, 56, 'Rasel', '2021-04-02 16:18:06', '2021-04-02 16:18:06'),
(238, 56, 'Hasan', '2021-04-02 16:18:06', '2021-04-02 16:18:06'),
(239, 56, 'Kamal', '2021-04-02 16:18:06', '2021-04-02 16:18:06'),
(240, 56, 'Jamal', '2021-04-02 16:18:06', '2021-04-02 16:18:06'),
(241, 57, 'Rasel', '2021-04-02 16:18:11', '2021-04-02 16:18:11'),
(242, 57, 'Hasan', '2021-04-02 16:18:11', '2021-04-02 16:18:11'),
(243, 57, 'Kamal', '2021-04-02 16:18:11', '2021-04-02 16:18:11'),
(244, 57, 'Jamal', '2021-04-02 16:18:11', '2021-04-02 16:18:11'),
(245, 58, 'Rasel', '2021-04-02 16:18:16', '2021-04-02 16:18:16'),
(246, 58, 'Hasan', '2021-04-02 16:18:16', '2021-04-02 16:18:16'),
(247, 58, 'Kamal', '2021-04-02 16:18:16', '2021-04-02 16:18:16'),
(248, 58, 'Jamal', '2021-04-02 16:18:16', '2021-04-02 16:18:16'),
(249, 59, 'Rasel', '2021-04-02 16:18:21', '2021-04-02 16:18:21'),
(250, 59, 'Hasan', '2021-04-02 16:18:21', '2021-04-02 16:18:21'),
(251, 59, 'Kamal', '2021-04-02 16:18:21', '2021-04-02 16:18:21'),
(252, 59, 'Jamal', '2021-04-02 16:18:21', '2021-04-02 16:18:21'),
(253, 60, 'Rasel', '2021-04-02 16:18:26', '2021-04-02 16:18:26'),
(254, 60, 'Hasan', '2021-04-02 16:18:26', '2021-04-02 16:18:26'),
(255, 60, 'Kamal', '2021-04-02 16:18:26', '2021-04-02 16:18:26'),
(256, 60, 'Jamal', '2021-04-02 16:18:26', '2021-04-02 16:18:26'),
(257, 61, 'Rasel', '2021-04-02 16:18:31', '2021-04-02 16:18:31'),
(258, 61, 'Hasan', '2021-04-02 16:18:31', '2021-04-02 16:18:31'),
(259, 61, 'Kamal', '2021-04-02 16:18:31', '2021-04-02 16:18:31'),
(260, 61, 'Jamal', '2021-04-02 16:18:31', '2021-04-02 16:18:31'),
(261, 62, 'Rasel', '2021-04-02 16:18:36', '2021-04-02 16:18:36'),
(262, 62, 'Hasan', '2021-04-02 16:18:36', '2021-04-02 16:18:36'),
(263, 62, 'Kamal', '2021-04-02 16:18:36', '2021-04-02 16:18:36'),
(264, 62, 'Jamal', '2021-04-02 16:18:36', '2021-04-02 16:18:36'),
(265, 63, 'Rasel', '2021-04-02 16:18:42', '2021-04-02 16:18:42'),
(266, 63, 'Hasan', '2021-04-02 16:18:42', '2021-04-02 16:18:42'),
(267, 63, 'Kamal', '2021-04-02 16:18:42', '2021-04-02 16:18:42'),
(268, 63, 'Jamal', '2021-04-02 16:18:42', '2021-04-02 16:18:42'),
(269, 64, 'Rasel', '2021-04-02 16:18:46', '2021-04-02 16:18:46'),
(270, 64, 'Hasan', '2021-04-02 16:18:46', '2021-04-02 16:18:46'),
(271, 64, 'Kamal', '2021-04-02 16:18:46', '2021-04-02 16:18:46'),
(272, 64, 'Jamal', '2021-04-02 16:18:46', '2021-04-02 16:18:46'),
(273, 65, 'abccccc', '2021-04-02 17:07:16', '2021-04-02 17:07:16'),
(274, 65, 'avbbbbbb', '2021-04-02 17:07:16', '2021-04-02 17:07:16'),
(275, 65, 'hhhasfsdf', '2021-04-02 17:07:16', '2021-04-02 17:07:16'),
(276, 65, 'tryertertert', '2021-04-02 17:07:16', '2021-04-02 17:07:16'),
(277, 66, 'abccccc', '2021-04-02 17:07:19', '2021-04-02 17:07:19'),
(278, 66, 'avbbbbbb', '2021-04-02 17:07:19', '2021-04-02 17:07:19'),
(279, 66, 'hhhasfsdf', '2021-04-02 17:07:19', '2021-04-02 17:07:19'),
(280, 66, 'tryertertert', '2021-04-02 17:07:19', '2021-04-02 17:07:19'),
(281, 67, 'abccccc', '2021-04-02 17:07:25', '2021-04-02 17:07:25'),
(282, 67, 'avbbbbbb', '2021-04-02 17:07:25', '2021-04-02 17:07:25'),
(283, 67, 'hhhasfsdf', '2021-04-02 17:07:25', '2021-04-02 17:07:25'),
(284, 67, 'tryertertert', '2021-04-02 17:07:25', '2021-04-02 17:07:25'),
(285, 68, 'abccccc', '2021-04-02 17:07:29', '2021-04-02 17:07:29'),
(286, 68, 'avbbbbbb', '2021-04-02 17:07:29', '2021-04-02 17:07:29'),
(287, 68, 'hhhasfsdf', '2021-04-02 17:07:29', '2021-04-02 17:07:29'),
(288, 68, 'tryertertert', '2021-04-02 17:07:29', '2021-04-02 17:07:29'),
(289, 69, 'abccccc', '2021-04-02 17:07:34', '2021-04-02 17:07:34'),
(290, 69, 'avbbbbbb', '2021-04-02 17:07:34', '2021-04-02 17:07:34'),
(291, 69, 'hhhasfsdf', '2021-04-02 17:07:34', '2021-04-02 17:07:34'),
(292, 69, 'tryertertert', '2021-04-02 17:07:34', '2021-04-02 17:07:34'),
(293, 70, 'abccccc', '2021-04-02 17:07:38', '2021-04-02 17:07:38'),
(294, 70, 'avbbbbbb', '2021-04-02 17:07:38', '2021-04-02 17:07:38'),
(295, 70, 'hhhasfsdf', '2021-04-02 17:07:38', '2021-04-02 17:07:38'),
(296, 70, 'tryertertert', '2021-04-02 17:07:38', '2021-04-02 17:07:38'),
(297, 71, 'abccccc', '2021-04-02 17:07:42', '2021-04-02 17:07:42'),
(298, 71, 'avbbbbbb', '2021-04-02 17:07:42', '2021-04-02 17:07:42'),
(299, 71, 'hhhasfsdf', '2021-04-02 17:07:42', '2021-04-02 17:07:42'),
(300, 71, 'tryertertert', '2021-04-02 17:07:42', '2021-04-02 17:07:42'),
(301, 72, 'abccccc', '2021-04-02 17:07:47', '2021-04-02 17:07:47'),
(302, 72, 'avbbbbbb', '2021-04-02 17:07:47', '2021-04-02 17:07:47'),
(303, 72, 'hhhasfsdf', '2021-04-02 17:07:47', '2021-04-02 17:07:47'),
(304, 72, 'tryertertert', '2021-04-02 17:07:47', '2021-04-02 17:07:47'),
(305, 73, 'abccccc', '2021-04-02 17:07:51', '2021-04-02 17:07:51'),
(306, 73, 'avbbbbbb', '2021-04-02 17:07:51', '2021-04-02 17:07:51'),
(307, 73, 'hhhasfsdf', '2021-04-02 17:07:51', '2021-04-02 17:07:51'),
(308, 73, 'tryertertert', '2021-04-02 17:07:51', '2021-04-02 17:07:51'),
(309, 74, 'abccccc', '2021-04-02 17:07:56', '2021-04-02 17:07:56'),
(310, 74, 'avbbbbbb', '2021-04-02 17:07:56', '2021-04-02 17:07:56'),
(311, 74, 'hhhasfsdf', '2021-04-02 17:07:56', '2021-04-02 17:07:56'),
(312, 74, 'tryertertert', '2021-04-02 17:07:56', '2021-04-02 17:07:56'),
(313, 75, 'abccccc', '2021-04-02 17:08:07', '2021-04-02 17:08:07'),
(314, 75, 'avbbbbbb', '2021-04-02 17:08:07', '2021-04-02 17:08:07'),
(315, 75, 'hhhasfsdf', '2021-04-02 17:08:07', '2021-04-02 17:08:07'),
(316, 75, 'tryertertert', '2021-04-02 17:08:07', '2021-04-02 17:08:07'),
(317, 76, 'abccccc', '2021-04-02 17:08:13', '2021-04-02 17:08:13'),
(318, 76, 'avbbbbbb', '2021-04-02 17:08:13', '2021-04-02 17:08:13'),
(319, 76, 'hhhasfsdf', '2021-04-02 17:08:13', '2021-04-02 17:08:13'),
(320, 76, 'tryertertert', '2021-04-02 17:08:13', '2021-04-02 17:08:13'),
(321, 77, 'abccccc', '2021-04-02 17:08:17', '2021-04-02 17:08:17'),
(322, 77, 'avbbbbbb', '2021-04-02 17:08:17', '2021-04-02 17:08:17'),
(323, 77, 'hhhasfsdf', '2021-04-02 17:08:17', '2021-04-02 17:08:17'),
(324, 77, 'tryertertert', '2021-04-02 17:08:17', '2021-04-02 17:08:17'),
(325, 78, 'abccccc', '2021-04-02 17:08:27', '2021-04-02 17:08:27'),
(326, 78, 'avbbbbbb', '2021-04-02 17:08:27', '2021-04-02 17:08:27'),
(327, 78, 'hhhasfsdf', '2021-04-02 17:08:27', '2021-04-02 17:08:27'),
(328, 78, 'tryertertert', '2021-04-02 17:08:27', '2021-04-02 17:08:27'),
(329, 79, 'abccccc', '2021-04-02 17:08:36', '2021-04-02 17:08:36'),
(330, 79, 'avbbbbbb', '2021-04-02 17:08:36', '2021-04-02 17:08:36'),
(331, 79, 'hhhasfsdf', '2021-04-02 17:08:36', '2021-04-02 17:08:36'),
(332, 79, 'tryertertert', '2021-04-02 17:08:36', '2021-04-02 17:08:36'),
(333, 80, 'abccccc', '2021-04-02 17:08:42', '2021-04-02 17:08:42'),
(334, 80, 'avbbbbbb', '2021-04-02 17:08:42', '2021-04-02 17:08:42'),
(335, 80, 'hhhasfsdf', '2021-04-02 17:08:42', '2021-04-02 17:08:42'),
(336, 80, 'tryertertert', '2021-04-02 17:08:42', '2021-04-02 17:08:42'),
(337, 81, 'abccccc', '2021-04-02 17:08:46', '2021-04-02 17:08:46'),
(338, 81, 'avbbbbbb', '2021-04-02 17:08:46', '2021-04-02 17:08:46'),
(339, 81, 'hhhasfsdf', '2021-04-02 17:08:46', '2021-04-02 17:08:46'),
(340, 81, 'tryertertert', '2021-04-02 17:08:46', '2021-04-02 17:08:46'),
(341, 82, 'abccccc', '2021-04-02 17:08:51', '2021-04-02 17:08:51'),
(342, 82, 'avbbbbbb', '2021-04-02 17:08:51', '2021-04-02 17:08:51'),
(343, 82, 'hhhasfsdf', '2021-04-02 17:08:51', '2021-04-02 17:08:51'),
(344, 82, 'tryertertert', '2021-04-02 17:08:51', '2021-04-02 17:08:51'),
(349, 84, 'vvv', '2021-04-02 17:53:40', '2021-04-02 17:53:40'),
(350, 84, 'cccc', '2021-04-02 17:53:40', '2021-04-02 17:53:40'),
(351, 84, 'nnnn', '2021-04-02 17:53:40', '2021-04-02 17:53:40'),
(352, 84, 'yyyy', '2021-04-02 17:53:40', '2021-04-02 17:53:40'),
(353, 85, 'vvv', '2021-04-02 17:53:45', '2021-04-02 17:53:45'),
(354, 85, 'cccc', '2021-04-02 17:53:45', '2021-04-02 17:53:45'),
(355, 85, 'nnnn', '2021-04-02 17:53:45', '2021-04-02 17:53:45'),
(356, 85, 'yyyy', '2021-04-02 17:53:45', '2021-04-02 17:53:45'),
(357, 86, 'vvv', '2021-04-02 17:53:50', '2021-04-02 17:53:50'),
(358, 86, 'cccc', '2021-04-02 17:53:50', '2021-04-02 17:53:50'),
(359, 86, 'nnnn', '2021-04-02 17:53:50', '2021-04-02 17:53:50'),
(360, 86, 'yyyy', '2021-04-02 17:53:50', '2021-04-02 17:53:50'),
(361, 87, 'vvv', '2021-04-02 17:53:55', '2021-04-02 17:53:55'),
(362, 87, 'cccc', '2021-04-02 17:53:55', '2021-04-02 17:53:55'),
(363, 87, 'nnnn', '2021-04-02 17:53:55', '2021-04-02 17:53:55'),
(364, 87, 'yyyy', '2021-04-02 17:53:55', '2021-04-02 17:53:55'),
(365, 88, 'vvv', '2021-04-02 17:54:00', '2021-04-02 17:54:00'),
(366, 88, 'cccc', '2021-04-02 17:54:00', '2021-04-02 17:54:00'),
(367, 88, 'nnnn', '2021-04-02 17:54:00', '2021-04-02 17:54:00'),
(368, 88, 'yyyy', '2021-04-02 17:54:00', '2021-04-02 17:54:00'),
(369, 89, 'vvv', '2021-04-02 17:54:04', '2021-04-02 17:54:04'),
(370, 89, 'cccc', '2021-04-02 17:54:04', '2021-04-02 17:54:04'),
(371, 89, 'nnnn', '2021-04-02 17:54:04', '2021-04-02 17:54:04'),
(372, 89, 'yyyy', '2021-04-02 17:54:04', '2021-04-02 17:54:04'),
(373, 90, 'vvv', '2021-04-02 17:54:11', '2021-04-02 17:54:11'),
(374, 90, 'cccc', '2021-04-02 17:54:11', '2021-04-02 17:54:11'),
(375, 90, 'nnnn', '2021-04-02 17:54:11', '2021-04-02 17:54:11'),
(376, 90, 'yyyy', '2021-04-02 17:54:11', '2021-04-02 17:54:11'),
(377, 91, 'vvv', '2021-04-02 17:54:15', '2021-04-02 17:54:15'),
(378, 91, 'cccc', '2021-04-02 17:54:15', '2021-04-02 17:54:15'),
(379, 91, 'nnnn', '2021-04-02 17:54:15', '2021-04-02 17:54:15'),
(380, 91, 'yyyy', '2021-04-02 17:54:15', '2021-04-02 17:54:15'),
(381, 92, 'vvv', '2021-04-02 17:54:20', '2021-04-02 17:54:20'),
(382, 92, 'cccc', '2021-04-02 17:54:20', '2021-04-02 17:54:20'),
(383, 92, 'nnnn', '2021-04-02 17:54:20', '2021-04-02 17:54:20'),
(384, 92, 'yyyy', '2021-04-02 17:54:20', '2021-04-02 17:54:20'),
(385, 93, 'Dhaka', '2021-04-04 15:40:18', '2021-04-04 15:40:18'),
(386, 93, 'India', '2021-04-04 15:40:18', '2021-04-04 15:40:18'),
(387, 93, 'Pakistan', '2021-04-04 15:40:18', '2021-04-04 15:40:18'),
(388, 93, 'Bangladesh', '2021-04-04 15:40:18', '2021-04-04 15:40:18'),
(389, 94, 'Dhaka', '2021-04-04 15:40:24', '2021-04-04 15:40:24'),
(390, 94, 'India', '2021-04-04 15:40:24', '2021-04-04 15:40:24'),
(391, 94, 'Pakistan', '2021-04-04 15:40:24', '2021-04-04 15:40:24'),
(392, 94, 'Bangladesh', '2021-04-04 15:40:24', '2021-04-04 15:40:24'),
(393, 95, 'Dhaka', '2021-04-04 15:40:29', '2021-04-04 15:40:29'),
(394, 95, 'India', '2021-04-04 15:40:29', '2021-04-04 15:40:29'),
(395, 95, 'Pakistan', '2021-04-04 15:40:29', '2021-04-04 15:40:29'),
(396, 95, 'Bangladesh', '2021-04-04 15:40:29', '2021-04-04 15:40:29'),
(397, 96, 'Dhaka', '2021-04-04 15:40:33', '2021-04-04 15:40:33'),
(398, 96, 'India', '2021-04-04 15:40:33', '2021-04-04 15:40:33'),
(399, 96, 'Pakistan', '2021-04-04 15:40:33', '2021-04-04 15:40:33'),
(400, 96, 'Bangladesh', '2021-04-04 15:40:33', '2021-04-04 15:40:33'),
(401, 97, 'Dhaka', '2021-04-04 15:40:43', '2021-04-04 15:40:43'),
(402, 97, 'India', '2021-04-04 15:40:43', '2021-04-04 15:40:43'),
(403, 97, 'Pakistan', '2021-04-04 15:40:43', '2021-04-04 15:40:43'),
(404, 97, 'Bangladesh', '2021-04-04 15:40:43', '2021-04-04 15:40:43'),
(405, 98, 'Dhaka', '2021-04-04 15:40:48', '2021-04-04 15:40:48'),
(406, 98, 'India', '2021-04-04 15:40:48', '2021-04-04 15:40:48'),
(407, 98, 'Pakistan', '2021-04-04 15:40:48', '2021-04-04 15:40:48'),
(408, 98, 'Bangladesh', '2021-04-04 15:40:48', '2021-04-04 15:40:48'),
(409, 99, 'Dhaka', '2021-04-04 15:40:53', '2021-04-04 15:40:53'),
(410, 99, 'India', '2021-04-04 15:40:53', '2021-04-04 15:40:53'),
(411, 99, 'Pakistan', '2021-04-04 15:40:53', '2021-04-04 15:40:53'),
(412, 99, 'Bangladesh', '2021-04-04 15:40:53', '2021-04-04 15:40:53'),
(413, 100, 'Dhaka', '2021-04-04 15:40:58', '2021-04-04 15:40:58'),
(414, 100, 'India', '2021-04-04 15:40:58', '2021-04-04 15:40:58'),
(415, 100, 'Pakistan', '2021-04-04 15:40:58', '2021-04-04 15:40:58'),
(416, 100, 'Bangladesh', '2021-04-04 15:40:58', '2021-04-04 15:40:58'),
(417, 101, 'Dhaka', '2021-04-04 15:41:02', '2021-04-04 15:41:02'),
(418, 101, 'India', '2021-04-04 15:41:02', '2021-04-04 15:41:02'),
(419, 101, 'Pakistan', '2021-04-04 15:41:02', '2021-04-04 15:41:02'),
(420, 101, 'Bangladesh', '2021-04-04 15:41:02', '2021-04-04 15:41:02'),
(421, 102, 'Dhaka', '2021-04-05 16:53:52', '2021-04-05 16:53:52'),
(422, 102, 'Jhenaidah', '2021-04-05 16:53:52', '2021-04-05 16:53:52'),
(423, 102, 'Khulna', '2021-04-05 16:53:52', '2021-04-05 16:53:52'),
(424, 102, 'Borishal', '2021-04-05 16:53:52', '2021-04-05 16:53:52'),
(425, 103, 'Dhaka', '2021-04-05 16:53:58', '2021-04-05 16:53:58'),
(426, 103, 'Jhenaidah', '2021-04-05 16:53:58', '2021-04-05 16:53:58'),
(427, 103, 'Khulna', '2021-04-05 16:53:58', '2021-04-05 16:53:58'),
(428, 103, 'Borishal', '2021-04-05 16:53:58', '2021-04-05 16:53:58'),
(429, 104, 'Dhaka', '2021-04-05 16:54:04', '2021-04-05 16:54:04'),
(430, 104, 'Jhenaidah', '2021-04-05 16:54:04', '2021-04-05 16:54:04'),
(431, 104, 'Khulna', '2021-04-05 16:54:04', '2021-04-05 16:54:04'),
(432, 104, 'Borishal', '2021-04-05 16:54:04', '2021-04-05 16:54:04'),
(433, 105, 'Dhaka', '2021-04-05 16:54:09', '2021-04-05 16:54:09'),
(434, 105, 'Jhenaidah', '2021-04-05 16:54:09', '2021-04-05 16:54:09'),
(435, 105, 'Khulna', '2021-04-05 16:54:09', '2021-04-05 16:54:09'),
(436, 105, 'Borishal', '2021-04-05 16:54:09', '2021-04-05 16:54:09'),
(437, 106, 'Dhaka', '2021-04-05 16:54:13', '2021-04-05 16:54:13'),
(438, 106, 'Jhenaidah', '2021-04-05 16:54:13', '2021-04-05 16:54:13'),
(439, 106, 'Khulna', '2021-04-05 16:54:13', '2021-04-05 16:54:13'),
(440, 106, 'Borishal', '2021-04-05 16:54:13', '2021-04-05 16:54:13'),
(441, 107, 'Dhaka', '2021-04-05 16:54:18', '2021-04-05 16:54:18'),
(442, 107, 'Jhenaidah', '2021-04-05 16:54:18', '2021-04-05 16:54:18'),
(443, 107, 'Khulna', '2021-04-05 16:54:18', '2021-04-05 16:54:18'),
(444, 107, 'Borishal', '2021-04-05 16:54:18', '2021-04-05 16:54:18'),
(445, 108, 'Dhaka', '2021-04-05 16:54:22', '2021-04-05 16:54:22'),
(446, 108, 'Jhenaidah', '2021-04-05 16:54:23', '2021-04-05 16:54:23'),
(447, 108, 'Khulna', '2021-04-05 16:54:23', '2021-04-05 16:54:23'),
(448, 108, 'Borishal', '2021-04-05 16:54:23', '2021-04-05 16:54:23'),
(449, 109, 'Dhaka', '2021-04-05 16:54:27', '2021-04-05 16:54:27'),
(450, 109, 'Jhenaidah', '2021-04-05 16:54:27', '2021-04-05 16:54:27'),
(451, 109, 'Khulna', '2021-04-05 16:54:27', '2021-04-05 16:54:27'),
(452, 109, 'Borishal', '2021-04-05 16:54:27', '2021-04-05 16:54:27'),
(453, 110, 'Dhaka', '2021-04-05 16:54:32', '2021-04-05 16:54:32'),
(454, 110, 'Jhenaidah', '2021-04-05 16:54:32', '2021-04-05 16:54:32'),
(455, 110, 'Khulna', '2021-04-05 16:54:32', '2021-04-05 16:54:32'),
(456, 110, 'Borishal', '2021-04-05 16:54:32', '2021-04-05 16:54:32'),
(457, 111, 'Dhaka', '2021-04-05 16:54:37', '2021-04-05 16:54:37'),
(458, 111, 'Jhenaidah', '2021-04-05 16:54:37', '2021-04-05 16:54:37'),
(459, 111, 'Khulna', '2021-04-05 16:54:37', '2021-04-05 16:54:37'),
(460, 111, 'Borishal', '2021-04-05 16:54:37', '2021-04-05 16:54:37'),
(461, 83, 'yyyy', '2021-04-17 17:38:45', '2021-04-17 17:38:45'),
(462, 83, 'nnnn', '2021-04-17 17:38:45', '2021-04-17 17:38:45'),
(463, 83, 'cccc', '2021-04-17 17:38:45', '2021-04-17 17:38:45'),
(467, 16, 'is trying', '2021-04-17 19:13:06', '2021-04-17 19:13:06'),
(468, 16, 'tried', '2021-04-17 19:13:07', '2021-04-17 19:13:07'),
(469, 16, 'tries', '2021-04-17 19:13:07', '2021-04-17 19:13:07'),
(495, 121, 'DHak', '2021-04-17 19:44:41', '2021-04-17 19:44:41'),
(496, 121, 'kasdflkj', '2021-04-17 19:44:41', '2021-04-17 19:44:41'),
(497, 122, 'Hasan', '2021-04-17 20:41:31', '2021-04-17 20:41:31'),
(498, 122, 'Kaal', '2021-04-17 20:41:31', '2021-04-17 20:41:31'),
(499, 123, 'DDD', '2021-04-17 20:42:22', '2021-04-17 20:42:22'),
(500, 123, 'vvvv', '2021-04-17 20:42:22', '2021-04-17 20:42:22'),
(501, 123, 'bbbb', '2021-04-17 20:42:22', '2021-04-17 20:42:22'),
(502, 123, 'cccc', '2021-04-17 20:42:22', '2021-04-17 20:42:22'),
(503, 124, 'av', '2021-04-17 20:49:09', '2021-04-17 20:49:09'),
(504, 124, 'vd', '2021-04-17 20:49:09', '2021-04-17 20:49:09'),
(505, 124, 'cv', '2021-04-17 20:49:09', '2021-04-17 20:49:09'),
(506, 124, 'ty', '2021-04-17 20:49:09', '2021-04-17 20:49:09'),
(507, 125, 'ff', '2021-04-17 20:50:06', '2021-04-17 20:50:06'),
(508, 125, 'bbb', '2021-04-17 20:50:06', '2021-04-17 20:50:06'),
(509, 125, 'v', '2021-04-17 20:50:06', '2021-04-17 20:50:06'),
(510, 125, 'c', '2021-04-17 20:50:06', '2021-04-17 20:50:06'),
(511, 126, 'rrr', '2021-04-17 20:50:41', '2021-04-17 20:50:41'),
(512, 126, 'tttt', '2021-04-17 20:50:41', '2021-04-17 20:50:41'),
(513, 127, 'dfdf', '2021-04-17 20:53:29', '2021-04-17 20:53:29'),
(514, 127, 'fgfgf', '2021-04-17 20:53:29', '2021-04-17 20:53:29'),
(515, 128, 'Dhaka', '2021-04-17 20:53:51', '2021-04-17 20:53:51'),
(516, 128, 'Rasel', '2021-04-17 20:53:51', '2021-04-17 20:53:51'),
(517, 128, 'cc', '2021-04-17 20:53:51', '2021-04-17 20:53:51'),
(518, 128, 'Programmer', '2021-04-17 20:53:51', '2021-04-17 20:53:51'),
(525, 129, 'cc', '2021-04-17 20:56:56', '2021-04-17 20:56:56'),
(526, 129, 'dd', '2021-04-17 20:56:56', '2021-04-17 20:56:56'),
(527, 129, 'Dhaka', '2021-04-17 20:56:56', '2021-04-17 20:56:56'),
(528, 130, 'rrr', '2021-04-17 20:59:15', '2021-04-17 20:59:15'),
(529, 130, 'ggg', '2021-04-17 20:59:15', '2021-04-17 20:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` datetime NOT NULL,
  `end_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `grade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person` int(11) DEFAULT NULL,
  `distribution` int(11) DEFAULT NULL,
  `active_bangla` int(11) DEFAULT NULL,
  `active_english` int(11) DEFAULT NULL,
  `mark` int(11) NOT NULL,
  `total_quistion` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `title`, `start_date`, `start_time`, `end_date`, `end_time`, `price`, `status`, `grade`, `person`, `distribution`, `active_bangla`, `active_english`, `mark`, `total_quistion`, `description`, `created_at`, `updated_at`) VALUES
(17, 'Reading Compaction 1', '2021-03-25 21:56:00', '21:56', '2021-05-23 21:56:00', '21:56', 100.00, 3, '0-5 grade', 2, 2, NULL, NULL, 10, 5, NULL, '2021-03-22 15:55:28', '2021-05-02 23:14:50'),
(18, 'Reading Compaction 2', '2021-03-25 22:55:00', '22:55', '2021-04-29 21:57:00', '21:57', 300.00, 3, '0-5 grade', 10, 2, NULL, NULL, 4, 10, NULL, '2021-03-22 15:56:16', '2021-05-02 23:15:02'),
(19, 'Reading Compaction 3', '2021-03-24 00:00:00', '21:59', '2021-03-31 00:00:00', '21:59', 200.00, 3, '9-12 grade', 20, 2, NULL, NULL, 10, 10, NULL, '2021-03-22 15:57:07', '2021-03-29 16:35:38'),
(20, 'Reading Compaction 4', '2021-03-24 12:57:00', '12:57', '2021-03-30 21:01:00', '21:01', 500.00, 3, 'above/adult', 5, 2, NULL, NULL, 20, 5, NULL, '2021-03-22 15:57:57', '2021-04-13 19:07:47'),
(21, 'General Quiz Compitision 1', '2021-03-23 21:56:00', '21:56', '2021-06-24 21:56:00', '21:56', 100.00, 1, '0-5 grade', 2, 2, 1, 1, 10, 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum', '2021-03-22 15:55:28', '2021-05-02 23:15:25'),
(22, 'General Quiz Compitision 2', '2021-03-25 00:00:00', '22:55', '2021-03-27 00:00:00', '21:57', 300.00, 1, '6-8 grade', 10, 2, NULL, NULL, 0, 0, NULL, '2021-03-22 15:56:16', '2021-03-22 15:56:16'),
(23, 'General Quiz Compitision 3', '2021-06-10 21:59:00', '21:59', '2021-06-28 21:59:00', '21:59', 200.00, 1, '9-12 grade', 20, 2, NULL, NULL, 0, 0, NULL, '2021-03-22 15:57:07', '2021-05-03 00:29:03'),
(24, 'General Quiz Compitision 4', '2021-03-24 12:57:00', '12:57', '2021-03-30 21:01:00', '21:01', 500.00, 1, 'above/adult', 5, 2, 1, 1, 5, 12, 'Hello', '2021-03-22 15:57:57', '2021-05-03 00:06:50'),
(25, 'Spelling Quiz Compitision 1', '2021-03-23 21:56:00', '21:56', '2021-06-24 21:56:00', '21:56', 100.00, 2, '0-5 grade', 2, 2, 1, 1, 10, 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum', '2021-03-22 15:55:28', '2021-05-02 23:15:50'),
(26, 'Spelling Quiz Compitision 2', '2021-03-25 00:00:00', '22:55', '2021-03-27 00:00:00', '21:57', 300.00, 2, '6-8 grade', 10, 2, NULL, NULL, 0, 0, NULL, '2021-03-22 15:56:16', '2021-03-22 15:56:16'),
(27, 'Spelling Quiz Compitision 3', '2021-03-24 00:00:00', '21:59', '2021-03-27 00:00:00', '21:59', 200.00, 2, '9-12 grade', 20, 2, NULL, NULL, 0, 0, NULL, '2021-03-22 15:57:07', '2021-03-22 15:57:07'),
(28, 'Spelling Quiz Compitision 4', '2021-03-24 12:57:00', '12:57', '2021-03-30 21:01:00', '21:01', 500.00, 2, 'above/adult', 5, 2, NULL, NULL, 0, 0, 'HGi', '2021-03-22 15:57:57', '2021-04-13 17:08:08'),
(29, 'Video Quiz Compitision 1', '2021-03-23 21:56:00', '21:56', '2021-06-10 21:56:00', '21:56', 100.00, 4, '0-5 grade', 2, 2, NULL, NULL, 10, 5, NULL, '2021-03-22 15:55:28', '2021-05-02 23:16:12'),
(30, 'Video Quiz Compitision 2', '2021-03-25 22:55:00', '22:55', '2021-04-07 21:57:00', '21:57', 300.00, 4, '6-8 grade', 10, 2, NULL, NULL, 0, 0, NULL, '2021-03-22 15:56:16', '2021-04-04 15:41:51'),
(31, 'Video Quiz Compitision 3', '2021-03-24 21:59:00', '21:59', '2021-04-30 21:59:00', '21:59', 200.00, 4, '0-5 grade', 2, 2, NULL, NULL, 10, 2, NULL, '2021-03-22 15:57:07', '2021-04-16 17:16:36'),
(32, 'Video Quiz Compitision 4', '2021-03-24 12:57:00', '12:57', '2021-04-13 21:01:00', '21:01', 500.00, 4, 'above/adult', 5, 2, NULL, NULL, 12, 45, NULL, '2021-03-22 15:57:57', '2021-05-03 00:05:42');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `privacy_policy` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `terms_and_condition` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `faq` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_us` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `banner`, `about_us`, `privacy_policy`, `terms_and_condition`, `faq`, `contact_us`, `created_at`, `updated_at`) VALUES
(1, '1617900473.png', '1617900484.jpg', '<h2>About Us</h2><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=qevjPl7d2sk\"></oembed></figure><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p>', '<p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p>', '<p>&nbsp;</p><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=66VN2ZIWPnw\"></oembed></figure><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p>', '<h2>Faq</h2><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=ilbis9tC3Dw\"></oembed></figure><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p>', '<h2>Contact Us</h2><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><figure class=\"media\"><oembed url=\"https://www.youtube.com/watch?v=ilbis9tC3Dw\"></oembed></figure><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p>', '2021-04-07 19:18:11', '2021-05-02 22:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `deg`, `desc`, `image`, `created_at`, `updated_at`) VALUES
(4, 'Rasel Hasan', 'Software Engineer', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', '1619813956.png', '2021-04-30 20:19:16', '2021-04-30 20:19:16'),
(5, 'Nadim Uddin', 'Software Developer', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p><p>&nbsp;</p><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>', '1619814041.jpeg', '2021-04-30 20:20:41', '2021-04-30 20:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation_with_guardian` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `address`, `city`, `zip`, `state`, `guardian`, `role`, `relation_with_guardian`, `image`, `date_of_birth`, `remember_token`, `created_at`, `updated_at`, `country`, `grade`, `guardian_email`, `guardian_phone`) VALUES
(1, 'Rasel Hasan', 'admin@gmail.com', NULL, '$2y$10$ubZ3gKjxs.MYiYs12jB1EetH3TXhH2XU8qSXMOlvLXJm/Ma/iyeQG', NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Rasel', 'rasel.laravel@gmail.com', NULL, '$2y$10$ubZ3gKjxs.MYiYs12jB1EetH3TXhH2XU8qSXMOlvLXJm/Ma/iyeQG', '01909888251', 'sddfsdafdsaf', 'Dhaka', '1209', 'Dhaka', 'Amzed', '2', 'Father', '1618481128.png', '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-05-02 22:47:44', 'ALBANIA', '0-5 grade', 'asdfdsaf@gmail.com', '34543535'),
(4, 'Rasel', 'rasel.laravel1@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(5, 'Rasel', 'rasel.laravel3@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(6, 'Rasel', 'rasel.laravel4@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(7, 'Rasel', 'rasel.laravel2@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(8, 'Rasel', 'rasel.laravel5@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(9, 'Rasel', 'rasel.laravel6@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(10, 'Rasel', 'rasel.laravel7@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(11, 'Rasel', 'rasel.laravel8@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(12, 'Rasel', 'rasel.laravel9@gmail.com', NULL, '$2y$10$aoKGsahelraFnwT9nyH3pOd6sh2NO6m8je6R.gk7jOEHToZUP.FfO', '01909888251', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '2021-03-16', NULL, '2021-03-28 14:32:21', '2021-03-28 18:48:12', NULL, '0-5 grade', NULL, NULL),
(14, 'Lukas', 'hr.lucas401@gmail.com', NULL, '$2y$10$IvuM6iBgR2XnrovhywtYhus1ZcWIjJsE5iyCtbQbWSJaFuN.IaZ9O', '345235345', NULL, 'sdafsadf', 'asdf', 'sadfsad', 'asdf', '2', 'adsfsda', NULL, '2021-04-15', NULL, '2021-04-12 17:32:58', '2021-04-12 17:32:58', 'BANGLADESH', '0-5 grade', NULL, NULL),
(15, 'Rasel', 'raselhasan584@gmail.com', NULL, '$2y$10$Qqo.WRGV1lqLtjnm9h3KJ.r53dMdZdLn1jz0VGc6RrL15iD82.reS', '+11909888251', NULL, 'dhaka', '1209', 'dhaka', NULL, '2', NULL, '1618485086.png', '2021-04-14', NULL, '2021-04-15 10:43:39', '2021-04-16 17:34:43', 'ALBANIA', '0-5 grade', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `quiz_id`, `title`, `language`, `video`, `active`, `description`, `created_at`, `updated_at`) VALUES
(1, 29, 'First Video', 'English', '<iframe width=\"559\" height=\"409\" src=\"https://www.youtube.com/embed/8dskqQG1z1E\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum', '2021-03-22 17:22:18', '2021-04-15 11:08:43'),
(3, 29, 'Video 1', 'English', '<iframe width=\"559\" height=\"409\" src=\"https://www.youtube.com/embed/8dskqQG1z1E\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum', '2021-03-22 17:51:23', '2021-04-15 11:08:39'),
(5, 32, 'Test Video', 'English', '<iframe width=\"727\" height=\"409\" src=\"https://www.youtube.com/embed/_iktURk0X-A?list=RDweAiiGJvvXQ\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 'ksdlfjlas asdlkfjaslkdfjasdf', '2021-04-16 16:20:40', '2021-05-03 00:05:52'),
(6, 31, 'Test', 'English', '<iframe width=\"727\" height=\"409\" src=\"https://www.youtube.com/embed/pKwNDsBxXRA?list=RDweAiiGJvvXQ\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', 1, 'sfsdfasdf', '2021-04-16 16:39:58', '2021-04-16 16:45:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_quiz_id_foreign` (`quiz_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_quiz_id_foreign` (`quiz_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_contents`
--
ALTER TABLE `home_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `money_requests`
--
ALTER TABLE `money_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `money_requests_user_id_foreign` (`user_id`);

--
-- Indexes for table `participant_users`
--
ALTER TABLE `participant_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_histories`
--
ALTER TABLE `payment_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_histories_user_id_foreign` (`user_id`),
  ADD KEY `payment_histories_withdrawal_id_foreign` (`withdrawal_id`);

--
-- Indexes for table `qnas`
--
ALTER TABLE `qnas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_options`
--
ALTER TABLE `question_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_options_question_id_foreign` (`question_id`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_quiz_id_foreign` (`quiz_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_contents`
--
ALTER TABLE `home_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `money_requests`
--
ALTER TABLE `money_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `participant_users`
--
ALTER TABLE `participant_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `payment_histories`
--
ALTER TABLE `payment_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `qnas`
--
ALTER TABLE `qnas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `question_options`
--
ALTER TABLE `question_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=530;

--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `money_requests`
--
ALTER TABLE `money_requests`
  ADD CONSTRAINT `money_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payment_histories`
--
ALTER TABLE `payment_histories`
  ADD CONSTRAINT `payment_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `payment_histories_withdrawal_id_foreign` FOREIGN KEY (`withdrawal_id`) REFERENCES `money_requests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `question_options`
--
ALTER TABLE `question_options`
  ADD CONSTRAINT `question_options_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
