<form action="{{route('admin.contact.save')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Contact</h5>
                    <span>Add New contact page content here</span>
                    <div class="card-header-right">
                        
                    </div>
                </div>
                <input type="hidden" name="id" value="{{@$contact->id}}">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Facebook Url</label>
                                <input type="text" name="fb" class="form-control" value="{{@$contact->fb}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Twitter Url</label>
                                <input type="text" name="tw" class="form-control" value="{{@$contact->tw}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Pinterest Url</label>
                                <input type="text" name="pnt" class="form-control" value="{{@$contact->pnt}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Youtube Url</label>
                                <input type="text" name="yt" class="form-control" value="{{@$contact->yt}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-default">
                                <label class="float-label">Instagram Url</label>
                                <input type="text" name="ins" class="form-control" value="{{@$contact->ins}}">
                            </div>
                        </div>      
                        <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Content</label>
                                <textarea id="contact-content" name="contact">{{@$contact->contact}}</textarea>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>