@extends('quiz.frontend.layout.app')
@section('title')
	Profile
@endsection
@section('style')

@endsection
@section('content')
	<section class="main-section p-0"> 
		<div class="row m-0">
			<div class="col-12 col-lg-2 p-0">
				@include('quiz.frontend.include.left-bar')
			</div>
			<div class="col-12 col-lg-10 p-0">
				<div class="main-body">
					@include('quiz.frontend.pages.quiz.bradcam')
					<form action="{{route('user.profile')}}" method="post">
						@csrf
						<div class="row mr-0">
							@if(Session::has('success'))
								<div class="col-12 col-md-12 pr-0">
									<div class="alert alert-primary" role="alert">
									   {{Session::get('success')}}
									</div>
								</div>
							@endif

							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Full Name</label>
									<input type="text" class="form-control" name="name" required value="{{$profile->name}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Email</label>
									<input type="email" class="form-control" name="email" required value="{{$profile->email}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Date of Birth</label>
									<input type="date" class="form-control" name="date_of_birth" required value="{{$profile->date_of_birth}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Grade</label>
									<select id="inputState" class="form-control" name="grade" required>
							        	<option value="">Choose...</option>
							        	<option value="0-5 grade" @if($profile->grade == '0-5 grade') selected @endif >0-5 Grade</option>
							        	<option value="6-8 grade" @if($profile->grade == '6-8 grade') selected @endif >6-8 Grade</option>
							        	<option value="9-12 grade" @if($profile->grade == '9-12 grade') selected @endif >9-12 Grade</option>
							        	<option value="above/adult" @if($profile->grade == 'above/adult') selected @endif >Above/Adult</option>
							      	</select>
								</div>
							</div>

							
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Phone</label>
									<input type="text" class="form-control" name="phone" required value="{{$profile->phone}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Address</label>
									<input type="text" class="form-control" name="address" value="{{$profile->address}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">City</label>
									<input type="text" class="form-control" name="city" value="{{$profile->city}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">State</label>
									<input type="text" class="form-control" name="state" value="{{$profile->state}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Zip</label>
									<input type="text" class="form-control" name="zip" value="{{$profile->zip}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Country</label>
									<select id="inputState" class="form-control" name="country" required>
							        	<option value="">Choose...</option>
							        	@foreach($countries as $country)
							        		<option value="{{$country->name}}" @if($country->name == $profile->country) selected @endif>{{$country->name}}</option>
							        	@endforeach	
							      	</select>
								</div>
							</div>
							
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Guardian</label>
									<input type="text" class="form-control" name="guardian" value="{{$profile->guardian}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Relation with Guardian</label>
									<input type="text" class="form-control" name="relation_with_guardian" value="{{$profile->relation_with_guardian}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Guardian Phone</label>
									<input type="text" class="form-control" name="guardian_phone" value="{{$profile->guardian_phone}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<label class="mb-2">Guardian Email</label>
									<input type="email" class="form-control" name="guardian_email" value="{{$profile->guardian_email}}">
								</div>
							</div>
							<div class="col-12 col-md-4 pr-0">
								<div class="form-group">
									<button type="submit" class="btn btn-info" style="margin-top: 28px;">Update</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')

@endsection
