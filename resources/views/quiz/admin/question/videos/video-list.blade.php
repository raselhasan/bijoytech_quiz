@if(count($quiz->videos) > 0)
    @foreach($quiz->videos as $video)
        <tr>
            <td>
                <div class="form-check" onclick="getAllQuestion({{$video->id}},'4','demo')">
                    <input class="form-check-input" type="radio" name="selected_book" id="exampleRadios{{$video->id}}" value="{{$video->id}}">
                    <label class="form-check-label" for="exampleRadios{{$video->id}}">
                        Select Video
                    </label>
                </div>
            </td>
            <td><a href="#">{{$video->title}}</a></td>
            <td>{{$video->language}}</td>
            <td>
                @if($video->active == 1)
                    <p class="bk-active" onclick="videoActive({{$video->id}},{{$video->quiz_id}},'0')">Active</p>
                @else 
                    <p class="bk-dactive" onclick="videoActive({{$video->id}},{{$video->quiz_id}},'1')">Dactive</p>
                @endif    
            </td>
            <td>
                <button class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn mr-2" onclick="editVideo({{$video->id}})" data-toggle="tooltip" data-placement="top" title="Edit Video"><i class="ti-slice"></i></button>
                <button class="btn waves-effect waves-dark btn-danger btn-outline-danger edit-del-btn" onclick="deleteVideo({{$video->quiz_id}},{{$video->id}})" data-toggle="tooltip" data-placement="top" title="Delete Video"><i class="ti-trash"></i></button>
                <button class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn" onclick="viewVideo('{{$video->video}}')" data-toggle="tooltip" data-placement="top" title="View Video"><i class="ti-eye"></i></button>
            </td>
        </tr>
    @endforeach
@endif
