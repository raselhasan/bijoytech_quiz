@extends('quiz.frontend.layout.app')

@section('title')
	Team
@endsection

@section('style')
	<style type="text/css">
		h4{
			color: yellowgreen;
			text-transform: uppercase;
		}
		hr{
			background: #448aff;
    		width: 16%;
		}
		.card{
			margin-top: 20px;
		}
	</style>
@endsection
@section('content')
<div class="home-cover">
	@include('quiz.frontend.include.banner')
</div>
<section class="main-section"> 
	<div class="container">
		<div class="row mr-0">
			<div class="col-3 col-md-3 col-lg-3 pr-0">
				<div class="card">
				  	<img class="card-img-top img-fluid" src="{{asset('teams/'.$team->image)}}" alt="Card image cap" style="height: 300px">
				  	<div class="card-body">
					    <h5 class="card-title text-center">{{$team->name}}</h5>
					    <h6 class="card-title text-center">{{$team->deg}}</h6>
					    
				  	</div>
				</div>
			</div>
			<div class="col-9 col-md-9 col-lg-9 pr-0" style="margin-top: 20px;">
				{!! $team->desc !!}
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')
	
@endsection
