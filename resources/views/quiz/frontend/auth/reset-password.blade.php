@extends('quiz.frontend.layout.app')
@section('style')

@endsection
@section('content')
<section class="main-section">
	<div class="log-cnt">
		<div class="row mx-0">
			<div class="col-6 px-0">
				<div class="form">
					<h4 class="text-center mb-4 text-uppercase">Reset password</h5>
					@if(Session::has('msg'))
						<p class="err-msg">{{Session::get('msg')}}</p>
					@endif
					<form action="{{route('user.confirmPassword')}}" method="post">
						@csrf
						<div class="form-group">
							<label class="mb-2">New Password</label>
							<input type="password" class="form-control" name="password" />
						</div>
						<div class="form-group">
							<label class="mb-2">Confirm New Password</label>
							<input type="password" class="form-control" name="c_password" />
						</div>
						<input type="hidden" name="code" value="{{$code}}">
						<div class="form-group mt-4">
							<button type="submit" class="btn btn-primary btn-block">Reset</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-6 px-0">
				<div class="lr-cover">
					<img class="cover"  src="{{asset('files/assets/images/log.jpg')}}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')

@endsection
