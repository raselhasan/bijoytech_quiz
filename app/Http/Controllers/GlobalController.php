<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use Helper;
use App\ParticipantUser;
use App\Answer;
use App\User;
use App\Setting;
use App\About;
use App\Team;
use App\Faq;
use App\Qna;
use App\Contact;

class GlobalController extends Controller
{
    public function addQuiz(Request $request)
    {
    	$status = Helper::quizStatus(url()->previous());
    	Helper::addQuiz($request, $status);
    	return redirect()->back()->with('success','Quiz successfully saved!');
    }
    public function editQuiz(Request $request)
    {
    	$data['quiz'] = Quiz::find($request->id);
    	return view('quiz.admin.global.edit-quiz', $data)->render();
    }

    public function updateQuiz(Request $request)
    {
    	Helper::updateQuiz($request);
    	return 'success';
    }
    public function deleteQuiz(Request $request)
    {
    	Quiz::where('id', $request->id)->delete();
    }
    public function activeLanguage(Request $request)
    {
        $column = $request->column;
        $quiz = Quiz::find($request->quiz_id);
        $quiz->$column = $request->status;
        $quiz->save();
        $data['quiz'] = $quiz;
        $data['status'] = $request->quiz_status;
        return view('quiz.admin.question.language.single-list',$data)->render();
        
    }

    public function settingContent( Request $request)
    {
        $data['name'] = \Request::route()->getName();
        $data['setting'] = Setting::first();
        return view('quiz.frontend.pages.setting.index',$data);
    }

    public function participant($quiz_id, $status)
    {
        $data['quiz_id'] = $quiz_id;
        $data['status'] = $status;
        // $posts = ParticipantUser::select('participant_users.*','users.name as user_name','books.title as book_title','quizzes.title as quiz_title')
        //         ->join("users","users.id","=","participant_users.user_id")
        //         ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
        //         ->join("books","books.id","=","participant_users.book_id")->get();

        //$data['users'] = ParticipantUser::with('book','quiz','user')->get();
        //dd($posts->toArray());
        return view('quiz.admin.global.answer', $data);
    }
    public function getParticipant(Request $request, $quiz_id, $status)
    {
        $columns = array( 
            0 => 'quiz_title',
            1 => 'book_title',
            2 => 'user_name',
            3 => 'total_right_ans', 
            4 => 'total_worng_ans',
            5 => 'aword',
            6 => 'position',
            7 => 'created_at',
            8 => 'id'

        );
    
        $totalData = ParticipantUser::where('quiz_id',$quiz_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = ParticipantUser::select('participant_users.*','users.name as user_name','users.id as user_id','books.title as book_title','quizzes.title as quiz_title')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("books","books.id","=","participant_users.book_id")->offset($start)
                 ->limit($limit)
                 ->orderBy($order,$dir)
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = ParticipantUser::select('participant_users.*','users.name as user_name','books.title as book_title','quizzes.title as quiz_title','users.id as user_id')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("books","books.id","=","participant_users.book_id")
                ->where(function($query) use ($search){
                    $query->where('participant_users.id','LIKE',"%{$search}%")
                        ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('books.title', 'LIKE',"%{$search}%")
                        ->orWhere('users.name', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = ParticipantUser::select('participant_users.*','users.name as user_name','books.title as book_title','quizzes.title as quiz_title','users.id as user_id')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("books","books.id","=","participant_users.book_id")
                ->where(function($query) use ($search){
                    $query->where('participant_users.id','LIKE',"%{$search}%")
                        ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('books.title', 'LIKE',"%{$search}%")
                        ->orWhere('users.name', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $nestedData['quiz_title'] = $post->quiz_title;
                $nestedData['book_title'] = $post->book_title;
                $nestedData['user_name'] = $post->user_name;
                $nestedData['total_right_ans'] = $post->total_right_ans;
                $nestedData['total_worng_ans'] = $post->total_worng_ans;
                $nestedData['aword'] = '$ '.number_format($post->aword, 2);
                $nestedData['position'] = $post->position;
                $nestedData['created_at'] = date('m/d/Y', strtotime($post->date)).' '.date('h:i A', strtotime($post->created_at));
                $url = route('admin.view.answer',['quiz_id'=>$quiz_id, 'status'=>$status,'user_id'=>$post->user_id]);
                $nestedData['options'] = '<a href="'.$url.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn quiz-dlt-btn" data-toggle="tooltip" data-placement="top" title="View Answer"><i class="ti-eye"></i></a>';
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }

    public function videoParticipant($quiz_id, $status){
        $data['quiz_id'] = $quiz_id;
        $data['status'] = $status;
        return view('quiz.admin.global.video.index', $data);
    }

    public function getVideoParticipant(Request $request, $quiz_id, $status)
    {
        $columns = array( 
            0 => 'quiz_title',
            1 => 'video_title',
            2 => 'user_name',
            3 => 'total_right_ans', 
            4 => 'total_worng_ans',
            5 => 'aword',
            6 => 'position',
            7 => 'created_at',
            8 => 'id'

        );
    
        $totalData = ParticipantUser::where('quiz_id',$quiz_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = ParticipantUser::select('participant_users.*','users.name as user_name','users.id as user_id','videos.title as video_title','quizzes.title as quiz_title')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("videos","videos.id","=","participant_users.book_id")->offset($start)
                 ->limit($limit)
                 ->orderBy($order,$dir)
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = ParticipantUser::select('participant_users.*','users.name as user_name','videos.title as video_title','quizzes.title as quiz_title','users.id as user_id')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("videos","videos.id","=","participant_users.book_id")
                ->where(function($query) use ($search){
                    $query->where('participant_users.id','LIKE',"%{$search}%")
                        ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('videos.title', 'LIKE',"%{$search}%")
                        ->orWhere('users.name', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = ParticipantUser::select('participant_users.*','users.name as user_name','videos.title as book_title','quizzes.title as quiz_title','users.id as user_id')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("videos","videos.id","=","participant_users.book_id")
                ->where(function($query) use ($search){
                    $query->where('participant_users.id','LIKE',"%{$search}%")
                        ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('videos.title', 'LIKE',"%{$search}%")
                        ->orWhere('users.name', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $nestedData['quiz_title'] = $post->quiz_title;
                $nestedData['video_title'] = $post->video_title;
                $nestedData['user_name'] = $post->user_name;
                $nestedData['total_right_ans'] = $post->total_right_ans;
                $nestedData['total_worng_ans'] = $post->total_worng_ans;
                $nestedData['aword'] = '$ '.number_format($post->aword, 2);
                $nestedData['position'] = $post->position;
                $nestedData['created_at'] = date('m/d/Y', strtotime($post->date)).' '.date('h:i A', strtotime($post->created_at));
                $url = route('admin.view.answer',['quiz_id'=>$quiz_id, 'status'=>$status,'user_id'=>$post->user_id]);
                $nestedData['options'] = '<a href="'.$url.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn quiz-dlt-btn"><i class="ti-eye"></i></a>';
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }


    public function GeneralParticipant( $quiz_id = null, $status = null)
    {
        $data['quiz_id'] = $quiz_id;
        $data['status'] = $status;
        
        return view('quiz.admin.global.general.index', $data);
    }
    public function getGeneralParticipant(Request $request, $quiz_id = null, $status = null)
    {
        $columns = array( 
            0 => 'quiz_title',
            1 => 'language',
            2 => 'user_name',
            3 => 'total_right_ans', 
            4 => 'total_worng_ans',
            5 => 'aword',
            6 => 'position',
            7 => 'created_at',
            8 => 'id'

        );
    
        $totalData = ParticipantUser::where('quiz_id',$quiz_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = ParticipantUser::select('participant_users.*','users.name as user_name','users.id as user_id','quizzes.title as quiz_title')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = ParticipantUser::select('participant_users.*','users.name as user_name','quizzes.title as quiz_title','users.id as user_id')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->where(function($query) use ($search){
                    $query->where('participant_users.id','LIKE',"%{$search}%")
                        ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.language', 'LIKE',"%{$search}%")
                        ->orWhere('users.name', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = ParticipantUser::select('participant_users.*','users.name as user_name','quizzes.title as quiz_title','users.id as user_id')
                ->where('participant_users.quiz_id',$quiz_id)
                ->join("users","users.id","=","participant_users.user_id")
                ->join("quizzes","quizzes.id","=","participant_users.quiz_id")
                ->join("videos","videos.id","=","participant_users.book_id")
                ->where(function($query) use ($search){
                    $query->where('participant_users.id','LIKE',"%{$search}%")
                        ->orWhere('quizzes.title', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.language', 'LIKE',"%{$search}%")
                        ->orWhere('users.name', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_right_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.total_worng_ans', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.aword', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.position', 'LIKE',"%{$search}%")
                        ->orWhere('participant_users.date', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {

                $nestedData['quiz_title'] = $post->quiz_title;
                $nestedData['language'] = $post->language;
                $nestedData['user_name'] = $post->user_name;
                $nestedData['total_right_ans'] = $post->total_right_ans;
                $nestedData['total_worng_ans'] = $post->total_worng_ans;
                $nestedData['aword'] = '$ '.number_format($post->aword, 2);
                $nestedData['position'] = $post->position;
                $nestedData['created_at'] = date('m/d/Y', strtotime($post->date)).' '.date('h:i A', strtotime($post->created_at));
                $url = route('admin.view.answer',['quiz_id'=>$quiz_id, 'status'=>$status,'user_id'=>$post->user_id]);
                $nestedData['options'] = '<a href="'.$url.'" class="btn waves-effect waves-dark btn-info btn-outline-info edit-del-btn quiz-dlt-btn"><i class="ti-eye"></i></a>';
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }




    public function viewAnser($quiz_id, $status, $user_id)
    {
        $data['perticipant'] = ParticipantUser::where('quiz_id',$quiz_id)->where('user_id',$user_id)->with('book','user','quiz')->first();
        if($status == 4){
            $data['perticipant'] = ParticipantUser::where('quiz_id',$quiz_id)->where('user_id',$user_id)->with('video','user','quiz')->first();
            //dd($data['perticipant']->toArray());
        }
        $data['status'] = $status;
        $data['quiz_id'] = $quiz_id;
        $data['questionAndAnswer'] = Answer::where('quiz_id', $quiz_id)->where('userId',$user_id)->with('question.options')->get();
        //dd($data['perticipant']->toArray());
        return view('quiz.admin.global.view-answer.index', $data);
    }

    public function evaluate( $quiz_id )
    {
        $quiz = Quiz::find($quiz_id);
        $perticipant = ParticipantUser::where('quiz_id',$quiz_id)->orderBy('total_mark','desc')->limit($quiz->person)->get();
        $per_person = 0;
        if($quiz->price){
            if($quiz->distribution == 2){
                $per_person = $quiz->price / $quiz->person;
            }else if($quiz->distribution == 1){
                $per_person = $quiz->price;
            }
        }
        if(count($perticipant) > 0){
            foreach ($perticipant as $key => $user) {
                $pUpdate = ParticipantUser::find($user->id);
                $pUpdate->position = $key + 1;
                $pUpdate->aword = $per_person;
                $pUpdate->save();
            }
        }
        return redirect()->back()->with('success','Money successfully distibute');
    }

    public function users()
    {
        return view('quiz.admin.users.index');
    }

    public function getUsers( Request $request )
    {
        $columns = array( 
            0 => 'image', 
            1 => 'name',
            2 => 'email',
            3 => 'phone',
            4 => 'address',
            5 => 'country',
            6 => 'id',

        );
  
        $totalData = User::where('role',2)->count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){

            $posts = User::where('role',2)->offset($start)
                 ->limit($limit)
                 ->orderBy('id','DESC')
                 ->get();
        }else {
            $search = $request->input('search.value'); 
            $posts = User::where('role',2)
                ->where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('name', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->orWhere('phone', 'LIKE',"%{$search}%")
                        ->orWhere('address', 'LIKE',"%{$search}%")
                        ->orWhere('phone', 'LIKE',"%{$search}%")
                        ->orWhere('country', 'LIKE',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = User::where('role',3)
                ->where(function($query) use ($search){
                    $query->where('id','LIKE',"%{$search}%")
                        ->orWhere('name', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->orWhere('phone', 'LIKE',"%{$search}%")
                        ->orWhere('address', 'LIKE',"%{$search}%")
                        ->orWhere('phone', 'LIKE',"%{$search}%")
                        ->orWhere('country', 'LIKE',"%{$search}%");
                })
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $imgUrl = url('user_image/default.jpg');
                if($post->image){
                     $imgUrl = url('user_image/'.$post->image);
                }

                $nestedData['image'] = '<img src="'.$imgUrl.'" class="img-fluid" style="width:50%; border-radius:100%">';
                $nestedData['name'] = $post->name;
                $nestedData['email'] = $post->email;
                $nestedData['phone'] = $post->phone;
                $nestedData['address'] = $post->address;
                $nestedData['country'] = $post->country;
                $nestedData['options'] = '
                <button user-id="'.$post->id.'" class="btn waves-effect waves-dark btn-success btn-outline-success edit-del-btn user-vw-btn" data-toggle="tooltip" data-placement="top" title="View User"><i class="ti-eye"></i></button>
                ';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }

    public function viewUser( Request $request )
    {
        $data['user'] = User::find( $request->id );
        return view('quiz.admin.users.view-user', $data)->render();
    }

    public function contactUs()
    {
        $data['contact'] = Contact::first();
        return view('quiz.frontend.contact-us.index',$data);
    }
    public function aboutUs()
    {
        $data['about'] = About::first();
        $data['teams'] = Team::all();
        return view('quiz.frontend.about-us.index', $data);
    }
    public function team($id)
    {
        $data['team'] = Team::findOrFail($id);
        return view('quiz.frontend.about-us.team-details', $data);
    }

    public function faq()
    {
        $data['faq'] = Faq::first();
        $data['qnas'] = Qna::all();
        return view('quiz.frontend.faq.index', $data);
    }
    public function terms()
    {
        $data['setting'] = Setting::first();
        return view('quiz.frontend.terms.index', $data);
    }
    public function policy()
    {
        $data['setting'] = Setting::first();
        return view('quiz.frontend.policy.index',$data);
    }

    

}
