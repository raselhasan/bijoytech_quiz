<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Team Content</h5>
                <span>List of team page contents</span>
            </div>
            
            <div class="card-block">
                @include('quiz.admin.about.table-data')
            </div>
        </div>
    </div>
</div>
