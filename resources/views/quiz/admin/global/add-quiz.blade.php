<form action="{{route('admin.quiz.add')}}" method="post">
    @csrf
    <div class="modal fade" id="add-quiz-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Quiz</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="row">
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Quiz Title</label>
                                <input type="text" name="title" class="form-control" required="">
                            </div>
                       </div> 
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Start Date</label>
                                <input type="date" name="start_date" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Start Time</label>
                                <input type="time" name="start_time" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">End Date</label>
                                <input type="date" name="end_date" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">End Time</label>
                                <input type="time" name="end_time" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Grade</label>
                                <select class="form-control" name="grade" id="exampleFormControlSelect1" required>
                                  <option value="0-5 grade">0 - 5 Grade</option>
                                  <option value="6-8 grade">6 - 8 Grade</option>
                                  <option value="9-12 grade">9 - 12 Grade</option>
                                  <option value="above/adult">Above/Adult</option>
                                </select>
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Every Question Mark</label>
                                <input type="number" name="mark" class="form-control" required="">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Total Question</label>
                                <input type="number" name="total_quistion" class="form-control" required="">
                            </div>
                       </div>

                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Price Money</label>
                                <input type="number" name="price" class="form-control">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Number of Person</label>
                                <input type="number" name="person" class="form-control">
                            </div>
                       </div>
                       <div class="col-md-4">
                            <div class="form-group form-default">
                                <label class="float-label">Amount Distribution</label>
                                <select class="form-control" name="distribution" id="exampleFormControlSelect2" required>
                                  <option value="">--select--</option>
                                  <option value="1">Every Person</option>
                                  <option value="2">Distribution</option>
                                </select>
                            </div>
                       </div>
                       @if(@$status == 1 || @$status == 2)
                       <div class="col-md-12">
                            <div class="form-group form-default">
                                <label class="float-label">Description</label>
                                <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                       </div>
                       @endif
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>