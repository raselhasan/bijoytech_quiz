@extends('quiz.frontend.layout.app')
@section('style')
	<style type="text/css">
		
	</style>
@endsection
@section('content')
	<section class="main-section p-0"> 
		<div class="row m-0">
			<div class="col-12 col-lg-2 p-0">
				@include('quiz.frontend.include.left-bar')
			</div>
			<div class="col-12 col-lg-10 p-0">
				<div class="main-body">
					@include('quiz.frontend.pages.quiz.bradcam')
					<div class="row mr-0">
						@if($status == 3)
							@include('quiz.frontend.pages.books.list')
						@endif
						@if($status == 4)
							@include('quiz.frontend.pages.books.video-list')
						@endif
						@if($status == 1 || $status == 2)
							@include('quiz.frontend.pages.books.general')
						@endif	
					</div>
				</div>
			</div>
		</div>
	</section>
	@if($status == 3)
		@include('quiz.frontend.pages.books.read-book')
	@endif
	@if($status == 4)
		@include('quiz.frontend.pages.books.video-modal')
	@endif

@endsection

@section('script')
	<script type="text/javascript">
		function readBook(url)
		{
			$('.setBookUrl').attr('src',url);
			$('#readBookModal').modal('show');
		}

		function watchVideo(url)
		{
			$('.setVideo').html(url);
			$('#videoModal').modal('show');
		}
	</script>
@endsection
