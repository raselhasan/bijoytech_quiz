<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Videos</h5>
            <span>Embed Video list here</span>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Select Video</th>
                            <th>Video Title</th>
                            <th>Video Language</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="set-all-videos">
                        @include('quiz.admin.question.videos.video-list')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('quiz.admin.question.videos.add-video')
<div class="set-edit-video-modal"></div>
