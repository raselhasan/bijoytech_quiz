@if($quiz->active_english == 1)
	<div class="col-12 col-md-6 col-lg-3 pr-0">
		<div class ="card mb-3">
			<div class="card-image">
				<img class="cover"  src="{{asset('files/assets/images/cover1.jpg')}}">
			</div>
			<div class="card-header text-center p-3">
				<a href="{{url('/question')}}"><h5 class="mb-0">{{$quiz->title}}</h5></a>
			</div>
			<div class="card-block pt-0 quiz-card">
				<p>Language: <span>English</span></p>
				<p>Total Question: <span>{{$quiz->total_quistion}}</span></p>
				<p>Every Question Mark: <span>{{$quiz->mark}}</span></p>
				@if($quiz->price)
					<p>Price Money: <span>${{ number_format($quiz->price, 2)}}</span></p>
					<p>Price Money Will Get: <span>{{$quiz->person}} Top Scorer</span></p>
					<p>Top Each Person Will Get: <span>${{number_format($quiz->price / $quiz->person, 2)}}</span></p>

				@endif
				<div class="text-center join-quiz-btn">
					<a href="{{route('user.question',['quiz_id'=>$quiz->id,'status'=>$status])}}?lang=English" class="btn btn-info">Start Quiz</a>

				</div>

			</div>
		</div>
	</div>
	<div class="col-12 col-md-9 col-lg-9 pr-0 quiz-des justify-content about-quiz-h2">
		<h4>About This Quiz</h4>
		<p>{{$quiz->description}}</p>
	</div>
@endif
@if($quiz->active_bangla == 1)
	<div class="col-12 col-md-6 col-lg-3 pr-0">
		<div class ="card mb-3">
			<div class="card-image">
				<img class="cover"  src="{{asset('files/assets/images/cover1.jpg')}}">
			</div>
			<div class="card-header text-center p-3">
				<a href="{{url('/question')}}"><h5 class="mb-0">{{$quiz->title}}</h5></a>
			</div>
			<div class="card-block pt-0 quiz-card">
				<p>Language: <span>Bangla</span></p>
				<p>Total Question: <span>{{$quiz->total_quistion}}</span></p>
				<p>Every Question Mark: <span>{{$quiz->mark}}</span></p>
				@if($quiz->price)
					<p>Price Money: <span>${{ number_format($quiz->price, 2)}}</span></p>
					<p>Price Money Will Get: <span>{{$quiz->person}} Top Scorer</span></p>
					<p>Top Each Person Will Get: <span>${{number_format($quiz->price / $quiz->person, 2)}}</span></p>

				@endif
				<div class="text-center join-quiz-btn">
					<a href="{{route('user.question',['quiz_id'=>$quiz->id,'status'=>$status])}}?lang=Bangla" class="btn btn-info">Start Quiz</a>

				</div>

			</div>
		</div>
	</div>
	<div class="col-12 col-md-9 col-lg-9 pr-0 quiz-des justify-content about-quiz-h2">
		<h4>About This Quiz</h4>
		<p>{{$quiz->description}}</p>
	</div>
@endif