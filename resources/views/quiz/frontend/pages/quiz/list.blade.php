@if(count($quizes) > 0)
	@php
		$hasQuiz = 0;
	@endphp
	@foreach($quizes as $quiz)
		@if(Helper::isAttendQuiz($quiz->id) == false)
			@php
				$hasQuiz = 1;
			@endphp
			<div class="col-12 col-md-6 col-lg-3 pr-0">
				<div class ="card mb-3">
					<div class="card-image">
						<img class="cover"  src="{{asset('files/assets/images/cover1.jpg')}}">
					</div>
					<div class="card-header text-center p-3">
						<a href="{{url('/question')}}"><h5 class="mb-0">{{$quiz->title}}</h5></a>
					</div>
					<div class="card-block pt-0 quiz-card">
						<p>Total Question: <span>{{$quiz->total_quistion}}</span></p>
						<p>Every Question Mark: <span>{{$quiz->mark}}</span></p>
						@if($quiz->price)
							<p>Price Money: <span>${{ number_format($quiz->price, 2)}}</span></p>
							<p>Price Money Will Get: <span>{{$quiz->person}} Top Scorer</span></p>
							<p>Top Each Person Will Get: <span>${{number_format($quiz->price / $quiz->person, 2)}}</span></p>

						@endif
						<div class="text-center join-quiz-btn">
							<a href="{{route('user.show.book',['quiz_id'=>$quiz->id,'status'=>$status])}}" class="btn btn-info">Join Quiz</a>
						</div>

					</div>
				</div>
			</div>
		@endif	
	@endforeach
	@if($hasQuiz == 0)
	
		<div class="col-md-12 no-quiz pr-0">
			<p class="">No Quiz found! Please try again after sometime.</p>
		</div>
	@endif	
@endif