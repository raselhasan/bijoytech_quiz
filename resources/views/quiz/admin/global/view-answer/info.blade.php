<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Quiz Details</h5>
            <span>Quiz and User Details</span>
        </div>
        <div class="card-block">
            <div class="row answerDetals">
                <div class="col-md-6">
                    <div class="quiz-dts">
                        <p class="qtitle">{{$perticipant->quiz->title}}</p>
                        <p><span>From</span> {{date('m/d/Y h:i A',strtotime($perticipant->quiz->start_date))}} - {{date('m/d/Y h:i A',strtotime($perticipant->quiz->end_date))}}</p>
                        @if($perticipant->quiz->price)
                            <p>${{number_format($perticipant->quiz->price, 2)}}</p>
                        @endif
                        @if($status == 3)
                            <p><span>Book Title:</span> {{$perticipant->book->title}}</p>
                            <p><span>Book Language:</span> {{$perticipant->book->language}}</p>
                        @endif
                        @if($status == 4)
                            <p><span>Video Title:</span> {{$perticipant->video->title}}</p>
                            <p><span>Video Language:</span> {{$perticipant->video->language}}</p>
                        @endif        
                    </div>
                </div> 
                <div class="col-md-6">
                    <p><span>Name:</span> {{$perticipant->user->name}}</p>
                    <p><span>Phone:</span> {{$perticipant->user->phone}}</p>
                    <p><span>Total Right Answer:</span> {{$perticipant->total_right_ans}}</p>
                    <p><span>Total Worng Answer:</span> {{$perticipant->total_worng_ans}}</p>
                    <p><span>Total Mark:</span> {{$perticipant->total_mark}}</p>
                    @if($perticipant->position)
                        <p><span>Position:</span> {{$perticipant->position}}</p>
                    @endif
                    @if($perticipant->aword)
                        <p><span>Position:</span> {{number_format($perticipant->aword, 2)}}</p>
                    @endif    
                </div>  
            </div>
        </div>
    </div>
</div>